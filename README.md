
# Sammendrag
Dette prosjektets hovedmål er å frembringe en programvare for kameraovervåkning til bruk i rovviltforvaltning og husdyrhold.  
Programvaren skal bruke maskinlæring for å gjenkjenne de vanligste husdyrene på gård, rovdyrene som vanligst tar dem, og personer.  
Programvaren skal ivareta en høy standard for personvern, datasikkerhet og brukervennlighet;  
og gjøre det unødvendig å stole på uverifiserbar programvare fra tredjeparter og kameraprodusenter. Programvaren skal være fri.
Prosjektet følger standarder for kvalitetsstyring og prosjektledelse, og bruker testdrevet utvikling, kontinuerlig integrasjon og programvare-prototyping. 


# Summary
The main goal of this project is to develop software for camera surveillance used in wildlife predator management and livestock farming.  
The software will utilize machine learning to recognize the most common farm animals, the predators that commonly target them, and humans.  
The software aims to uphold a high standard of privacy, data security, and user-friendliness;  
and eliminate the need to rely on unverified software from third parties and camera manufacturers. The software shall be open source.
The project adheres to standards for quality management and project leadership, employing test-driven development, continuous integration, and software prototyping.


# Bidrag
For å gjøre gjenkjenning av ulike dyr så pålitelig som mulig, behøves det flere tusen bilder av hver dyreart.  
Send gjerne bilder eller lenker til datasett av dyretypene:  
sau, lam, jerv, gaupe, kongeørn, hund, katt, ulv, rev  
til <jacob.d.ludvigsen@uit.no>


# Contributions
To increase reliability of detecting various animals, several thousand images of each type of animal is needed.  
A good way to contribute is by sending images or links to larger datasets av these animals:  
sheep, lamb, wolverine, lynx, golden eagle, dog, cat, wolf, fox  
to <jacob.d.ludvigsen@uit.no>

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
