# Arbeidsøkt timelogg

### Dato: 15.05.2024
### Tidsrom: 13:00 - 23:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Fortsetter skrivinga på kapitlene Resultat, Konklusjon og Diskusjon.

# Emne 2: Utvikling / Trening av objektgjenkjenning (7 timer)
Videreutvikler backenden, og trener objektgjenkjenningsmodellen.

Møter noen utfordringer med ulik håndtering av bl.a. mellomrom i filnavn, mellom backenden og frontenden, og lager en halvgrei omvei.


	


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
