# Arbeidsøkt timelogg

### Dato: 28.03.2024
### Tidsrom: 10:00 - 12:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Prøver å eksportere RT-DETR fra pytorch til Nvidias TensorRT (for effektiv kjøring på min Nvidia GPU) og Tensorflow-lite (for kjøring på Coral ML-akselerator). Lykkes ikke med tensorflow-lite grunnet programvarelus i konverteringsbibliotekene, og SAHI viser seg å ikke støtte TensorRT.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
