# Arbeidsøkt timelogg

### Dato: 20.05.2024
### Tidsrom: 12:00 - 13:00,  15:00 - 17:00, 19:00 - 22:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling (30 min)
Utvikler funksjon i Predalert som sladder skikkelsen av personer med et ugjennomsiktig rektangel, dersom objektet "person" gjenkjennes. Fikser også sletting av gjenkjenning av "person" fra gjenkjenningsdetaljer, som jeg forsøkte å implementere tidligere.

# Emne 2: Rapportskriving
Skriver på kapitlene System og Litteraturstudie, med fokus på seksjonene Arkitektur, Konseptutvikling og Programvare.

Ordner flere vedlegg.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
