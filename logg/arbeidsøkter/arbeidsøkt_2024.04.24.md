# Arbeidsøkt timelogg

### Dato: 24.04.2024
### Tidsrom: 08:00 - 09:00, 10:00 - 16:00, 18:00 - 20:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
## Statusrapport 2
Skriver mer på Statusrapport 2. Fyller inn kapitler og seksjoner med tekst, ordner vedlegg.

Sender epost til studieleder for bachelor i maskinteknikk ved UiT Narvik, Øyvind Søraas; om at jeg har blitt invitert til et digitalt jobbintervju imorgen, og ber om råd. Han råder meg til å lage en knallgod statuspresentasjon for bacheloroppgaven, for å vise frem til intervjueren, og heller levere statusrapporten litt senere.  
Legger derfor fra meg arbeidet med statusrapporten, og begynner arbeidet med presentasjonen.

## Statuspresentasjon 2
Lager en lysbildepresentasjon med LibreOffice Impress. 	

## Endringsmelding 1
Lager den første endringsmeldinga i prosjektet, som tar for seg revisjon av aktivitetsbeskrivelser.





<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
