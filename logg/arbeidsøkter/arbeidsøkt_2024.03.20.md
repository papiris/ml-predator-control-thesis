# Arbeidsøkt timelogg

### Dato: 20.03.2024
### Tidsrom: 10:15 -  17:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling 
## Web push for Predalert
Fortsetter arbeidet med å implementere Web Push i programmet Predalert.  
Spør om hjelp i Discord-serveren Norsk Programmering, og får svar fra brukeren @matsl at jeg burde følge denne løypa:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.

I dag har jeg løst 1.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
