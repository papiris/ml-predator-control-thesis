# Arbeidsøkt timelogg

### Dato: 03.04.2024
### Tidsrom: 20:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Feilsøker OpenZiti integrasjonen i Predalert ved hjelp av Clint i OpenZiti forumet.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
