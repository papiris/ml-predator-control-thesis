# Arbeidsøkt timelogg

### Dato: 22.03.2024
### Tidsrom: 08:30 - 15:00, 17:00 - 22:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Maskinvare
## Undersøke tilgjengelig maskinvare
Undersøker maskinvare som kan være aktuell for å bruke i endelig produkt.  
Tynne klienter, minipc-er og énkortsmaskiner tilgjengelig nye og brukte blir undersøkt.

Ser også på maskinlæringsakseleratorer, hvor [Hailo AI](https://hailo.ai) sine Hailo-8 og Hailo-8L virker spesielt interessante. Finner ut at Hailo er et israelsk selskap, hvilket betyr at handel med dem medfører uforsonlige etiske ulemper, på grunn av Israels pågående folkemord av palestinere.  
Sender selskapet følgende epost: 

> Greetings!
> 
> We're developing a video surveillance solution, with the value proposition of alerting farmers immediately if predators approach their livestock; and repelling the predators before they can attack by deploying audio-visual repellants.  
> Many other use cases will be applicable in the future.  
> We want to sell customers an encapsulated hardware unit the size of a mini-pc or smaller, which they can deploy to their local network. The unit will ingest from 1 to 10 1920x1080 h264 and h265 video streams, perform object detection in near real time, and provide a web UI displaying analyzed images.
> 
> To suit our use case, we were evaluating entry-level Coral and Hailo AI accelerator cards.  
> It came to our attention that Hailo is based in Israel, which severely limits our ability to do business with you.  
> Our company values universal human rights and the laws of our country, which means we cannot support Israel or Israeli businesses while Israel continues its genocide and massacre of civilians in Palestine.  
> Please support efforts to end the genocide and reach a lasting peace agreement, so that our company may have the opportunity to purchase your products.
> 
> Best regards,  
> Jacob Ludvigsen

## Nødvendig maskinvare
Oppdager at maskinlæringsakseleratorer gjerne er spesialisert til én type modellarkitektur, selv om de kan støtte flere ulike rammeverk.
[YOLO](https://yolov8.org/yolov8-architecture/) er basert på CNN arkitekturen, imens [RT-DETR](https://arxiv.org/pdf/2304.08069.pdf) er basert på Transformers arkitekturen. Movidius Myriad-2 akseleratoren er god på DFF arkitekturen, imens Coral akseleratorene er gode på CNN og RNN. [TRON](https://arxiv.org/pdf/2303.12914.pdf) akseleratoren er best på Transformers arkitekturen. Designet for en annen [FPGA-basert akselerator for Transformer-arkitekturen](https://arxiv.org/abs/2304.03986) er tilgjengelig under MIT-lisensen.

[Denne artikkelen](https://openreview.net/pdf?id=PibYaG2C7An)  sammenlikner kjøring av transformer-modeller på et utvalg "edge" ML akseleratorer. De kom frem til at Coral Edge TPU funker greit for Transformer-arkitekturen, dersom man erstatter operasjoner i den spesifikke modellen som ikke støttes av akseleratoren; med operasjoner som er støttet. Dette er utenfor prosjektets fokusområde, og vil derfor ikke bli etterfulgt. 

Forsøkte en ordinær konvertering av RT-DETR modell i Pytorch-format til TensorFlow Lite ved hjelp av ultralytics, men konerteringen feilet fordi enkelte pytorch-operasjoner ikke har et konverteringsmål implementert i ONNX.  
Åpner et issue om det på [Ultralytics sitt repo](https://github.com/ultralytics/ultralytics/issues/9226).


# Emne 2: Utvikling
Undersøker hvorfor forhåndstrent RTDETR brukt via SAHI kun gir ut tall for gjenkjente klasser, heller enn navn slik YOLOv8 gjør under samme omstendigheter. For å finne ut av det, analyserer jeg samme bildet med begge modellene, og inspiserer `result.object_prediction_list` objektet. Der ser jeg at RTDETR har bare tall for både klassens id og navn, og yolov8 har klassens leselige navn som dens navn.

```python
>>> detection_model = AutoDetectionModel.from_pretrained(model_type="yolov8", model_path="models/yolov8x.pt", confidence_threshold=0.2)
>>> result = get_sliced_prediction("datasets/Cats.v2i.yolov8/lynx/images/501_png.rf.562f61b679a6e82fcdb4c7cb0006b538.jpg", detection_model)
Performing prediction on 1 number of slices.
>>> result.object_prediction_list
[ObjectPrediction<
    bbox: BoundingBox: <(22.01556396484375, 115.84844970703125, 612.646484375, 583.4599609375), w: 590.6309204101562, h: 467.61151123046875>,
    mask: None,
    score: PredictionScore: <value: 0.9248048663139343>,
    category: Category: <id: 15, name: cat>>]
>>> detection_model = AutoDetectionModel.from_pretrained(model_type="rtdetr", model_path="models/rtdetr-l.pt", confidence_threshold=0.2)
>>> result = get_sliced_prediction("/var/mnt/data/jacob/git/farm-animal-detection/datasets/Cats.v2i.yolov8/lynx/images/501_png.rf.562f61b679a6e82fcdb4c7cb0006b538.jpg", detection_model)
Performing prediction on 1 number of slices.
>>> result.object_prediction_list
[ObjectPrediction<
    bbox: BoundingBox: <(55.75307846069336, 114.40850830078125, 599.880126953125, 581.2420043945312), w: 544.1270484924316, h: 466.83349609375>,
    mask: None,
    score: PredictionScore: <value: 0.9240108132362366>,
    category: Category: <id: 15, name: 15>>]
```

Videre undersøkelse viser at dette også er tilfelle i Ultralytics biblioteket. [Åpner derfor et issue](https://github.com/ultralytics/ultralytics/issues/9238) i repoet deres, om å implementere navn fra COCO også på den forhåndstrente RTDETR modellen.

```python
>>> from ultralytics import RTDETR
>>> model_rtdetr = RTDETR('models/rtdetr-l.pt')
>>> model_rtdetr.names
{0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9', 10: '10', 11: '11', 12: '12', 13: '13', 14: '14', 15: '15', 16: '16', 17: '17', 18: '18', 19: '19', 20: '20', 21: '21', 22: '22', 23: '23', 24: '24', 25: '25', 26: '26', 27: '27', 28: '28', 29: '29', 30: '30', 31: '31', 32: '32', 33: '33', 34: '34', 35: '35', 36: '36', 37: '37', 38: '38', 39: '39', 40: '40', 41: '41', 42: '42', 43: '43', 44: '44', 45: '45', 46: '46', 47: '47', 48: '48', 49: '49', 50: '50', 51: '51', 52: '52', 53: '53', 54: '54', 55: '55', 56: '56', 57: '57', 58: '58', 59: '59', 60: '60', 61: '61', 62: '62', 63: '63', 64: '64', 65: '65', 66: '66', 67: '67', 68: '68', 69: '69', 70: '70', 71: '71', 72: '72', 73: '73', 74: '74', 75: '75', 76: '76', 77: '77', 78: '78', 79: '79'}


>>> from ultralytics import YOLO
>>> model_yolo = YOLO('models/yolov8x.pt')
>>> model_yolo.names
{0: 'person', 1: 'bicycle', 2: 'car', 3: 'motorcycle', 4: 'airplane', 5: 'bus', 6: 'train', 7: 'truck', 8: 'boat', 9: 'traffic light', 10: 'fire hydrant', 11: 'stop sign', 12: 'parking meter', 13: 'bench', 14: 'bird', 15: 'cat', 16: 'dog', 17: 'horse', 18: 'sheep', 19: 'cow', 20: 'elephant', 21: 'bear', 22: 'zebra', 23: 'giraffe', 24: 'backpack', 25: 'umbrella', 26: 'handbag', 27: 'tie', 28: 'suitcase', 29: 'frisbee', 30: 'skis', 31: 'snowboard', 32: 'sports ball', 33: 'kite', 34: 'baseball bat', 35: 'baseball glove', 36: 'skateboard', 37: 'surfboard', 38: 'tennis racket', 39: 'bottle', 40: 'wine glass', 41: 'cup', 42: 'fork', 43: 'knife', 44: 'spoon', 45: 'bowl', 46: 'banana', 47: 'apple', 48: 'sandwich', 49: 'orange', 50: 'broccoli', 51: 'carrot', 52: 'hot dog', 53: 'pizza', 54: 'donut', 55: 'cake', 56: 'chair', 57: 'couch', 58: 'potted plant', 59: 'bed', 60: 'dining table', 61: 'toilet', 62: 'tv', 63: 'laptop', 64: 'mouse', 65: 'remote', 66: 'keyboard', 67: 'cell phone', 68: 'microwave', 69: 'oven', 70: 'toaster', 71: 'sink', 72: 'refrigerator', 73: 'book', 74: 'clock', 75: 'vase', 76: 'scissors', 77: 'teddy bear', 78: 'hair drier', 79: 'toothbrush'}
```

Finner to relaterte issues: [#3563](https://github.com/ultralytics/ultralytics/issues/3563) og [#4092](https://github.com/ultralytics/ultralytics/issues/4092).


Implementerer [typene fra WebPush-py programvarebiblioteket](https://github.com/delvinru/webpush-py/blob/main/webpush/types.py), for å slippe å importere biblioteket i programvaren.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
