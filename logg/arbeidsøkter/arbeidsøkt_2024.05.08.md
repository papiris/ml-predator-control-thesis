# Arbeidsøkt timelogg

### Dato: 08.05.2024
### Tidsrom: 11:00 - 17:00, 23:00 - 00:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Introduksjon, Litteraturstudie, System og Risikoanalyse.
Fokus på seksjonene Målsetninger, Kritisk suksessfaktor, Arktiektur

Søker opp og setter inn referanser og vedlegg.

Begynner så smått å lage arkitekturdiagram for programvaren med "Diagram as Code"-metoden. Bruker [Diagrams](https://diagrams.mingrammer.com/docs/nodes/c4)


# Emne 2: Utvikling/Trening av maskinlæringsmodell
Sjekker lovligheten av skriptet for skraping av NINAs database, og bruk av bildene deres i prosjektet. Det ser ut til at jeg er innafor.

https://lovdata.no/lov/2018-06-15-40/§6 om bearbeiding av opphavsrett-beskyttet materiale

https://lovdata.no/lov/2018-06-15-40/§24 om databaser

https://lovdata.no/static/NLX3/31996l0009.pdf EUs databasedirektiv som sier "kopiering og bruk av lukkede databaser i vitenskapelig forskning er OK" i kapittel 2, artikkel 6 punkt 2(b)

Sender epost til veileder Halldor for å få hans syn på saken.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
