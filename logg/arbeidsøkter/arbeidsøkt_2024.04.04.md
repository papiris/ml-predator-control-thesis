# Arbeidsøkt timelogg

### Dato: 04.04.2024
### Tidsrom: 09:30 - 12:00, 17:00 - 19:00
### Sted: UiT campus Bodø, Makerspace Bodø

---


# Emne 1: Prosjektstyring
## Forretningsutvikling
Setter ballen igang for å meisle ut veien videre etter bacheloroppgaven er presentert.  
Jeg har et enkeltpersonforetak, og ønsker å tjene penger på Predalert. Én mulighet jeg undersøker er å starte et samvirke for utvikling og salg av fri programvare og -maskinvare, og tilhørende tjenester. For dette trengs én person som kan økonomi, administrasjon og forretningsutvikling; og gjerne én til utvikler som fordelaktig har erfaring med webutvikling.  

- Sender en epost til vennen min Ørjan Pettersen og spør om han vet om noen som kan være interesserte i å bli med på reisen.  
- Sender en epost til Inkubator Salten, hvor jeg tidligere har hatt et prosjekt, og forteller om dette prosjektet og behovene som trengs.


> 
> Hvert av de siste 7 årene har 16 000 får beviselig blitt tatt av freda rovvilt, hvorav en god del av dem angripes inne i gjerdet sitt. Det er ofte vanskelig å bevise at et dødt dyr er tatt av freda rovvilt, og ikke tatt av annet rovvilt eller naturlig død. De reelle tallene er derfor større.
> Jeg utvikler Fri Programvare som bruker overvåkningskameraer, objektgjenkjenning og diverse alarmintegrasjoner for å avverge, og i verste fall dokumentere og varsle om, tap av husdyr til rovvilt.
> 
> En ulempe ved overvåkningskameraer er at de ironisk nok er notoriske for sine slapphendte holdninger til datasikkerhet. Flere ganger bare hittil i år har millioner av brukere av slike nettverkstilkoblede kameraer blitt utsatt for databrudd av diverse karakter. Dette prosjektets tilnærming isolerer disse kameraene fra resten av nettverket, og gjør at all deres kommunikasjon med omverdenen kun kan nå de spesifikke enhetene brukeren tillater.
> Prosjektet gjør det unødvendig å stole på noen tredjeparter, inkludert programvareutvikler/tilbyder.
> 
> Prosjektet er min bacheloroppgave i maskinteknikk ved UiT Narvik; og har hovedfokus på tillitsbygging i digitalisering og IoT gjennom transparens og digital sjølråderett.
> 
> Prosjektdokumentasjonen er tilgjengelig her: https://gitlab.com/papiris/ml-predator-control-thesis
> Programvaren utvikles her: https://gitlab.com/papiris/predator-detect-and-notify
> 
> Case-studie og nærmeste partner i bacheloroppgaven er Strandmoa Gård i Steigen, hvor fire sau ble tatt av gaupe i vinter. 
> 
> For å redusere "time to market" og øke sannsynligheten for suksess; søker jeg en partner som vil bli med på reisen videre, og kan ta seg av økonomi og administrasjon, samt delta i forretningsutviklinga. Én til utvikler med fokus på web vil også bidra sterkt til suksess.  
> 
> Mvh Jacob Ludvigsen  
> Ludvigsen Ingeniørskap og Fri Teknologi  
> org.nr 932 966 905  

## Veiledningsmøte
Forbereder til veiledningsmøte i morgen.


# Emne 2: Rapportskriving
TexStudio vil ikke lengre kompilere LaTex-dokumentet Sluttrapport, så jeg undersøker den oppdaterte UiT-thesis malen. Finner ikke løsningen, så jeg installerer en eldre versjon av texlive.  Dette hjelper heller ikke, men det hjalp å bruke TexMaker istedenfor TexStudio eller Kile; selv om tex-kommandoene de bruker tilsynelatende er identiske.  

Produserer en graf over de 3 nærmeste leddene av eksterne programvarebiblioteker som brukes i Predalert, ved å bruke pakken `pydeps`.  
`pydeps predalert --max-bacon=3 --cluster --no-show`


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
