# Arbeidsøkt timelogg

### Dato: 14.03.2024
### Tidsrom: 08:00 - 17:00, 18:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Prototyping
## Utvikler Predalert

### Individuelle videostrømmer
Gjengi individuelle bildestrømmer til nettstedet.

 web_ui:  
 Lag en rute dynamisk for hver kø i q_result_dict.
 Bruk Starlette direkte i stedet for Webgear for å håndtere nettresponser,
 med en templateResponse som sender ruter for videoer til index.html.  
 Dette lar webappen tilpasse seg antall tilgjengelige strømmer,
 og åpner for interaksjon med individuelle strømmer (som fullskjerming).
 Dette var ikke mulig når web_ui konsumerte q_img_grid_web.

 templates/index.html:  
 Bruk Jinja2 til å dynamisk lage < img >-tagger for ruter
 mottatt fra Starlette.
 Hver tag får en unik ID, som kan refereres til av static/custom.js
 for å gjøre elementet i fullskjerm.

 __main__.py:  
 Ikke start make_image_grid hvis local_ui er deaktivert.
 Dette sparer ressurser.
 
 ### Web push varsler
 webpush krever at applikasjonen har et gyldig SSL/TLS sertifikat. Dette fordi det innebærer å varig installere en bit javascript på klient-nettleseren, som kan kjøres uten at nettleseren er åpen. Disse små skriptene kalles ServiceWorker.  
Et sertifikat for lokal utvikling genereres ved hjelp av [mkcert](https://github.com/FiloSottile/mkcert), og jeg får til å kjøre predalert-nettsida i kryptert tilstand. Dessverre stoler ikke Firefox på sertifikatet, fordi utstederen er ukjent.  
For å komme rundt dette, kjøres mkcert som rotbruker på dette viset:
```
sudo mkcert -install
sudo mkcert --key-file local_key_keep_secret.pem --cert-file local_cert_keep_secret.pem localhost
```

Fordi disse filene gir alle med tilgang, mulighet til å avskjære og omdirigere all traffik på pc'en, må de lagres trygt og kun brukes til lokal utvikling.
 
 I tillegg må applikasjonen åpnes i et privat Firefox-vindu for å omgå diverse innebygde og "nettleser-tillegg" sikkerhetstiltak.
 
Web Push krever at man bruker en varig backend-database for å lagre bruker-kredentialer; for å kunne fortsette å sende webpush varsler til brukerne etter programmet har blitt restartet. Ser derfor nærmere på bruk av databaser i Starlette, hovedsaklig `gino` som er asynkron og `SQLAlchemy` som er synkron og dermed ikke håndterer asynkrone endepunkter (dvs flere klientenheter tilkoblet på samme tid).
 
# Emne 2: Prosjektstyring
Blir med i [Norsk Programmering på Discord]( https://discord.gg/norskprogrammering) for å lufte prosjektet, få hjelp med problemstillinger som nettsøk ikke løser, og søke samarbeidspartnere.
 
<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
