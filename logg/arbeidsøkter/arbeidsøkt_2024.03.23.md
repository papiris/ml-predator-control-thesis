# Arbeidsøkt timelogg

### Dato: 23.03.2024
### Tidsrom: 09:00 - 15:00, 21:00 - 00:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
## Migrere fra Webgear og Starlette til FastAPI
Prosjektet bruker egentlig ikke WebGear noe særlig, derfor implementerer jeg den funksjonaliteten som faktisk brukes selv, og fjerner bruken av WebGear.  
Med dette grepet åpnes porten for bedre prosjektstruktur, bruk av FastAPI og bedre prosesskontroll.

FastAPI bygger på Starlette og Pydantic, så det eneste som trengs for å bytte over fra Starlette er å fastsette _typer_ på input i funksjoner, og endre importnavn fra starlette.* til fastapi.*.
Funker bra!

Gjør også endel andre småting, som kan ses på repoet denne dagen.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
