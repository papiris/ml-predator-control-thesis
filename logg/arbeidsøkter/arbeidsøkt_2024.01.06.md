
# Arbeidsøkt timelogg

### Dato: 06.01.2024
### Tidsrom: 16:00 - 18:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; innledning, bakgrunn, mål

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
