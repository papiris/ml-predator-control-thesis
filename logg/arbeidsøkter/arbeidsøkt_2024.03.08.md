
# Arbeidsøkt timelogg

### Dato: 08.03.2024
### Tidsrom: 10:00 - 17:00
### Sted: Strandmoa Gård, Steigen

----

# Emne 1: Konseptutvikling

## FOSOPAS
Separere relaterte funksjoner ut fra hovedskriptet, til hver sine filer. Dette gjør det lettere å laste inn kun den koden som skal brukes, og gjør kodemengden og -kontekstene man må forholde seg til under utvikling mer håndterbar.  
Fjerne død kode.  

Implementere sentral logging-fasilitet; som håndterer logg-beskjeder fra alle prosesser og tråder på en trygg måte. Dette oppnås med Pythons `multiprocessing.Manager.Queue` objekt. Én loggkø lages i hovedskriptet, og deles med alle underprosesser. Alle prosesser produserer logg-beskjeder til køen, og én enkelt prosess konsumerer logg-beskjedene i køen og printer dem til terminalen etter beskjedens logg-nivå.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
