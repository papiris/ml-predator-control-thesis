# Arbeidsøkt timelogg

### Dato: 11.04.2024
### Tidsrom: 09:00 - 14:00, 19:00 - 23:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Videreutvikler minimumseksempelet for OpenZiti, ved å implementere `media_type="multipart/x-mixed-replace ` i `StreamingResponse` for tekst. Det funker halvveis.

Oppdager at `multipart/x-mixed-replace` ikke lengre er offisielt støttet i Firefox og Chrome, og at dette antageligvis ikke er veien å gå.

Oppdager at websocket protokollen er avslått på Ziti Routeren jeg bruker i Netfoundy, så den protokollen kan ikke brukes.

Er godt igang med å implementere fungerende Server-Sent Events (SSE) i det minimale eksempelet, når jeg oppdager at maksimalt antall kilder hvert domene kan lytte på når protokollen HTTP/1.1 og ikke HTTP/2 brukes; er 6. [Dokumentasjon MDN](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events)  
[Uvicorn](https://www.uvicorn.org/#introduction) har bare støtte for HTTP/1.1, men [Hyprcorn](https://hypercorn.readthedocs.io/en/latest/index.html) støtter HTTP/2 i tillegg.

Alle nettlesere krever TLS / SSL for å bruke HTTP/2 protokollen.

Går derfor over til å bruke hypercorn istedenfor uvicorn. Dokumentasjonen er mye mer tungtlest enn for uvicorn, så det tar litt tid.

Veikartet blir da:  
1. Sende bilder som base64 kodet tekst via SSE over openziti  
2. SSL over openziti (via SPIFFE?)  
3. All funksjonalitet over openziti  

I dag har jeg løst 1.







<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
