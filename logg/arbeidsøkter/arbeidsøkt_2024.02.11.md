
# Arbeidsøkt timelogg

### Dato: 11.02.2024
### Tidsrom: 12:20 - 18:50
### Sted: Makerspace Bodø, Bodø

---


# Emne 1: litteraturstudie

## Klimagassutslipp fra maskinlæring
Grep som kan redusere karbonavtrykk fra trening og bruk av maskinlæringsmodeller:
1. Bruk maskinvare laget spesifikt for trening og kjøring av maskinlæringsmodeller. Slik maskinvare gjør prosesseringen 2x-5x mer effektivt enn bruk av generelle prosessorer.
2. Velg effektiv arkitektur og modell.
3. Tren og kjør modellen i et moderne datasenter. Datasentre har mer effektiv maskinvare enn de fleste hjemmeservere, og kan spare 1.4x-2x energi.
4. Tren og kjør modellen et sted med klimavennlig energiproduksjon.

[[1]](https://blog.research.google/2022/02/good-news-about-carbon-footprint-of.html)


Ultralytics publiserer detaljert statistikk på plattformen de bruker for å trene YOLO-modellene.  
Det tok fire dager og fire timer, altså 100 timer å trene YOLOv8m på COCO datasettet. I den perioden brukte Nvidia A100 GPU-en gjennomsnittlig 300W.
[[2]](https://wandb.ai/glenn-jocher/YOLOv8/runs/oxwuolug?workspace=user-)  
YOLOv8m har 25.9 millioner parametere.  
COCO har 330 000 bilder, hvorav 200 000 inngår i treningsmateriale og resterende i testingsmateriale.

Glenn Jocher i Ultralytics skriver at treningstid skalerer lineært med modellparametere og bilder i datasettet. For å finne strømforbruket av å trene en YOLOv8x modell på et mindre datasett på 24 000 bilder (3000 bilder per dyreart);  
Skaleringsfaktor datasett: 24,000 / 200,000 = 0.12  
Skaleringsfaktor parametere: 68.2 millioner / 25.9 millioner = ca 2.634  

Strømforbruk for den større modellen på vårt datasett = (treningstid for den mindre modellen) \* (skaleringsfaktor for parametere) \* (skaleringsfaktor for datasett)  
= 30kWh \* 2.634 \* 0.12  
**= 9.48kWh**

Den fransk-kanadiske datasenter-virksomheten OVH sier deres datasentre har et karbonavtrykk på 0.18kg CO2e / kWh IT-forbruk.

Karbonavtrykket av å trene modellen blir da **1.71kg CO2e.**

[[3]](https://corporate.ovhcloud.com/en/sustainability/environment/)


## Karbonavtrykk fra husdyr
Forholdet mellom kjøtt og protein i <https://journals.plos.org/climate/article?id=10.1371/journal.pclm.0000010> er 0,23 for storfe og 0,21 for får.

Det ikke-annualiserte karbonavtrykket for kjøtt, fra deres datakilde GLEAM:
- får: 19,1kg CO2e / kg kjøtt
- fe: 35,8kg CO2e / kg kjøtt

Det ikke-annualiserte karbonavtrykket av et får med kjøttvekt etter slakt på 10kg blir da 191kg CO2e.


[[4]](https://foodandagricultureorganization.shinyapps.io/GLEAMV3_Public/)



## Sammenstille litteraturstudie
Skrive mer på kapittel 5 Miljøbelastning



[1] https://blog.research.google/2022/02/good-news-about-carbon-footprint-of.html  
[2] https://wandb.ai/glenn-jocher/YOLOv8/runs/oxwuolug?workspace=user-  
[3] https://corporate.ovhcloud.com/en/sustainability/environment/

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
