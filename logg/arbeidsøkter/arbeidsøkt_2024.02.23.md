
# Arbeidsøkt timelogg

### Dato: 23.02.2024
### Tidsrom: 10:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Publiserer [et innlegg](https://openziti.discourse.group/t/using-openziti-in-distributed-surveillance-system/2135) på OpenZitis forum, der jeg forteller om prosjektet og ber om råd til oppsett av OpenZiti. Kontaktes via personlig melding av Philip Griffith, én av OpenZitis vedlikeholdere og ansvarlig for produkter ment for eksterne. Han forklarer noen konsepter jeg slet med å forstå, og viste meg nyttige ressurser. Jeg forteller om en programvarelus i oppsetts-veiledninga deres. Vi avtaler et videomøte tirsdag 27.02, klokka 11:00 - 11:25; med formål å få meg i gang med å bruke OpenZiti i prosjektet, og overføre kunnskap.

Mottar også en hjelpsom kommentar på innlegget, som hjelper meg videre.

## Moonfire-NVR
Åpner et [issue om integrert støtte for zero-trust](https://github.com/scottlamb/moonfire-nvr/issues/308) i Moonfire.


## Blue-candle
[Blue-candle](https://github.com/xnorpx/blue-candle) er en frittstående modul for objektgjenkjenning, skrevet i Rust, som utvikleren har [uttrykt støtte for å integrere i Moonfire](https://github.com/scottlamb/moonfire-nvr/issues/30#issuecomment-1873575896).  
Jeg tilbyr min hjelp i [et issue i Blue-Candle](https://github.com/xnorpx/blue-candle/issues/15) om støtte for RT-DETR modellen.


## Frigate
Jeg setter opp og kjører Frigate på k3s klyngen lokalt på laptopen min, for å teste funksjonalitet. Møter noen problemer ved gjentatte konfigurasjonsendringer.  
Frigate har støtte for maskinvareakselerasjon av ML på Nvidia-maskinvare med TensorRT, så jeg prøver å sette det opp.
- setter opp [nvidia-container-runtime]()
- Gjør nvidia tilgjengelig for k3s ved å følge [nvidias anvisninger](https://github.com/NVIDIA/k8s-device-plugin) og [k3s sine anvisninger](https://docs.k3s.io/advanced?_highlight=container#nvidia-container-runtime-support)
- Får eksportert pytorch-modellen RT-DETR til ONNX og TensorRT via ultralytics, ved å skape et virtuelt miljø med python3.11, og installere + oppgradere disse pakkene hver for seg i miljøet: <code> pip-24.0, ultralytics-8.1, onnx-1.15, torch-2.2.1-cp311, tensorrt-8.6.1-post1 </code>

Lyktes ikke med å få noen slags objektgjenkjenning til å fungere.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
