# Arbeidsøkt timelogg

### Dato: 16.05.2024
### Tidsrom: 09:00 - 13:00, 18:00 - 24:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapitlene System, Litteraturstudie og Verktøy, og finner referanser.

Lager system- og konteinerdiagrammer med Python-pakken `diagrams`.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
