# Arbeidsøkt timelogg

### Dato: 22.05.2024
### Tidsrom: 09:00 - 15:00,  16:00 - 21:00, 00:00 - 00:30
### Sted: Håsand Gård og Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Lager tabeller for utvelgelseskriterier

Bruker 2  timer på å finne ut hvordan man viser en forkortelse som også har en forklaring i latex. (hint, \newdualentry)

Skriver om datasikkerhet, avverging, objektgjenkjenning og videre arbeid


# Emne 2: Testing (30min)
Under kveldsfôring på Strandmoa Gård blir jeg oppmerksom på gaupehyl bortenfor ene sauegjerdet, 100-150 meter unna. Jeg tar med Border Collie bikkjene våre, og går mot lyden. Det viser seg at gaupa er bak lauvskog på andre siden av Stamsvikelva, som renner langsmed sauegjerdet. Jeg oppfordrer bikkjene til å bjeffe, og de setter i et álo. Gaupa slutter å hyle etter noen minutter.  
Jeg vasser over elva med den lydigste av bikkjene, og starter sporing. Vi ser at gaupa har vandret rundt på platået og daltoppen, og langs etter elvebredden. Når vi mister sporet, har gaupa stukket seg opp mot fjellet, i motsatt retning av hundebjeffene. Den har trolig vært på leting etter et sted å krysse elva, kanskje for å angripe sauene.

Hundebjeff ser ut til å ha en viss effekt på gaupe.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
