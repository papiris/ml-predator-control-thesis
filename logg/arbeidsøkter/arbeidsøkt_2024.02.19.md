
# Arbeidsøkt timelogg

### Dato: 19.02.2024
### Tidsrom: 22:00 - 22:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Sammenlikning av Tailscale, Nebula, Netmaker og ZeroTier
Defined Networks, som utvikler og selger Nebula, har publisert en tilsynelatende nøytral [sammenlikning](https://www.defined.net/blog/nebula-is-not-the-fastest-mesh-vpn/) av sitt produkt og konkurrentenes produkter. De siste fem årene har de jevnlig målt ytelsen av produktene under ulike forhold. Testenes grensebetingelser er kjente, innstillingene for de ulike produktene og testenes rådata skal publiseres [i et eget repo på Github.](https://github.com/slackhq/nebula)

Leser en tredjedel av sammenlikningen.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
