
# Arbeidsøkt timelogg

### Dato: 28.01.2024
### Tidsrom: 18:00 - 20:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Gjør ferdig prosjektbeskrivelse; Risikovurdering, Aktivitetsbeskrivelser, Gjennomføring, Milepæler, Fremdriftsplan

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
