# Arbeidsøkt timelogg

### Dato: 13.03.2024
### Tidsrom: 08:00 - 10:00, 11:30 - 13:30, 16:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Prosjektstyring 

## Statusrapport 
Skriver ferdig statusrapport.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
