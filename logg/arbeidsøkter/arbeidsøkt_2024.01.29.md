
# Arbeidsøkt timelogg

### Dato: 29.01.2024
### Tidsrom: 14:00 - 15:00
### Sted: Strandmoa Gård, Steigen

-----

# Emne 1: Prosjektstyring, loggføring
Skrive logg for tidligere arbeidsøkter

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
