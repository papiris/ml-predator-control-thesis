# Arbeidsøkt timelogg

### Dato: 04.01.2024
### Tidsrom: 12:00 - 16:00
### Sted: Bodø

-----

# Emne 1: Prosjektstart
I diskusjon med emneansvarlig Øyvind Søraas, avtaler vi at jeg skal skrive oppgave om kameraovervåkning og maskinlæring i rovviltforvaltningen.
Basert på mitt tidligere arbeid med [Farmers' Open Source Optical Predator Alert System](https://hackaday.io/project/191509-farmersopen-source-optical-predator-alert-system).
Begynner å skrive prosjektbeskrivelse


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 06.01.2024
### Tidsrom: 16:00 - 18:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; innledning, bakgrunn, mål

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 10.01.2024
### Tidsrom: 10:00 - 13:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; bakgrunn, mål

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 25.01.2024
### Tidsrom: 12:00 - 15:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; mål, organisering, kravspesifikasjon, kvalitetsstyring

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 26.01.2024
### Tidsrom: 09:00 - 20:00, pause 16:00 - 18:00
### Sted: Steigen, Strandmoa Gård


-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; Kvalitetsstyring, Gjennomføring, Aktivitetsbeskrivelser, Fremdriftsplan.


-----

# Emne 2: Presentere prosjektbeskrivelse
prosjektbeskrivelsen er ikke ferdig enda når jeg har presentasjon kl 14:20, og jeg har ikke begynt på en presentasjon.
Derfor presenterer det jeg har ferdig til da.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 27.01.2024
### Tidsrom: 12:00 - 17:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; Pusser på mål, organisering, kravspesifikasjon, kvalitetsstyring, fremdriftsplan

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 28.01.2024
### Tidsrom: 18:00 - 20:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Gjør ferdig prosjektbeskrivelse; Risikovurdering, Aktivitetsbeskrivelser, Gjennomføring, Milepæler, Fremdriftsplan

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 29.01.2024
### Tidsrom: 14:00 - 15:00
### Sted: Strandmoa Gård, Steigen

-----

# Emne 1: Prosjektstyring, loggføring
Skrive logg for tidligere arbeidsøkter

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 30.01.2024
### Tidsrom: 12:00 - 19:00, pause 14:00 - 16:00
### Sted: Strandmoa Gård, Steigen

-----

# Emne 1: Prosjektstyring
- Lisensiere prosjektdokumenter under CC-BY-SA 4.0, fordi åpen forskning er bra.
- Begynne å bruke git versjonskontroll av prosjektdokumenter, og publisere dem på https://gitlab.com/papiris/ml-predator-control-thesis


# Emne 2: Prosjektbeskrivelse
- Legge til sammendrag


# Emne 3: Litteraturstudie
Lese om eksisterende løsninger innenfor samme område.
Frigate, Zoneminder, Shinobi, Moonfire NVR, Viseron


## Zoneminder
Intro: Antageligvis den mest velutprøvde og "modne" fri programvaren for kameraovervåkning og objektgjenkjenning.
- Stort og aktivt utviklersamfunn, og etablerte sikkerhetspraksiser.
- Enormt bibliotek av støttede kameraer. Dokumentasjonen er utdatert, og installasjon litt vanskelig.
- Skrevet i PHP, Perl, C++, javascript, hvorav jeg har erfaring med ingen av dem.
- lisensiert under GPL-2.0-or-later. Dette gjør det mulig å endre lisens til AGPL-v3-or-later, men det ser ikke ut til å ha vært diskusjon om det før nå.
- Har API med standard protokoller (Websocket, MQTT, perl) for triggering av eksterne skripter ved hendelser
- Har innebygget alarmfunksjonalitet
- Har tilhørende app, som fungerer på Android, IOS, Windows, Mac og Linux. Er lisensiert GPL-v3: https://zmninja.zoneminder.com/
- Hjemmeside: https://zoneminder.com/
- Maskinlærings-modul: https://github.com/ZoneMinder/zmeventnotification
- Etter sigende begynner arkitekturen å vise sin alder
- Har et selskap i førersetet, som tilbyr betalt support og kontraktarbeid
- Støtter konteinerisering


## Shinobi
Intro: Relativt ny på banen, startet i 2016. Opererer med en progressiv lisensmodell, som låser brukeren til selskapet.
- Ikke Fri programvare lisens: https://gitlab.com/Shinobi-Systems/Shinobi/-/blob/dev/COPYING.md
-- Jeg åpnet en tråd om det på repoet deres: https://gitlab.com/Shinobi-Systems/Shinobi/-/issues/506
- Skrevet i Nodejs (javascript)
- Krever minimum $6/mnd for mulighet til å tilkoble 5 kameraer og 1 mobilapp.
Progressiv lisenskostnad, opptil $600/mnd for 1000 kameraer.
Har også betalingsmur for avansert funksjonalitet, som kryptert fjerntilgang uten personlig VPN.
- Har et selskap i førersetet, som tilbyr betalt support og kontraktarbeid
- Har API-er
- Har innebygget alarmfunksjonalitet


## Frigate
Intro: konteiner-native, ML-by-default, sterkt fokus på integrering med Home-Assistant og MQTT
- lisensiert under MIT
- bruker WebRTC
- Tilbyr egen datasett-annoterings + modelltreningstjeneste, koster $50 / år: https://frigate.video/plus/
- Støtter Coral ML-akselerator m/ tensorflow lite
- Støtter Nvidia TensorRT maskinvareakselerasjon for YOLO modeller
- Støtter avlasting av maskinlærings objektgjenkjenning over nettverk via Deepstack / Codeproject.ai
- repo: https://github.com/blakeblackshear/frigate
- dokumentasjon: https://docs.frigate.video


## Moonfire NVR
Intro: Ungt prosjekt, har ikke publisert 1.0 enda, selv om tidligste commit var i 2016. Lite utviklet, men under utvikling. Spennende.
- Lisensiert under GPL-v3
- Skriver .h264 videostrømmer direkte til disk, uten omkoding. Dette gjør det svært effektivt og ressursgjerrig i bruk.
- Skrevet i Rust og Typescript
- Svært lite prosjekt, men en god del større enn Furbinator
- Har ikke innebygget Maskinlærings objektgjenkjenning, men det er planlagt.
- Jeg har preferanse for Rust og Python, så oppstrøms bidrag er en mulighet.
- God dokumentasjon, for de få brukstilfellene og funksjonene som finnes per nå.
- Har ikke en app, og nettgrensesnittet kan kun brukes til visning.
- Sikkerhet er ikke enkelt: https://github.com/scottlamb/moonfire-nvr/blob/master/guide/secure.md
-- Må enten implementere https-støtte i inkludert webserver, bytte ut inkludert webserver, eller proxy til en webserver som støtter https.
-- Kan settes opp med traefik foran seg, med autentisering, i så måte?
- støtter konteinerisering
- Støtter per nå ikke Coral ML-akselerator m/ tensorflow lite
- repo: https://github.com/scottlamb/moonfire-nvr


## Surrey man; Furbinator 3000
Intro: Basic sak, men i grunn nesten akkurat hva jeg er ute etter.
- Bloggpost: https://medium.com/@james.milward/deterring-foxes-and-badgers-with-tensorflow-lite-python-raspberry-pi-ring-cameras-ultrasonic-75b3160faa3cs
- Hovedkode: https://github.com/Jimbwlah/tensorflow-ring-animal-detector
- Arduinokode: https://github.com/Jimbwlah/furbinator3000
- Ikke AGPL-kompatibel for nå, siden prosjektet mangler lisens.
-- Jeg åpnet en tråd om AGPL-v3 kompatibel lisens på repoet: https://github.com/Jimbwlah/tensorflow-ring-animal-detector/issues/1
- Ingen support
- Ingen utviklersamfunn rundt
- Simpel sak, simpelt oppsett
- helmanuelt oppsett, ingen integrert ONVIF kamera-bibliotek
- Støtter ikke Coral ML-akselerator m/ tensorflow lite


## Viseron
Intro: Startet for 4 år siden, basert på moduler. Skryter av brukervennlighet.
- Skrevet i Python og Typescript
- Lisensiert under MIT
- konteinerisering aktivit støttet
- Mange tilgjengelige moduler
- ingen commits siste 4 måneder, ingen store commits siden juli 2023 ==> treg utvikling
- Har flere moduler for objektgjenkjenning med maskinlæring
- Repo: https://github.com/roflcoopter/viseron
- Hjemmeside: https://viseron.netlify.app/


## MotionPlus / MotionEye
Intro: Gammel og moden applikasjon. Nylig renovert.
- Lisensiert under GPL-v-3-or-later
- Hjemmeside: https://motion-project.github.io/
- Repo backend: https://github.com/Motion-Project/motionplus
- Repo frontend: https://github.com/motioneye-project/motioneye
- Frontend er python og javascript. Backend er C++
- full https støtte, samt autentisering
- Kjør skripter ved hendelser
- inkludert støtte for maskinlærings objektgjenkjenning

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 31.01.2024
### Tidsrom: 14:20 - 16:00, 16:20 - 18:00
### Sted: Steigen Folkebibliotek, Steigen

-----


# Emne 1: Litteraturstudie, eksisterende løsninger
Lese om eksisterende løsninger innenfor samme område.


## FOSOPAS - Jacob Ludvigsen
Intro: En meget begrenset og simpel programvare, utviklet av meg siden april 2023.
- Lisensiert under AGPL-v3
- Skrevet i Python
- Laget av undertegnede
- Ingen innebygget webserver, kun lokalt på maskinen
- Ingen eksterne tjenester
- Kun varsling lokalt på maskinen
- Innebygget objektgjenkjenning med maskinlæring
- Ingen støtte for konteinerisering
- Ingen utviklersamfunn rundt
- Begrenset innebygget auto-oppdaging og konfigurering av ONVIF-støttede kameraer
- Ingen støtte for avlasting av objektgjenkjenning


# Emne 2: Litteraturstudie; Objektgjenkjennings-modeller
Lese om ulike maskinlæring modeller for objektgjenkjenning.  
Til evaluering av presisjon brukes mAP: https://towardsdatascience.com/map-mean-average-precision-might-confuse-you-5956f1bfa9e2  
Hastighet er målt på T4 GPU med filformatet TensorRT FP16

### Støttelitteratur
- https://neptune.ai/blog/object-detection-algorithms-and-libraries
- https://deci.ai/blog/yolov8-vs-yolo-nas-showdown-exploring-advanced-object-detection/
- https://www.augmentedstartups.com/blog/yolo-nas-vs-yolov8-a-comprehensive-comparison
- https://medium.com/@pankajsharma01/analyzing-the-performance-yolo-nas-and-yolo-v8-side-by-side-11406800906a

## YOLOv8
Intro: En modell jeg har erfaring med. God treffsikkerhet, kjapp.
- Utgitt i april 2022
- Lisensiert AGPL-v3.
- Mange tilgjengelige integrasjoner, som SAHI for "små glidende undersøkelsesvinduer"
- Aktivt utviklet, med 240 bidragsytere
- Har et selskap i ryggen, som tjener penger på tilleggstjenester og "enterprise" lisensiering.  
Altså at man kan betale dem for å omgå kravene til å publisere kildekoden om man gjør endringer på modellen / trener den.
- Opptil 53.9 mean Average Precision (MaP_val)
- Repo: https://github.com/ultralytics/ultralytics
- Skrvet i Python og Pytorch, men modellen kan omdannes til andre rammeverk som Tensorflow Lite, ONNX og OpenVino.
- Kan kvantiseres til F16 eller INT8, ned fra F32 i trening.
- FPS for YOLOv8-x: 50

## YOLOX
Intro: En litt eldre modell, men fortsatt bra.
- Utgitt i 2021
- Lisensiert Apache-2.0
- Ikke veldig aktivt utviklet lengre
- Opptil 51.1 MaP_val
- Repo: https://github.com/Megvii-BaseDetection/YOLOX


## YOLO-NAS
Intro: Nyere enn YOLOv8, en mix mellom yolov8 og yolov6.
- Støtter INT 8-bit kvantisering (ned fra FLOAT 32-bit i trening), med lite presisjonstap.  
Dette øker effektiviteten og hastigheten, særlig på plattformer med få ressurser.
- Ikke like presis som YOLOv8, men betydelig mindre latens.
- Opptil 52.1 mAP ved 8-bit kvantisering 
- Repo: https://github.com/Deci-AI/super-gradients
- Ultralytics tilbyr modellen integrert i deres verktøy: https://docs.ultralytics.com/models/yolo-nas

## RT-DETR
Intro: Veldig god, veldig ny. Den første sanntids ende-til-ende objektgjenkjenningsmodellen.  
Bedre mAP og FPS enn alle YOLO-modeller. State of The Art
- Utgitt i juli 2023
- Opptil 54.8 mAP
- Lisensiert Apache-2.0
- Ultralytics har integrert i deres verktøy https://docs.ultralytics.com/models/rtdetr/
- Repo: https://github.com/lyuwenyu/RT-DETR
- Papir: https://arxiv.org/abs/2304.08069
- FPS for RT-DETR-X: 74


#  Emne 3: Litteraturstudie; maskinvare
Det finnes ulike typer maskinvare egnet for å kjøre maskinlæringsmodeller.  
Vi har enkeltkorts-datamaskiner som [ASUS Tinkerboard Edge R](https://tinker-board.asus.com/series/tinker-edge-r.html) som har en ordinær prosessor og en nevral ko-prosessor,  
eksternt tilkoblede ASIC kretskort som kun har en nevral prosessor, som [Coral AI sin USB akselerator](https://coral.ai/products/accelerator);  
og PCI-e pluggbare moduler med kun nevrale prosessorer [som Coral AI sine](https://coral.ai/products/m2-accelerator-dual-edgetpu).

De ulike maskinvarene og produsentene har sine egne optimaliseringer, og ofte egne verktøy og rammeverk for å bruke dem. For å abstrahere dette bort kan man bruke Microsoft sin [Olive programvare](https://github.com/microsoft/Olive)  
Dokumentasjon for Olive: https://microsoft.github.io/Olive/overview/olive.html

Coral.ai sine PCIe akseleratorer er tilgjengelig i formfaktoren m.2 A+E og m.2 B+M. Den doble PCIe akseleratoren er bare tilgjengelig i m.2 A+E.  
B+M brukes typisk til SSD-er, A+E brukes typisk til trådløse nettverkskort.  
Adapter for å montere m.2 A+E dobbel akselerator i m.2 B+M slot: https://www.makerfabs.com/dual-edge-tpu-adapter-m2-2280-b-m-key.html


# Emne 4: Rammeverk
Oversikt over ulike rammeverk og filformater for maskinlæring. Alt er Fri programvare med mindre noe annet er skrevet.  

Rammeverk: ONNX, openvino, Tensorflow, Pytorch, CUDA, ROCM  
Filformat: Safetensors

Pytorch og Tensorflow er mest populære, og har støtte for flest funksjoner og maskinvare.

OpenVino er et rammeverk for optimalisering av diverse maskinlæringsmodeller, spesifikt for Intel maskinvare.  
CUDA er et rammeverk for kjøring av diverse maskinlæringsmodeller (dersom de implementerer CUDA støtte), spesifikt for Nvidia maskinvare. Proprietær. Generelt populær.  
ROCM er et rammeverk for kjøring av diverse maskinlæringsmodeller (dersom de implementerer CUDA støtte), spesifikt for AMD maskinvare. Lav popularitet.  
ONNX er et abstraksjonsledd mellom ulike rammeverk, og mellom rammeverk og maskinvare. Det legger opp til at de ulike rammeverkene sikter seg inn på maks støtte for ONNX, så sikter ONNX seg på maks støtte for maskinvare. Populært.

Safetensors er et filformat som sikter på tryggere lagring og deling av maskinlæringsmodeller. Enkelte andre formater er trege å laste inn, utsatt for korrupsjon, og utrygge med tanke på kjøring av innpakket, gjemt kode. Særlig utsatt er Pytorch, hvor ønsket om å unngå bruken av `Pickle` programvarebiblioteket [var hovedårsaken til at Safetensors ble utviklet.](https://github.com/huggingface/safetensors?tab=readme-ov-file#yet-another-format-) 

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 01.02.2024
### Tidsrom: 21:00 - 21:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Ingen arbeid i dag
Grunnet ekstremværet Ingunn har Trollfjord Bredbånds kunder i Steigen vært [foruten internett-tilgang i dag.](trollfjord.no/driftsmeldinger)  
Fordi så mye av litteraturstudiet avhenger av søk på nettet, bestemte jeg meg for å heller bruke dagen på ting jeg ikke trengte internett til; som ikke var prosjektrelaterte.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 02.02.2024
### Tidsrom: 12:00 - 16:20
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: litteraturstudie
Nettverkstilgang er nå gjenopprettet, og arbeidet kan fortsette.  

## Relevante standarder

### EUs Cyber Resilience Act
https://digital-strategy.ec.europa.eu/en/library/cyber-resilience-act-factsheet  
- I løpet av 2024 blir EUs Cyber Resilience Act den første loven som krever at programvare (og maskinvare med programvare-elementer) solgt i EU, skal følge standarder for utvikling, ansvar, tilgjengelighet og datasikkerhet. Produsenter må etterfølge standardene innen 36 måneder, så det er ikke umiddelbart klart at dette prosjektet må etterleve kravene.
 
### Standard for informasjonssikkerhet i datasystemer ISO/IEC 27000:2018
https://www.iso.org/standard/73906.html  
- Ikke offentlig tilgjengelig, ei heller gjennom UiTs abbonnement hos Norsk Standard.  

### NS-EN ISO/IEC 27002:2022 for Informasjonssikkerhetstiltak
- tilgjengelig gjennom UiTs abbonnement  https://online.standard.no/nb/ns-en-isoiec-27002-2022 
- Tre aspekter: Konfidensialitet, integritet, tilgjengelighet.
- Kapittel 5.8: Informasjonssikkerhet som del av prosjektstyring.  
-- Informasjonssikkerhetsrisikoer bør vurderes på et tidlig stadie, og oppdateres underveis i prosjektet. 
-- Vurdér hvilken informasjon som er involvert, hva som trengs for å beskytte den, tilgangskontroll, informering av brukere om deres ansvar og risiko, vurdering av tredjeparters pålitelighet.
- Lag oversikt over informasjonsverdier

### NS-ISO/IEC 27017 for skytjenester

### NS-ISO/IEC 27701 for personvern

### NS-ISO 21500 og 21502 for Prosjektstyring i IKT

### NS-ISO/IEC 27005 for risikostyringsprosesser

### Digitalytelsesloven
https://lovdata.no/dokument/NL/lov/2022-06-17-56  
- Paragraf 8, punkt 3: "{produktet skal} leveres med tilbehør, innpakning, installasjonsveiledning og lignende som forbrukeren med rimelighet kan forvente å få"  
- Paragraf 8, punkt 6: "{produktet skal} være upåvirket av tredjepersoners rettigheter i ytelsen, for eksempel eiendomsrett, panterett eller immaterialrettigheter." <== Hva betyr det mtp fri programvare?  
- Paragraf 9: "Leverandøren skal sørge for at forbrukeren får beskjed om og får levert oppdateringer, herunder sikkerhetsoppdateringer, som er nødvendige for å oppfylle kravene i §§ 7 og 8."  
- Ellers gjelder at man ikke kan avtale seg bort fra lovkravene, at man skal levere det man averterer og lover.  
- Paragraf 26: Krav om erstatning, retting, heving, avslag kan kun rettes mot tidligere ledd i avtalekjeden (oppstrøms fra B2C-leverandør), dersom det tidligere leddet opptrer i næringsvirksomhet.  
- Paragraf 40: Spesifikasjon om ytelsen, når kunden skal bidra til utforming av spesifikasjonen, kan utformes av leverandøren dersom ikke kunden bidrar innen tidsfristen.
- Paragraf 45: Ved heving av avtalen, skal leverandør gjøre forbrukers data  tilgjengelig for kostnadsfri nedlasting i et ordinært, maskinlesbart format.


## Verktøy for ekstern tilknytning
Avveininger:
- Er det ønskelig med kommunikasjon mellom klienter i nettverket, eller skal det etableres enkeltvis tilkobling mellom hver klient (mobiltelefon) og tjener (overvåkningsprogram)?

### Veilid
Intro: Utviklet av det anarkistiske hackerkollektivet Cult of the Dead Cow, Veilid er et ungt rammeverk for ende-til-ende vert-til-vert kryptert zero-trust kommunikasjon.  
Nettside: https://veilid.com/
- Fordeler:
	- Fullstendig distribuert, ingen behov for å lite på eksterne.
	- Alle nettverkets deltakere bidrar med å videresende trafikk ==> Økt antall deltakere medfører automatisk økt båndbredde.
	- Skrevet i Rust, et trygt språk
* Ulemper:
	- Relativt ungt prosjekt, utgitt sommeren 2023.
	- Lite dokumentasjon
	- Ingen fullstendige integrasjoner, i noen språk, for sending av video eller bilder.
* Interessante objekter:
	- https://github.com/stillonearth/bevy_veilid
	- https://github.com/socketwench/veilid-server-alpine
	- https://github.com/spithash/Veilid-IRC-Bot
	- https://github.com/stillonearth/veilid_duplex  !
	- https://github.com/miampf/bote/blob/main/src/config.rs
	

### Tailscale
Intro: et sentralisert overlay VPN-nettverk, hvor sentralen kun administrer hvordan verter kan koble seg sammen med hverandre, uten at sentralen håndterer trafikken deres.
- Ikke fri programvare på tjenersiden.
-- En friprog implementasjon av tjenersiden kalles headscale. https://github.com/juanfont/headscale
- Jeg har erfaring med å bruke tailscale


### Nebula
Intro: Et overlay nettverk utviklet av Slack. Omtrent likt som Headscale, Tailscale og netbird
- Fri programvare tvers gjennom
- Testet av uavhengig sikkerhetsrevisor
- Bloggpost: https://medium.com/several-people-are-coding/introducing-nebula-the-open-source-global-overlay-network-from-slack-884110a5579
- repo: https://github.com/slackhq/nebula

### Netbird
Et sentralisert Overlay VPN-nettverk, ikke så ulikt Tailscale og Headscale

- https://github.com/netbirdio/netbird

### Spritely
Et rammeverk for å lage klient-til-klient applikasjoner, fri fra tjener-klient modellen for internett.
- Under utvikling
- Objekt-orientert programmering
- Støtter språkene Guile og Racket, begge språk jeg ikke har noen erfaring med.
- Hjemmeside: https://spritely.institute

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 03.02.2024
### Tidsrom: 12:00 - 12:30, 14:30 - 15:30, 21:30 - 22:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: litteraturstudie

## Nasjonal Sikkerhetsmyndighets Grunnprinsipper for IKT-sikkerhet (NSM GP-IKT)
Et offentlig tilgjengelig rammeverk, koblet mot ISO/IEC 27002:2022, tilpasset NSMs vurderinger
- Inneholder 118 sikkerhetstiltak, fordelt på 21 prinsipper og 4 kategorier
- Lettlest og forståelig, lite vanskelig fagterminologi
Siden denne standarden er like gyldig som ISO/IEC 27002, men denne er betydelig lettere å lese;
og dermed å etterleve, tror rett og slett dette blir standarden jeg skal følge.
- Det er ingen krav om at virksomheter som ikke er underlagt sikkerhetsloven må følge NSM GP-IKT




## Rammeverk for brukergrensesnitt
### Rust
Egui https://github.com/emilk/egui  
Yew https://github.com/yewstack/yew  
Dioxus https://github.com/dioxuslabs/dioxus  
Iced https://iced.rs/



## Dypdykk hos Veilid
Et dypdykk i [Veilid teamets Discord server](https://discord.com/invite/5Qx3B9eedU) viser at de ikke offisielt har startet utviklingen av håndtering av lagring og sending av datablokker på nettverket; kun 64bit chunker. Ei heller har de begynt å utvikle protokoller for sending av mediafiler og strømmer. En implementasjon av WebRTC er i planleggingsfasen.  
Det at disse funksjonene ikke er tilgjengelige, diskvalifiserer Veilid umiddelbart fra 


---

# Konstruksjonsgrunnlag
## Hva skal utføres?
1. koble videostrøm, samhandling med tjener og varsling til mobil enhet både innenfor og utenfor det lokale nettverket
2. koble videostrøm, samhandling med tjener og varsling til datamaskin innenfor og utenfor det lokale nettverket
3. All personlig informasjon skal være kryptert i transitt
4. mulighet for å samhandle med enheten plassert lokalt i nettverket
5. Alt av overvåkningskameraenes internett-tilgang skal skje gjennom denne slusen, som ikke tillater andre tilkoblinger enn til godkjente enheter.
6. Så lite behov for lokal konfigurering som overhodet mulig
7. Skal funke uavhengig av kundens installerte nettverksruter
8. Multifaktor-autentisering
9. Være tilgjengelig for folk med funksjonsvariasjoner

## Hva må til for å gjøre det?
1 & 2: Det enkleste for å fer å bruke en eksisterende applikasjonssuite.  
1 & 2: Det nest enkleste, dersom noe skal bygges fra grunnen, er å lage en progressiv webapp, som kan "installeres" på skrivebordet på pc og telefon.  
3: En omvendt proxy som Traefik kan konfigureres til å bruke et inkludert sertifikat for å kryptere trafikk til https  
4: Et brukergrensesnitt, duplex kommunikasjon, og håndtering av brukerhendelser på tjenersiden.  

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 04.02.2024
### Tidsrom: 21:20 - 21:50
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: litteraturstudie

## Verktøy for statisk svakhetsanalyse av programvarekildekode
Før jeg velger hvilke eksisterende løsninger prosjektet skal bygge på, er det lurt å danne seg et bilde over etablert sikkerhetspraksis i løsningene.
Dersom det er mange svakheter i kildekoden som kan oppdages av automatiserte verktøy, er nok sikkerhet ikke i løsningens førersete.
Det er likevel viktig å tenke på at verktøy for statisk kodeanalyse har rykte på seg for å produsere mange falske positiver.

- https://owasp.org/www-community/Source_Code_Analysis_Tools
- ASH integrerer mange verktøy: https://github.com/awslabs/automated-security-helper

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 05.02.2024
### Tidsrom: 14:30 - 15:00
### Sted: Steigen Vertshus, Steigen

---


# Emne 1: litteraturstudie

## NSM Grunnprinsipper
Disse tiltakene prioriterer NSM høyest, fordi mangel på korrekt implementering av disse som oftest er rotårsaken til at dataangrep finner sted.  
Nummeret tilsvarer deres tiltaks-ID i NSM GP  
1.2.3  Kartlegg enheter i bruk i virksomheten.  
1.2.4  Kartlegg programvare i bruk i virksomheten.  
2.1.2  Kjøp moderne og oppdatert maskin- og programvare.  
2.1.9  Ta ansvar for virksomhetens sikkerhet også ved tjenesteutsetting.  
2.2.3  Del opp virksomhetens nettverk etter virksomhetens risikoprofil.  
2.3.1  Etabler et sentralt styrt regime for sikkerhetsoppdatering.  
2.3.2  Konfigurer klienter slik at kun kjent programvare kjører på dem.  
2.3.3  Deaktiver unødvendig funksjonalitet.  
2.3.7  Endre alle standardpassord på IKT-produktene før produksjonssetting.  
2.6.4  Minimer rettigheter til sluttbrukere og spesialbrukere.  
2.6.5  Minimer rettigheter på drifts-kontoer.  
2.9.1  Legg en plan for regelmessig sikkerhetskopiering av alle virksomhetsdata.  
3.2.3  Avgjør hvilke deler av IKT-systemet som skal overvåkes.  
3.2.4  Beslutt hvilke data som er sikkerhetsrelevant og bør samles inn.  
4.1.1  Etabler et planverk for hendelseshåndtering.  


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 06.02.2024
### Tidsrom: 14:00 - 16:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring; Melde ifra om etterslep i tidsplan
Det har ikke vært lett for meg å sette av tid til å arbeide med bacheloroppgaven den siste uka.  
Det har vært møter i diverse lag og foreninger, fullt hus med unger og hunder, sykdom og bursdag i familien, jeg har startet enkeltpersonforetak og gjort mitt første oppdrag.  
Per nå henger jeg 23 timer etter tidsplanen; og likeledes med fremdrifta.  
For å unngå videre forsinkelser, har jeg kontaktet veilederen i prosjektet Halldor Arnason og forklart situasjonen. Jeg skal forsøke å med noen om å bli en "ansvarlighetskamerat" som kan studere sammen med meg, for å gi en følelse av ansvarlighet og kollegium.


# Emne 2: litteraturstudie
## Skrive oppsummering av litteratur
Sammenfatter det jeg har lært gjennom litteraturstudiet; i ett dokument.


## Bruke LaTex istedenfor Libreoffice?
Jeg ønsker at prosjektet skal være reproduserbart, etterprøvbart, og at den publiserte dokumentasjonen skal være kontinuerlig oppdatert. Dette kan være utfordrende ved bruk av formater som Libreoffice's Open Document Format (.odf).
Det er ganske populært å bruke LaTex for å skrive artikler, avhandlinger og oppgaver i akademia, og fremfor alt er det programmerbart.
Det lar meg lage en prosess for kontinuerlig utvikling og integrering av dokumentasjonen (CI/CD), som så kan publiseres automatisk på prosjektsiden på Gitlab og refereres til.
Jeg har aldri brukt LaTex, men er åpen for å prøve.
Relavant lesning:
- https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/
- https://www.vipinajayakumar.com/continuous-integration-of-latex-projects-with-gitlab-pages.html
- https://egraff.github.io/uit-thesis/manual/frontpage.html
- https://github.com/egraff/uit-thesis

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 07.02.2024
### Tidsrom: 11:00 - 16:30, 20:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: litteraturstudie
## Undersøke personvernlovgivning
Et sentralt krav i peronopplysningsloven er at ethvert system eller løsning skal ha innebygd personvern.
Hva som menes med det generelt, og i programvareutvikling spesielt, har datatilsynet veiledning på hhv. [her](https://www.datatilsynet.no/rettigheter-og-plikter/virksomhetenes-plikter/innebygd-personvern-og-personvern-som-standard/) og [her](https://www.datatilsynet.no/rettigheter-og-plikter/virksomhetenes-plikter/programvareutvikling-med-innebygd-personvern/).

## Undersøke miljøbelastning av dyrehold
Hva er miljøbelastningen av ett lam som dør, og dermed ikke havner på matfat?  
Hvis man går ut fra at forbruket av kjøtt fra småfe og rein er forutbestemt og stabilt, og produksjonen tilpasser seg forbruket; hvor mye kan miljøbelastningen av dyreholdet reduseres dersom man forhindrer tap av husdyr til rovvilt?

Langtidseffekten (frem til år 2100) av å gradvis redusere produksjonen av dyrebaserte matvarer over en periode på 15 år fra 2020, per kg matvare: 
- fårekjøtt: 286kg aCO2e 
- storfekjøtt: 298kg aCO2e
- fåremelk: 23kg aCO2e
- storfemelk: 10kg aCO2e

https://journals.plos.org/climate/article?id=10.1371/journal.pclm.0000010

<br>

Husdyrbasert matproduksjon står for mellom 11 og 16% av totale menneskelige klimagassutslipp. 10% reduksjon i utslipp fra dyrebasert matproduksjon åpner for 1,5% økning i utslipp fra industriell sektor. Dersom klimagassutslipp fra matproduksjon holdes stabil, må utslippene fra industri reduseres med 80% før 2050 for å holde global oppvarming under 2 grader Celsius.
https://www.nature.com/articles/s43016-021-00265-1

<br>

Det finnes ikke statistikk for kjøttvekt og alder på dyr tapt til rovvilt, derfor brukes gjennomsnittlig slaktevekt når tapt kjøttproduksjon beregnes. Dette medfører overestimering, men å innhente og beregne mer presise data for dette er utenfor prosjektets fokusområde.  
I mars 2023 var det 915 344 vinterfôra sau i Norge.  https://www.ssb.no/jord-skog-jakt-og-fiskeri/jordbruk/statistikk/husdyrhald  
I 2022 ble 81 638 slaktelam godkjent til menneskemat, som tilsammen veide 1925 tonn slaktevekt.  https://www.ssb.no/jord-skog-jakt-og-fiskeri/jordbruk/statistikk/kjotproduksjon   https://data.norge.no/datasets/b8da2c09-d2ab-3258-9bc7-ec2472a7e9fd  
Gjennomsnittlig slaktevekt var da 23,6kg.


Statistikken i Nature bruker kg kjøtt, imens slaktevekt inkluderer skjelett og andre deler av slaktet som ikke normalt spises. Ifølge Universitetet i Wisconsin-Madison blir rundt halvparten av slaktevekta av sau, brukbar mat. https://livestock.extension.wisc.edu/articles/how-much-meat-should-a-lamb-yield/   
På Strandmoa Gård i Steigen, med blanding av rasene villsau, spæl og dorper; var 40% av slaktevekta kjøtt. Dette er ikke representativt for Norge.

Jeg kan da estimere at hvert lam tapt til rovvilt innebærer 10 kg tapt kjøtt, og unødvendig utslipp på 2860kg annualiserte CO2-ekvivalenter (år 2100).


---

## Undersøke miljøbelastningen av maskinlæring
Nødvendig prosesseringskraft som brukes av en maskinlæringsmodell for objektgjenkjenning kan deles inn i to faser:
- treningsfasen
- bruksfasen 

Nødvendig prosessorkraft og strøm varierer sterkt med antall objektklasser man ønsker å trene på; maskinvaren man bruker for treningen, treningens parametere, ønsket presisjon, samt modellen man trener. Det er ofte store besparelser i å fin-justere en eksisterende modell-fil, heller enn å trene en ny modell-fil fra grunnen av.

Det tar mellom 2 og 5 dager å trene en modell-fil av YOLOv8-nano fra grunnen av; på COCO datasettet med 80 objektklasser, på en Nvidia A100 GPU. Treningstiden øker for større modeller. https://github.com/ultralytics/ultralytics/issues/5172

Stiller spørsmål om strømforbruk til utviklerne av YOLOv8: https://github.com/ultralytics/ultralytics/issues/8083


## Skrive oppsummering av litteratur
Skrive kapitler 3, 4 og 5 om rammeverk for brukergrensesnitt, relevante standarder og lovverk, og miljøbelastning.


---

# Emne 2: Prosjektstyring
For å imøtekomme forsinkelser, inngår jeg avtale med prosjektveileder Halldor Arnason om å ha 30min framdriftsmøter over video annenhver uke. Jeg inngår også avtale med en medstudent om å være studiekamerater, der vi studerer sammen virtuelt, selv om vi ikke studerer det samme.

Lage README.md fil, som folk vil se når de besøker git repoet på gitlab.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 08.02.2024
### Tidsrom: 08:00 - 09:00
### Sted: Leinesfjord, Steigen

---


# Emne 1: litteraturstudie
## Undersøke miljøbelastningen av maskinlæring
Fikk svar om strømforbruk av utviklerne av YOLOv8: https://github.com/ultralytics/ultralytics/issues/8083  
Nødvendig prosessorkraft for å trene en maskinlæringsmodell (og da også å finjustere den); skalerer lineært med antallet bilder i treningsmaterialet og antallet parametere i modellen.

Antallet objektklasser har mindre å si.

For dette prosjektets spesifikke datasett, modellkonfigurasjon og maskinvare; er nok den mest presise måten å finne ut av strømforbruk å måle forbruket av treningen.
Der kan [OpenCost](https://www.opencost.io/), [cAdvisor](https://github.com/google/cadvisor) eller [sysdig](https://sysdig.com/) komme inn. De alle monitorerer ressursbruken til Linux-konteinere, enten for seg selv (docker/podman) eller som del av en system-klynge (kubernetes/openshift).

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 11.02.2024
### Tidsrom: 12:20 - 18:50
### Sted: Makerspace Bodø, Bodø

---


# Emne 1: litteraturstudie

## Klimagassutslipp fra maskinlæring
Grep som kan redusere karbonavtrykk fra trening og bruk av maskinlæringsmodeller:
1. Bruk maskinvare laget spesifikt for trening og kjøring av maskinlæringsmodeller. Slik maskinvare gjør prosesseringen 2x-5x mer effektivt enn bruk av generelle prosessorer.
2. Velg effektiv arkitektur og modell.
3. Tren og kjør modellen i et moderne datasenter. Datasentre har mer effektiv maskinvare enn de fleste hjemmeservere, og kan spare 1.4x-2x energi.
4. Tren og kjør modellen et sted med klimavennlig energiproduksjon.

[[1]](https://blog.research.google/2022/02/good-news-about-carbon-footprint-of.html)


Ultralytics publiserer detaljert statistikk på plattformen de bruker for å trene YOLO-modellene.  
Det tok fire dager og fire timer, altså 100 timer å trene YOLOv8m på COCO datasettet. I den perioden brukte Nvidia A100 GPU-en gjennomsnittlig 300W.
[[2]](https://wandb.ai/glenn-jocher/YOLOv8/runs/oxwuolug?workspace=user-)  
YOLOv8m har 25.9 millioner parametere.  
COCO har 330 000 bilder, hvorav 200 000 inngår i treningsmateriale og resterende i testingsmateriale.

Glenn Jocher i Ultralytics skriver at treningstid skalerer lineært med modellparametere og bilder i datasettet. For å finne strømforbruket av å trene en YOLOv8x modell på et mindre datasett på 24 000 bilder (3000 bilder per dyreart);  
Skaleringsfaktor datasett: 24,000 / 200,000 = 0.12  
Skaleringsfaktor parametere: 68.2 millioner / 25.9 millioner = ca 2.634  

Strømforbruk for den større modellen på vårt datasett = (treningstid for den mindre modellen) \* (skaleringsfaktor for parametere) \* (skaleringsfaktor for datasett)  
= 30kWh \* 2.634 \* 0.12  
**= 9.48kWh**

Den fransk-kanadiske datasenter-virksomheten OVH sier deres datasentre har et karbonavtrykk på 0.18kg CO2e / kWh IT-forbruk.

Karbonavtrykket av å trene modellen blir da **1.71kg CO2e.**

[[3]](https://corporate.ovhcloud.com/en/sustainability/environment/)


## Karbonavtrykk fra husdyr
Forholdet mellom kjøtt og protein i <https://journals.plos.org/climate/article?id=10.1371/journal.pclm.0000010> er 0,23 for storfe og 0,21 for får.

Det ikke-annualiserte karbonavtrykket for kjøtt, fra deres datakilde GLEAM:
- får: 19,1kg CO2e / kg kjøtt
- fe: 35,8kg CO2e / kg kjøtt

Det ikke-annualiserte karbonavtrykket av et får med kjøttvekt etter slakt på 10kg blir da 191kg CO2e.


[[4]](https://foodandagricultureorganization.shinyapps.io/GLEAMV3_Public/)



## Sammenstille litteraturstudie
Skrive mer på kapittel 5 Miljøbelastning



[1] https://blog.research.google/2022/02/good-news-about-carbon-footprint-of.html  
[2] https://wandb.ai/glenn-jocher/YOLOv8/runs/oxwuolug?workspace=user-  
[3] https://corporate.ovhcloud.com/en/sustainability/environment/

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 12.02.2024
### Tidsrom: 10:30 - 15:30, 22:30 - 23:30
### Sted: Tjeldberget, Bodø

---

# Emne 1: litteraturstudie

## Sammenstille litteraturstudie
Utbrodere kapittel 1: Eksisterende løsninger. Skrive inn informasjon om ulike løsninger for kameraovervåkning og vert-til-vert nettverkskommunikasjon.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 13.02.2024
### Tidsrom: 10:00 - 11:00, 14:30 - 17:30
### Sted: Tjeldberget, Bodø; og Makerspace Bodø

---

# Emne 1: litteraturstudie

## Sammenstille litteraturstudie
Utbrodere kapitler 1: Eksisterende løsninger, 6: Programvarebiblioteker, 7: Maskinlæringsmodeller, og 8: Treningmateriale for maskinlæring.  
Skrive om ulike nettverksløsninger for kryptert vert-til-vert kommunikasjon, programvarebiblioteker som vanligvis brukes til maskinlæring, maskinlæringsmodeller, og hvor treningsmaterialet for maskinlæring skal komme fra i dette prosjektet.

Skrev referanser.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 14.02.2024
### Tidsrom: 9:00 - 15:00
### Sted: Tjeldberget, Bodø

---

# Emne 1: Konstruksjonsgrunnlag

## Flette sammen konsepter
Ser videre på tre eksisterende løsninger;
- Frigate, Viseron og Zoneminder
- Nebula og Tailscale
- Traefik, Authentik og Atuhelia

Nebula har ikke per nå en kontainer, eller helm chart for kubernetes. 
Relevant issue: https://github.com/slackhq/nebula/issues/25  
sudo docker run -d --restart always --name nebula  --net host --cap-add=NET_ADMIN --cap-add=SYS_ADMIN --device=/dev/net/tun -v $PWD:/app -w /app debian:buster-slim ./nebula -config ./nebula.config.yml


Tailscale er tilgjengelig som konteiner, og har god integrering med kubernetes
Dokumentasjon: https://tailscale.com/kb/1185/kubernetes

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 15.02.2024
### Tidsrom: 10:30 - 11:00, 12:30 - 16:00, 17:30 - 22:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Kundemøte
Holder innledende møte med Stig Strandmoa for å avklare ønsker og behov, som grunnlagsmateriale for konstruksjonsgrunnlaget / kravspesifikasjonen.

## Komponenter
Går bort fra metoden med å flette komponenter sammen til konsepter og så vurdere dem med vektingsmatrise, siden alle komponentene i hver kategori (nettverk, kameraprogramvare, maskinlæringsmodell) stort sett er internt utbyttbare. Vurderer heller komponentene i hver kategori for seg selv, og syr sammen et konsept av det.

# Emne 2: Prototyping
Aktiverer en 90-dagers prøveperiode med Google Cloud Platform sitt Kubernetes-tilbud, for å ha en klynge med offentlig IP-adresse. Nødvendig for å kunne teste ulike nettverksløsninger

Lager en virtuell maskin på GCP, og installerer cloudflareddns i docker på den for å sikre at vertsnavnet alltid peker på riktig IP-adresse.

Setter opp en klynge med k3s, og installerer headscale på den. Når ikke frem til IP-adressen, så jeg installerer en enkel webtjener på serveren for feilsøking. Når fortsatt ikke frem.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 16.02.2024
### Tidsrom:  13:30 - 16:30, 18:00 - 22:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Konstruksjonsgrunnlag
## Undersøker ergonomien av å bruke k3s, headscale og zoneminder
Setter opp en klynge med k3s, og forsøker å installere Zoneminder på den. Det lykkes, men tilkoblinger nektes. Mulig mismatch mellom helm-charten sine forventninger til ingress, og k3s' standard-konfigurasjon med traefik.


Fant ut hvorfor jeg ikke kunne nå noen ting på Debian VM'en i Google Cloud, ved å lage en ny Fedora VM der, med kun pakke-installert nginx som serverer en statisk nettside på port 80.

Feilen er at k3s i standard-konfigurasjon kjører reverse-proxyen Traefik som binder seg til bl.a. port 80. Alle andre tjenester som forsøker å bruke den porten, blir da nektet.
Løsningen er nok å bruke reverse proxyen, vil jeg tro. Eller å kjøre ukonteinerisert på serveren.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 19.02.2024
### Tidsrom: 22:00 - 22:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Sammenlikning av Tailscale, Nebula, Netmaker og ZeroTier
Defined Networks, som utvikler og selger Nebula, har publisert en tilsynelatende nøytral [sammenlikning](https://www.defined.net/blog/nebula-is-not-the-fastest-mesh-vpn/) av sitt produkt og konkurrentenes produkter. De siste fem årene har de jevnlig målt ytelsen av produktene under ulike forhold. Testenes grensebetingelser er kjente, innstillingene for de ulike produktene og testenes rådata skal publiseres [i et eget repo på Github.](https://github.com/slackhq/nebula)

Leser en tredjedel av sammenlikningen.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 20.02.2024
### Tidsrom: 11:00 - 14:00, 15:00 - 17:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Sammenlikning av Tailscale, Nebula, Netmaker og ZeroTier

Leser resten av sammenlikningen.  
- ZeroTier er uaktuelt pga mangel på tilgangskontroll-lister (ACL), samt dårlig ytelse.  
- Tailscale's ytelse er noe mer variabel enn Nebulas og Netmakers.  
- Tailscale bruker betydelig mer minne enn alternativene, og minnebruken er svært variabel i testingen deres. Tailscale har brukt opp mot 1 GB i testing, mot Nebulas og Zerotiers stabile 25MB og 10MB respektivt. Netmaker bruker Wireguard kjernemodulen på Linux og er derfor vanskelig å måle men virker ressursgjerrig.
- Gjennomflyt per CPU-kjerne er best hos Tailscale, grunnet deres avlasting av nettverksruting til Linux-kjernen. Det har antagelig også sammenheng med den økte minnebruken.
- Nebula skal visstnok være mer ressursgjerrig og høytytende enn alternativene på plattformer som ikke er Linux, da alternativene hovedsaklig er optimalisert for Linux.


## Deepdive Headscale dokumentasjon
Det ser ut til at Headscale har svært begrenset støtte for ACL'er per nå; og det er allerede etablert at Headscale ikke har intensjoner om å støtte mer enn ett "tailnet" (virtuelt flatt, sammenkoblet nettverk) per kontrollserver. Det gjør Headscale mindre kurant for prosjektet, ettersom et av målene mine er å kunne tilby én sentral server som kunder kan koble seg til; som tilbyr sikker kommunikasjon mellom deres enheter og ingen mulighet for tilgangs- eller informasjonslekkasje.


## Netmaker
Netmaker ser ut til å ha best ytelse av mesh-nettverks-løsningene, men har per nå ikke en mobilapp for enkel tilkobling til mesh-nettverket.


## Nebula
Jeg begynner å sette opp et Nebula nettverk, med et "fyrtårn" i Google Cloud. Alle produserte nøkler og sertifikater krypteres i hvile, med nøklene lagret i min Bitwarden passordbehandler.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 21.02.2024
### Tidsrom: 15:00 - 17:00, 18:00 - 20:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Nebula
Legger til brannmur-regel i Google Cloud Platform (GCP) om å akseptere inngående UDP-trafikk til port 4242, og likeledes i `iptables` i den Virtuelle Maskinen (VM).

Fortsetter oppsett av Nebula nettverk; med å inkludere laptopen min i nettverket.  
Det funker så som så, har problemer med at "handshake" mellom laptop og fyrtårn ikke fullfører. Det kommer ikke opp i loggen til fyrtårnet at det har mottatt noen "handshake" pakke, så jeg undrer hva som skjer.

Etter å ha lagt til <code>respond: true</code> i nebula konfigurasjonsfilas <code>punch</code> seksjon, som tilrådet i [Nebula issue #660](https://github.com/slackhq/nebula/issues/660#issuecomment-1102535594) får jeg opprettet kontakt knirkefritt. 

Husk; `systemctl reload nebula` ved endret konfigurasjon, ikke `systemctl restart nebula`.

### isolering av brukere
Nebula har støtte for "tagger", og å tillate at kun verter med samme tagger kan kommunisere med hverandre. Det kan være aktuelt å bruke hashede kundenr som tagg, sånn at alle enhetene til én enkelt bruker kan kommunisere, uten at andre brukere kan legge til taggen hos seg manuelt.


### Nebula intern DNS
Jeg aktiverer DNS-funksjon på fyrtårnet, slik at verter kan nås med sine vertsnavn. For dette, lager jeg en ny brannmurregel i GCP som tillater inngående UDP-trafikk på port 53; for DNS-forespørsler.


<br>


## Dynamisk DNS
Når man har en server i et datasenter, en hjemmeruter eller en mobiltelefon; tildeles nesten alltid den eksterne IP-adressen til enheten dynamisk. Adressen kan altså endres relativt ofte.  
DNS er systemet som holder styr på hvilke URL'er (f.eks. uit.no) som hører til hvilke IP-adresser.

Med dynamisk DNS holdes IP-addressen til spesifikke URL'er oppdatert, gjennom at maskinen som ønsker å vite sin IP-adresse; spør en ekstern tilbyder hvilken ekstern adresse senderen har. Deretter gir DynDNS-programmet beskjed til en DNS-server om at IP-adressen til den relevante URL'en er endret; og ber om at den oppdateres.

I mitt tilfelle bruker jeg [Timothy Miller's Cloudflare-ddns](https://github.com/timothymiller/cloudflare-ddns/), som kjøres i k3s klynga på Debian VM'en hos GCP.  
Den sørger for at URL'en <https://net.ingeniorskap.no> alltid peker på den faktiske eksterne IP-adressen som tilhører VM'en. DNS-oppføringen bruker ikke Cloudflares proxy-funksjon.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 22.02.2024
### Tidsrom: 10:00 - 16:30, 18:00 - 19:30
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Konstruksjonsgrunnlag

## Nebula
### Installasjon på Windows 10
Må kjøres med administrator-privilegier, kan kjøres uten installasjon.
nebula install service?
Banene for de ulike delene av PKI i konfigurasjonsfila trenger ikke være i gåseøyne.

En tjeneste for å kjøre nebula-agenten kan installeres. Dette åpner for at systemet kan håndtere logger ved å følge [guiden her](https://nebula.defined.net/docs/guides/viewing-nebula-logs/#windows)

<br>


### Automatisk installasjon og konfigurasjon
Etter å ha installert og satt opp to-tre nebula verter/agenter manuelt, ser jeg at det er en omstendelig og manuell prosess som ikke lar seg enkelt skalere. Begynner derfor letingen etter et verktøy som kan foreta automatisk installasjon og oppsett av nebula-klienter; gjerne gjennom et brukergrensesnitt som er tilgjengelig for brukeren selv.

Finner da [CEGO A/S sin Nebula-provisioner](https://github.com/cego/nebula-provisioner), som ihvertall overfladisk ser ut til å passe formålet. Prosjektet har svært begrenset dokumentasjon, og har ikke definert en programvarelisens. [Jeg åpnet et issue med ønske om lisens-avklaring og bedre dokumentasjon.](https://github.com/cego/nebula-provisioner/issues/571)

<br>

### Trygg bruk

Det er ikke nødvendig at systemet som kontrollerer Nebula-nettverkets CA og utsteder sertifikater til verter (både agenter og fyrtårn), må være del av Nebula nettverket. Det er fullt mulig å bruke et separat system, som utsteder sertifikater til verter på forespørsel; og får sertifikatet overført til den respektive verten. Et slikt system trenger ikke ordinært være tilkoblet internett utenom selve overføringen, så dette kan gjøres ganske trygt.

Det er flere interessante guider for nebula, med ulike perspektiver.

- [Eliseo Martelli fokuserer på sikkerhet](https://www.eliseomartelli.it/blog/nebula)
- [apalrd](https://invidious.einfachzocken.eu/watch?v=aImSCypCsuw)


<br>

## OpenZiti
Oppdager [OpenZiti](https://openziti.io/docs/learn/introduction/features), som virker å være som Nebula, med tilleggsmuligheten å bruke en [ekstern identitetstilbyder](https://openziti.io/docs/learn/core-concepts/identities/overview#3rd-party-ca
---auto-enrolled), for å logge inn i nettverket. OpenZiti har også sterkt fokus på konteinerisering og kubernetes-bruk; og har offisielle helm-kart for alle sine komponenter.

OpenZiti har også brukervennlige interaktive veiledninger for å sette opp og bruke programvaren i ulike scenarioer.

OpenZiti har applikasjoner for [Android](https://openziti.io/docs/reference/tunnelers/android/) og [iOS](https://openziti.io/docs/reference/tunnelers/iOS/), og har også mulighet for å [opprette en tilkobling kun i nettleseren](https://openziti.io/docs/learn/quickstarts/browzer/), uten å installere noe lokalt på enheten.

Ser noen OpenZiti videoer:
- [introduksjon](https://invidious.einfachzocken.eu/watch?v=qyjM5y8Op_I)


### Implementasjon
Ziti har mange komponenter, og fremmedord. Tenker jeg skal prøve å sette opp et enkelt nettverk, med en kontroller på GCP; og så skrive om prosjektet mitt på [OpenZitis diskusjonsforum](https://openziti.discourse.group/), og etterspørre hvilket oppsett og arkitektur OpenZiti bør ha for mitt formål.


## Arkitekturdiagram
Jeg lager et simpelt diagram over systemets arkitektur, for å forklare tredjeparter og partnere hva systemet skal se ut som.

![diagram of network architecture. Each node in a subnet is connected to a mesh router / control server.](../../media/pred-eye_overview.png "Nettverksdiagram")

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 23.02.2024
### Tidsrom: 10:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Publiserer [et innlegg](https://openziti.discourse.group/t/using-openziti-in-distributed-surveillance-system/2135) på OpenZitis forum, der jeg forteller om prosjektet og ber om råd til oppsett av OpenZiti. Kontaktes via personlig melding av Philip Griffith, én av OpenZitis vedlikeholdere og ansvarlig for produkter ment for eksterne. Han forklarer noen konsepter jeg slet med å forstå, og viste meg nyttige ressurser. Jeg forteller om en programvarelus i oppsetts-veiledninga deres. Vi avtaler et videomøte tirsdag 27.02, klokka 11:00 - 11:25; med formål å få meg i gang med å bruke OpenZiti i prosjektet, og overføre kunnskap.

Mottar også en hjelpsom kommentar på innlegget, som hjelper meg videre.

## Moonfire-NVR
Åpner et [issue om integrert støtte for zero-trust](https://github.com/scottlamb/moonfire-nvr/issues/308) i Moonfire.


## Blue-candle
[Blue-candle](https://github.com/xnorpx/blue-candle) er en frittstående modul for objektgjenkjenning, skrevet i Rust, som utvikleren har [uttrykt støtte for å integrere i Moonfire](https://github.com/scottlamb/moonfire-nvr/issues/30#issuecomment-1873575896).  
Jeg tilbyr min hjelp i [et issue i Blue-Candle](https://github.com/xnorpx/blue-candle/issues/15) om støtte for RT-DETR modellen.


## Frigate
Jeg setter opp og kjører Frigate på k3s klyngen lokalt på laptopen min, for å teste funksjonalitet. Møter noen problemer ved gjentatte konfigurasjonsendringer.  
Frigate har støtte for maskinvareakselerasjon av ML på Nvidia-maskinvare med TensorRT, så jeg prøver å sette det opp.
- setter opp [nvidia-container-runtime]()
- Gjør nvidia tilgjengelig for k3s ved å følge [nvidias anvisninger](https://github.com/NVIDIA/k8s-device-plugin) og [k3s sine anvisninger](https://docs.k3s.io/advanced?_highlight=container#nvidia-container-runtime-support)
- Får eksportert pytorch-modellen RT-DETR til ONNX og TensorRT via ultralytics, ved å skape et virtuelt miljø med python3.11, og installere + oppgradere disse pakkene hver for seg i miljøet: <code> pip-24.0, ultralytics-8.1, onnx-1.15, torch-2.2.1-cp311, tensorrt-8.6.1-post1 </code>

Lyktes ikke med å få noen slags objektgjenkjenning til å fungere.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 25.02.2024
### Tidsrom: 23:00 - 01:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Går gjennom den interaktive [quickstart-guiden](https://openziti.io/docs/learn/quickstarts/network/local-no-docker/) for lokal bruk uten konteinerisering.

## Blue-candle
Ønsket om støtte for RT-DETR modellen lukkes av prosjektvedlikeholderen, fordi prosjektet avhenger av støtte i oppstrømsbiblioteket [candle](https://github.com/huggingface/candle).

## Moonfire-NVR
Jeg prøver å kjøre programmet konteinerisert, men møter mange problemer.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 26.02.2024
### Tidsrom: 10:00 - 12:00, 13:00 - 17:15, 18:00 - 20:00, 00:00 - 01:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Publiserer en kommentar i [tråden hos OpenZiti forumet](https://openziti.discourse.group/t/using-openziti-in-distributed-surveillance-system/2135/3), der jeg oppklarer noen ting og nevner at det ville være nyttig med en SDK for Openziti i Rust.

Prøver å få kontrolleren, konsollen og en ruter opp og gå på k3s-klynga i VM'en hos GCP, ved å følge [disse guidene](https://openziti.io/docs/category/hosting-openziti)

Etter at det ikke funket så bra, prøver jeg å gå steg for steg gjennom den manuelle utgaven av [hurtigoppsettet for OpenZiti i kubernetes](https://openziti.io/docs/learn/quickstarts/network/local-kubernetes). For at det skal funke på et mer permanent vis, må jeg oversette kommando for kommando til de forholdene jeg ønsker å oppnå; som også viser seg å bli problematisk. Det er vanskelig å bygge bro mellom en liten og automatisk test-oppføring av OpenZiti, og en bruksklar oppføring.

## Cert-Manager
For at brukere skal kunne bruke tjenesten uten å bli varslet om at tjenesten bruker "selvsignerte utrygge sertifikater", må jeg få utstedt TLS-sertifikater. Det gjennomføres ved hjelp av [Cert-Manager](https://cert-manager.io/). Jeg følger [denne guiden](https://cert-manager.io/docs/installation/helm/) for å installere Cert-Manager i k3s-klyngen, og [denne guiden](https://cert-manager.io/docs/configuration/acme/dns01/cloudflare/) for å sette opp en ACME sertifikathenter for Cloudflare.  
Det viser seg at YAML-formatet er svært følsomt for innrykk, og jeg strever en god halvtime med å få korrekt innrykk på Issuer.yaml. Dette er den endelige fila:

```
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-dns01-issuer
spec:
  acme:
    email: jacob.ludvigsen@protonmail.com
    server: https://acme-v02.api.letsencrypt.org./directory
    privateKeySecretRef:
      name: cluster-issuer-account-key
    solvers:
    - dns01:
        cloudflare:
          email: jacob.ludvigsen@protonmail.com
          apiTokenSecretRef:
            name: cloudflare-api-token-secret
            key: api-token
      selector:
        dnsZones:
          - 'ingeniorskap.no'
```

Cloudflare er den aller største internett-aktøren, særlig innen DNS, CDN og sertifikat-signering. All bruk av Cloudflare medfører derfor visse personvernavveininger og bekymringer. Ved en senere anledning vil jeg gå over til å bruke [desec.io](https://desec.io/) for DNS og TLS-signering, via en [Cert-Manager Webhook](https://github.com/su541/cert-manager-desec-webhook?tab=readme-ov-file). deSEC er en ideell organisasjon i Tyskland, med formål å yte DNS-tjenester på en personvernrespekterende måte.

Ved endringer i Cert-managers issuer sin konfigurasjonsfil: Issuer.yaml, må endringene gjøres gjeldende slik: `sudo helm --kubeconfig /etc/rancher/k3s/k3s.yaml -n cert-manager upgrade cert-manager jetstack/cert-manager  -f cert-manager/Issuer.yaml`  
Det er viktig å spesifisere `--namespace` / `-n`når man ikke kjører ting i standard-navnerommet til kubernetes.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 27.02.2024
### Tidsrom: 10:00 - 15:15
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Møter Philip Griffith i OpenZiti/Netfoundry.

## Moonfire-NVR
Prøver å kjøre Moonfire utenfor konteiner, som en statisk kjørbar fil. Får fortsatt ikke bilde fra lokale IP-kameraer, så Moonfire legges bort for nå.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 29.02.2024
### Tidsrom: 12:00 - 17:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Hører på opptaket fra møtet med Philip Griffith, og lager en transkripsjon ved hjelp av [Speechnote](https://flathub.org/apps/net.mkiol.SpeechNote) og [Whisper-v3](https://huggingface.co/openai/whisper-large-v3).  
Lager en bruker på [Netfoundry's CloudZiti SaaS tilbud](https://netfoundry.io/), som bygger på OpenZiti. Det vil ta bort mye av hodebryet under testing av ulike løsninger for integrering av OpenZiti i prosjektet mitt. Poliser og andre innstillinger som funker i CloudZiti, kan direkte overføres til en instans av OpenZiti jeg oppfører selv.

## Zoneminder
Undersøker muligheten for å bruke OpenZiti sin SDK direkte i Zoneminder, for enda sikrere kommunikasjon. For å kunne gjøre dét, tenker jeg [Alpine Linux](https://alpinelinux.org/) er en fin og sikker base for et [konteinerisert byggemiljø](https://github.com/ZoneMinder/zmdockerfiles/tree/master/development). Dessverre viser det seg at Alpine's Musl-libc ikke tillater mange av funksjonene fra GNU-libc som Zoneminder gjør bruk av. Det er én av grunnene til at Alpine Linux er sikrere enn mange andre Linux-distribusjoner, men gjør at jeg ikke kan bruke den for å bygge zoneminder.

Foreløpig er nok det enkleste å bruke en OpenZiti tunneler eksternt fra applikasjonen for å få få applikasjonens kommunikasjon over på ziti-nettverket.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 05.03.2024
### Tidsrom: 10:30 - 12:00, 15:00 - 16:00, 19:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Konstruksjonsgrunnlag

## FOSOPAS
Etter råd fra veileder Halldor Arnason i veiledningsmøte 04.03.2024, ser jeg nærmere på hvordan jeg kan lage et minimumsprodukt basert på FOSOPAS.

## Zoneminder
Prøver å bruke Zoneminders distribusjonspakker, via [distrobox](https://distrobox.it/). [Zoneminder-pakkene i Fedora](https://zoneminder.readthedocs.io/en/latest/installationguide/redhat.html) kan bruke enten nginx eller apache nettserver, i motsetning til pakkene i Debian og deres nedstrøms-distribusjoner som kun bruker apache. Apache krever rot-tilgang, som ikke er enkelt forenlig med bruk i Distrobox. Derfor installerer jeg en fedora-40 konteiner med distrobox, og inkluderer init-systemet `systemd` og støtte for bruk av vert-operativsystemets grafikkort.  
Hele kommandolinjen jeg brukte følger her:
```
distrobox create -i fedora:40 --nvidia --init  --additional-packages "systemd nano" -n fedora-zm
distrobox enter fedora-zm
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y zoneminder-nginx
sudo dnf install -y mariadb-server
sudo systemctl enable --now mariadb
sudo mysql_secure_installation   # her svarer jeg "ja" på bruk av unix-socket authentication, og klikker gjennom resten.
sudo mysql -u root -p < /usr/share/zoneminder/db/zm_create.sql
sudo mysql -u root -p -e "CREATE USER 'zmuser'@'localhost' \
                    IDENTIFIED BY 'zmpass';"
sudo mysql -u root -p -e "GRANT ALL PRIVILEGES ON zm.* TO \
                    'zmuser'@localhost;"
sudo mysqladmin -uroot -p reload
sudo ln -sf /etc/zm/www/zoneminder.nginx.conf /etc/nginx/conf.d/
sudo ln -sf /etc/zm/www/redirect.nginx.conf /etc/nginx/default.d/
sudo nano /etc/sysconfig/fcgiwrap   # her setter jeg DAEMON_PROCS lik antall kameraer denne zoneminder-installasjonen skal håndtere.
```

Siden vi kjører konteinerisert, får ikke prosesser i konteineren binde seg til porter under 1024 i standardoppsettet. Derfor flytter vi portene til 8080, som det er tillatt å bruke fra konteinere.  
Filen `/etc/nginx/conf.d/zoneminder.nginx.conf` skal se slik ut i toppen:
```
server {
    listen	 8080 default_server;
    listen	 [::]:8080 default_server;
    server_name = localhost $hostname;

#    ssl_certificate "/etc/pki/tls/certs/localhost.crt";
#    ssl_certificate_key "/etc/pki/tls/private/localhost.key";
#    ssl_session_cache shared:SSL:1m;
#    ssl_session_timeout  10m;
#    ssl_ciphers PROFILE=SYSTEM;
#    ssl_prefer_server_ciphers on;
```

Filen `/etc/nginx/nginx.conf` skal se slik ut på ca. linjene 37-41:
```
	server {
		listen       8080;
        listen       [::]:8080;
        server_name  _;
        root         /usr/share/nginx/html;
```

Vi kan nå ferdigstille installasjonen:
```
sudo systemctl enable --now nginx
sudo systemctl enable --now zoneminder
```

Zoneminders brukergrensesnitt er nå tilgjengelig i nettleseren på http://localhost:8080

Dette funker den første starten, men senere starter feiler pga MariaDB.

Førsøk med å bruke ordinære docker-conteinere (via podman, podman-compose, og podman kube create --> podman kube play / sudo k3s kubectl apply) feiler alle pga fcgiwrap ikke har tillatelse til å binde seg til en bestemt unix socket.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 06.03.2024
### Tidsrom: 10:30 - 15:00, 20:00 - 23:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1:  Konseptutvikling

## FOSOPAS
Gjør arbeid på FOSOPAS: Går bort fra bruk av kommandolinje-argumenter ved kjøring av skriptet, til en TOML-formatert konfigurasjonsfil. Dette åpner for mer fleksibel og repeterbar bruk; og gjør det enklere å konteinerisere programmet.

Bytter ut bruk av print()-funksjonen med logging()-funksjonen; for å ha bedre kontroll på beskjedene som betyr noe, uten å bli oversvømt av mindre viktige beskjeder.

Bytter ut lisens-tekst i headeren til skriptet med en SPDX-lisens-identifikator. Dette forenkler maskinell redegjørelse for såkalte SBOM, programvare-datablad.

Bytter ut programvarebiblioteket SAHI med en midlertidig forgrening som implementerer støtte for bruk av RT-DETR modeller.

Det FOSOPAS behøver for å kunne funke halvgreit, er:
- Mulighet for å varsle per epost, uten å måtte oppgi passord i klartekst
- Mulighet til å slå av og på ulike funksjoner (typer varsel, display)
- Ingen unødvendig bildegjenkjenning når det ikke er bevegelse eller interessante objekter i interesseområdet.
- Mulighet til å avklare et interesseområde vha polygon-koordinater

# Emne 2: Statusrapport og -møte 1
Statusrapport 1 skal leveres 12. mars, og statusmøte skal holdes 15.mars. Det er ikke lenge til, og jeg må lage ferdig en rapport til da.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 07.03.2024
### Tidsrom: 13:00 - 16:00, 18:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konseptutvikling

## FOSOPAS
Gjør arbeid på FOSOPAS.
- Gjør skriptet om til en installerbar python-modul
- Gjør konfigurasjonsfil importerbar i programvaren
- Går over til å bruke RT-DETR i programvaren
- Fjerner kommandolinje-argumenter

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 08.03.2024
### Tidsrom: 10:00 - 17:00
### Sted: Strandmoa Gård, Steigen

----

# Emne 1: Konseptutvikling

## FOSOPAS
Separere relaterte funksjoner ut fra hovedskriptet, til hver sine filer. Dette gjør det lettere å laste inn kun den koden som skal brukes, og gjør kodemengden og -kontekstene man må forholde seg til under utvikling mer håndterbar.  
Fjerne død kode.  

Implementere sentral logging-fasilitet; som håndterer logg-beskjeder fra alle prosesser og tråder på en trygg måte. Dette oppnås med Pythons `multiprocessing.Manager.Queue` objekt. Én loggkø lages i hovedskriptet, og deles med alle underprosesser. Alle prosesser produserer logg-beskjeder til køen, og én enkelt prosess konsumerer logg-beskjedene i køen og printer dem til terminalen etter beskjedens logg-nivå.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 09.03.2024
### Tidsrom: 10:00 - 18:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konseptutvikling

## FOSOPAS
Separerte relaterte funksjoner ut fra hovedskriptet, til hver sine filer.   
Forbedret dokumentasjon av funksjoner og objekter.  

Prøver først å implementere en nettside med Django, men det viser seg svært omstendelig. Bytter derfor til Flask.
Implementere en simpel nettside med Flask, som delvis fungerer. Mye feilsøking i den anledning. Det viste seg at Flask, når `debug=True` , restarter den innebygde webserveren. Dette gjorde at kø-objektene som hovedskriptet delte med flask-prosessen gjennom Flasks `config`-objekt; ble klonet, og det var klonen som ble videreført i Flask-prosessen etter restarten. Når produsent-skriptene da fylte på de originale kø-objektene, hadde ikke Flask-prosessen tilgang på dem.

Gjør feil i koden synlige ved å legge til flere hendelser som sender deskriktive loggmeldinger ved feil.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 10.03.2024
### Tidsrom: 19:30 - 21:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Prosjektstyring
## Arbeidslogger
Lager arbeidslogger over fredag, lørdag og dagen i dag; fordi jeg glemte det.

# Emne 2: Konseptutvikling
## FOSOPAS
Setter opp GPG-signering av commits, for å øke sikkerhetsnivået på prosjektet.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage

# Arbeidsøkt timelogg

### Dato: 11.03.2024
### Tidsrom: 08:30 - 20:00, 23:00 - 00:00
### Sted: Håsand Gård, Steigen

----

# Emne 1: Prosjektstyring
## Fremdriftsplan
Oppdaterer fremdriftsplan.

## Avviksmeldinger
1. Jeg har ikke skrevet avviksmeldinger hittil, selv om det har vært flere avvik. Den originale planen var ikke fullstendig realistisk; og forsøket på å smelte sammen UiTs krav til distinkte faser i plan og gjennomføring av bacheloroppgave i Maskinteknikk, og de iterative metodene jeg bruker i programvareutvikling, har ikke båret frukt. 

2. Jeg lærte svært mye under litteraturstudiet, som har kommet til nytte. Fokuset på å lære _alt jeg ville trenge senere_ før jeg kunne gå videre, gjorde at jeg brukte mye tid på å undersøke ting som _kanskje_ ville komme til nytte; og mindre viktige aspekter. 

3. Jeg har ikke begynt å skrive statusrapport 1 enda, og den skal leveres 12.03.


## Statusrapport
Jeg har enda ikke begynt å skrive statusrapporten som skal leveres i morgen. Det må jeg gjøre.


# Emne 2: Konseptutvikling
## FOSOPAS
- Implementerer en fungerende nettside som bruker Webgear og det asynkrone rammeverket Starlette; for å vise annoterte bilder fra analyserte videokilder, i et adaptivt rutenett.
- Forbedrer dokumentasjon ved å skrive "docstrings" på alle funksjonsdefinisjoner som forklarer hva funksjonen gjør og hvilke inputs den forventer.
- Forsøker å implementere Zero Trust nettverk i applikasjonen med OpenZitis Python SDK.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 12.03.2024
### Tidsrom: 08:30 - 13:00, 15:00 - 16:00, 20:00 - 23:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Prosjektstyring 

## Statusrapport 
Begynner å skrive statusrapport. Skriver alt utenom Fremdriftsstatus, og leverer utkastet på Canvas. Gjør resten i morgen.

## Lisensiering
Setter maskinlesbare lisens-markører inn i alle markdown-filer i prosjektet, for å vise tydelig at de alle er under fri kultur-lisensen CC-BY-SA-4-0+. Lisensmarkørene følger den internasjonale standarden SPDX (Software Package Data Exchange) v2.3. 


# Emne 2: Kravspesifikasjon 
Hittil har kravspesifikasjonen levd som del av konstruksjonsgrunnlaget; men for å separere hensyn blir også filene separert.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 13.03.2024
### Tidsrom: 08:00 - 10:00, 11:30 - 13:30, 16:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Prosjektstyring 

## Statusrapport 
Skriver ferdig statusrapport.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 14.03.2024
### Tidsrom: 08:00 - 17:00, 18:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Prototyping
## Utvikler Predalert

### Individuelle videostrømmer
Gjengi individuelle bildestrømmer til nettstedet.

 web_ui:  
 Lag en rute dynamisk for hver kø i q_result_dict.
 Bruk Starlette direkte i stedet for Webgear for å håndtere nettresponser,
 med en templateResponse som sender ruter for videoer til index.html.  
 Dette lar webappen tilpasse seg antall tilgjengelige strømmer,
 og åpner for interaksjon med individuelle strømmer (som fullskjerming).
 Dette var ikke mulig når web_ui konsumerte q_img_grid_web.

 templates/index.html:  
 Bruk Jinja2 til å dynamisk lage < img >-tagger for ruter
 mottatt fra Starlette.
 Hver tag får en unik ID, som kan refereres til av static/custom.js
 for å gjøre elementet i fullskjerm.

 __main__.py:  
 Ikke start make_image_grid hvis local_ui er deaktivert.
 Dette sparer ressurser.
 
 ### Web push varsler
 webpush krever at applikasjonen har et gyldig SSL/TLS sertifikat. Dette fordi det innebærer å varig installere en bit javascript på klient-nettleseren, som kan kjøres uten at nettleseren er åpen. Disse små skriptene kalles ServiceWorker.  
Et sertifikat for lokal utvikling genereres ved hjelp av [mkcert](https://github.com/FiloSottile/mkcert), og jeg får til å kjøre predalert-nettsida i kryptert tilstand. Dessverre stoler ikke Firefox på sertifikatet, fordi utstederen er ukjent.  
For å komme rundt dette, kjøres mkcert som rotbruker på dette viset:
```
sudo mkcert -install
sudo mkcert --key-file local_key_keep_secret.pem --cert-file local_cert_keep_secret.pem localhost
```

Fordi disse filene gir alle med tilgang, mulighet til å avskjære og omdirigere all traffik på pc'en, må de lagres trygt og kun brukes til lokal utvikling.
 
 I tillegg må applikasjonen åpnes i et privat Firefox-vindu for å omgå diverse innebygde og "nettleser-tillegg" sikkerhetstiltak.
 
Web Push krever at man bruker en varig backend-database for å lagre bruker-kredentialer; for å kunne fortsette å sende webpush varsler til brukerne etter programmet har blitt restartet. Ser derfor nærmere på bruk av databaser i Starlette, hovedsaklig `gino` som er asynkron og `SQLAlchemy` som er synkron og dermed ikke håndterer asynkrone endepunkter (dvs flere klientenheter tilkoblet på samme tid).
 
# Emne 2: Prosjektstyring
Blir med i [Norsk Programmering på Discord]( https://discord.gg/norskprogrammering) for å lufte prosjektet, få hjelp med problemstillinger som nettsøk ikke løser, og søke samarbeidspartnere.
 
<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 15.03.2024
### Tidsrom: 10:30 - 12:00
### Sted: I bilen på vei fra Steigen til Bodø

---

# Emne 1: Prosjektstyring
Skriver dokumentasjon fra statusmøte 1 som ble holdt tidligere i dag.  
Gjør klart et brukermiljø for å bruke LaTex til skriving av sluttrapporten. Prøver [Texmaker](https://www.xm1math.net/texmaker/index.html) og [TexStudio](https://texstudio.org). Bruker [UiTs LaTex-mal for masteroppgaver og Doktoravhandlinger](https://github.com/egraff/uit-thesis?tab=readme-ov-file).


 
<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 19.03.2024
### Tidsrom: 13:15 -  16:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling 
## Web push for Predalert
Fortsetter arbeidet med å implementere Web Push i programmet Predalert. Dette gir muligheten for å motta push-varsler på mobiltelefon (eller andre enheter med tilgang til nettleser), selv om nettsiden og nettleseren er lukket.  
Web Push avhenger av såkalte "Service Workers", som arbeider i en tråd på klientenheten, separat fra nettsiden som startet dem. For å minimere sikkerhetsrisiko, kreves det at nettsider bruker kryptert tilkobling (SSL/TLS) for å lagre Service Workers på klientenheter og la dem kjøre i bakgrunnen.

Jeg sliter med å komme igang igjen etter noen dager uten studier. Var på fylkesårsmøte i Rødt i helga, og farfaren min sin begravelse i Senja. Det tar naturlig nok litt tid å få hodet tilbake til studiene.

Vegrer meg litt for å starte opp igjen med Web Push arbeidet. Dette grunner i at jeg ikke har arbeidet med Javascript før, og ikke klarer helt godt å lese og forstå det. Jeg har ikke forutsetningene for å utvikle Web Push-implementasjon for Predalert på egenhånd. Jeg har heller ikke forutsetningene for å vurdere sikkerheten og egnetheten av de ulike eksempel-implementasjonene som finnes på internett.

Gode kilder om Service Workers:  
[Felix Gerchau blog 1](https://felixgerschau.com/service-workers-explained-introduction-javascript-api/)  
[Mozilla Developer Network 1](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers)

Gode kilder om Web Push:  
[Felix Gerchau blog 2](https://felixgerschau.com/web-push-notifications-tutorial/)   
[Mozilla Developer Network 2](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Tutorials/js13kGames/Re-engageable_Notifications_Push)  
[GauntFace's komplette oversikt](https://web-push-book.gauntface.com)  
[Surya Sankars gjennomgang](https://suryasankar.medium.com/how-to-setup-basic-web-push-notification-functionality-using-a-flask-backend-1251a5413bbe)  
[Google's Codelab på temaet](https://codelabs.developers.google.com/codelabs/push-notifications)  
[Nitin Raturi's programvareutviklings blog](https://tech.raturi.in/webpush-notification-using-python-and-flask)
 
<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 20.03.2024
### Tidsrom: 10:15 -  17:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling 
## Web push for Predalert
Fortsetter arbeidet med å implementere Web Push i programmet Predalert.  
Spør om hjelp i Discord-serveren Norsk Programmering, og får svar fra brukeren @matsl at jeg burde følge denne løypa:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.

I dag har jeg løst 1.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 21.03.2024
### Tidsrom: 09:00 -  17:00, 18:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling 
## Web push for Predalert
Fortsetter arbeidet med å implementere Web Push i programmet Predalert.  
løypa er:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.

I dag har jeg løst punkt 2.  
Brukte hovedsaklig [Akilesh Raos videoer](https://invidious.einfachzocken.eu/watch?v=oDIYl3G613E) som veiledning. Dette fordi han bruker async javascript, og videoene ble publisert for kun 6 måneder siden. De er dermed 3,5 - 5 år nyere enn de andre veiledningene jeg har sett på.

commit-melding for igår og idag:
* Implement frontend javascript to ask for notification permission,  
  register a serviceWorker and a pushManager,  
  fetch vapid public key from the backend,  
  subscribe to push notifications from the backend,  
  and display recieved notifications.  
* Implement backend python to send vapid public key to frontend,  
  recieve pushSubscription from frontend and send test notification
  to frontend.  
* Implement starlette lifespan to connect and disconnect from database  
  before starlette app is started and after it's stopped.  
* Implement starlette middleware to accept a wider scope of CORS,  
  which will be necessary once the app is deployed.  
* Improve docstrings everywhere

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 22.03.2024
### Tidsrom: 08:30 - 15:00, 17:00 - 22:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Maskinvare
## Undersøke tilgjengelig maskinvare
Undersøker maskinvare som kan være aktuell for å bruke i endelig produkt.  
Tynne klienter, minipc-er og énkortsmaskiner tilgjengelig nye og brukte blir undersøkt.

Ser også på maskinlæringsakseleratorer, hvor [Hailo AI](https://hailo.ai) sine Hailo-8 og Hailo-8L virker spesielt interessante. Finner ut at Hailo er et israelsk selskap, hvilket betyr at handel med dem medfører uforsonlige etiske ulemper, på grunn av Israels pågående folkemord av palestinere.  
Sender selskapet følgende epost: 

> Greetings!
> 
> We're developing a video surveillance solution, with the value proposition of alerting farmers immediately if predators approach their livestock; and repelling the predators before they can attack by deploying audio-visual repellants.  
> Many other use cases will be applicable in the future.  
> We want to sell customers an encapsulated hardware unit the size of a mini-pc or smaller, which they can deploy to their local network. The unit will ingest from 1 to 10 1920x1080 h264 and h265 video streams, perform object detection in near real time, and provide a web UI displaying analyzed images.
> 
> To suit our use case, we were evaluating entry-level Coral and Hailo AI accelerator cards.  
> It came to our attention that Hailo is based in Israel, which severely limits our ability to do business with you.  
> Our company values universal human rights and the laws of our country, which means we cannot support Israel or Israeli businesses while Israel continues its genocide and massacre of civilians in Palestine.  
> Please support efforts to end the genocide and reach a lasting peace agreement, so that our company may have the opportunity to purchase your products.
> 
> Best regards,  
> Jacob Ludvigsen

## Nødvendig maskinvare
Oppdager at maskinlæringsakseleratorer gjerne er spesialisert til én type modellarkitektur, selv om de kan støtte flere ulike rammeverk.
[YOLO](https://yolov8.org/yolov8-architecture/) er basert på CNN arkitekturen, imens [RT-DETR](https://arxiv.org/pdf/2304.08069.pdf) er basert på Transformers arkitekturen. Movidius Myriad-2 akseleratoren er god på DFF arkitekturen, imens Coral akseleratorene er gode på CNN og RNN. [TRON](https://arxiv.org/pdf/2303.12914.pdf) akseleratoren er best på Transformers arkitekturen. Designet for en annen [FPGA-basert akselerator for Transformer-arkitekturen](https://arxiv.org/abs/2304.03986) er tilgjengelig under MIT-lisensen.

[Denne artikkelen](https://openreview.net/pdf?id=PibYaG2C7An)  sammenlikner kjøring av transformer-modeller på et utvalg "edge" ML akseleratorer. De kom frem til at Coral Edge TPU funker greit for Transformer-arkitekturen, dersom man erstatter operasjoner i den spesifikke modellen som ikke støttes av akseleratoren; med operasjoner som er støttet. Dette er utenfor prosjektets fokusområde, og vil derfor ikke bli etterfulgt. 

Forsøkte en ordinær konvertering av RT-DETR modell i Pytorch-format til TensorFlow Lite ved hjelp av ultralytics, men konerteringen feilet fordi enkelte pytorch-operasjoner ikke har et konverteringsmål implementert i ONNX.  
Åpner et issue om det på [Ultralytics sitt repo](https://github.com/ultralytics/ultralytics/issues/9226).


# Emne 2: Utvikling
Undersøker hvorfor forhåndstrent RTDETR brukt via SAHI kun gir ut tall for gjenkjente klasser, heller enn navn slik YOLOv8 gjør under samme omstendigheter. For å finne ut av det, analyserer jeg samme bildet med begge modellene, og inspiserer `result.object_prediction_list` objektet. Der ser jeg at RTDETR har bare tall for både klassens id og navn, og yolov8 har klassens leselige navn som dens navn.

```python
>>> detection_model = AutoDetectionModel.from_pretrained(model_type="yolov8", model_path="models/yolov8x.pt", confidence_threshold=0.2)
>>> result = get_sliced_prediction("datasets/Cats.v2i.yolov8/lynx/images/501_png.rf.562f61b679a6e82fcdb4c7cb0006b538.jpg", detection_model)
Performing prediction on 1 number of slices.
>>> result.object_prediction_list
[ObjectPrediction<
    bbox: BoundingBox: <(22.01556396484375, 115.84844970703125, 612.646484375, 583.4599609375), w: 590.6309204101562, h: 467.61151123046875>,
    mask: None,
    score: PredictionScore: <value: 0.9248048663139343>,
    category: Category: <id: 15, name: cat>>]
>>> detection_model = AutoDetectionModel.from_pretrained(model_type="rtdetr", model_path="models/rtdetr-l.pt", confidence_threshold=0.2)
>>> result = get_sliced_prediction("/var/mnt/data/jacob/git/farm-animal-detection/datasets/Cats.v2i.yolov8/lynx/images/501_png.rf.562f61b679a6e82fcdb4c7cb0006b538.jpg", detection_model)
Performing prediction on 1 number of slices.
>>> result.object_prediction_list
[ObjectPrediction<
    bbox: BoundingBox: <(55.75307846069336, 114.40850830078125, 599.880126953125, 581.2420043945312), w: 544.1270484924316, h: 466.83349609375>,
    mask: None,
    score: PredictionScore: <value: 0.9240108132362366>,
    category: Category: <id: 15, name: 15>>]
```

Videre undersøkelse viser at dette også er tilfelle i Ultralytics biblioteket. [Åpner derfor et issue](https://github.com/ultralytics/ultralytics/issues/9238) i repoet deres, om å implementere navn fra COCO også på den forhåndstrente RTDETR modellen.

```python
>>> from ultralytics import RTDETR
>>> model_rtdetr = RTDETR('models/rtdetr-l.pt')
>>> model_rtdetr.names
{0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9', 10: '10', 11: '11', 12: '12', 13: '13', 14: '14', 15: '15', 16: '16', 17: '17', 18: '18', 19: '19', 20: '20', 21: '21', 22: '22', 23: '23', 24: '24', 25: '25', 26: '26', 27: '27', 28: '28', 29: '29', 30: '30', 31: '31', 32: '32', 33: '33', 34: '34', 35: '35', 36: '36', 37: '37', 38: '38', 39: '39', 40: '40', 41: '41', 42: '42', 43: '43', 44: '44', 45: '45', 46: '46', 47: '47', 48: '48', 49: '49', 50: '50', 51: '51', 52: '52', 53: '53', 54: '54', 55: '55', 56: '56', 57: '57', 58: '58', 59: '59', 60: '60', 61: '61', 62: '62', 63: '63', 64: '64', 65: '65', 66: '66', 67: '67', 68: '68', 69: '69', 70: '70', 71: '71', 72: '72', 73: '73', 74: '74', 75: '75', 76: '76', 77: '77', 78: '78', 79: '79'}


>>> from ultralytics import YOLO
>>> model_yolo = YOLO('models/yolov8x.pt')
>>> model_yolo.names
{0: 'person', 1: 'bicycle', 2: 'car', 3: 'motorcycle', 4: 'airplane', 5: 'bus', 6: 'train', 7: 'truck', 8: 'boat', 9: 'traffic light', 10: 'fire hydrant', 11: 'stop sign', 12: 'parking meter', 13: 'bench', 14: 'bird', 15: 'cat', 16: 'dog', 17: 'horse', 18: 'sheep', 19: 'cow', 20: 'elephant', 21: 'bear', 22: 'zebra', 23: 'giraffe', 24: 'backpack', 25: 'umbrella', 26: 'handbag', 27: 'tie', 28: 'suitcase', 29: 'frisbee', 30: 'skis', 31: 'snowboard', 32: 'sports ball', 33: 'kite', 34: 'baseball bat', 35: 'baseball glove', 36: 'skateboard', 37: 'surfboard', 38: 'tennis racket', 39: 'bottle', 40: 'wine glass', 41: 'cup', 42: 'fork', 43: 'knife', 44: 'spoon', 45: 'bowl', 46: 'banana', 47: 'apple', 48: 'sandwich', 49: 'orange', 50: 'broccoli', 51: 'carrot', 52: 'hot dog', 53: 'pizza', 54: 'donut', 55: 'cake', 56: 'chair', 57: 'couch', 58: 'potted plant', 59: 'bed', 60: 'dining table', 61: 'toilet', 62: 'tv', 63: 'laptop', 64: 'mouse', 65: 'remote', 66: 'keyboard', 67: 'cell phone', 68: 'microwave', 69: 'oven', 70: 'toaster', 71: 'sink', 72: 'refrigerator', 73: 'book', 74: 'clock', 75: 'vase', 76: 'scissors', 77: 'teddy bear', 78: 'hair drier', 79: 'toothbrush'}
```

Finner to relaterte issues: [#3563](https://github.com/ultralytics/ultralytics/issues/3563) og [#4092](https://github.com/ultralytics/ultralytics/issues/4092).


Implementerer [typene fra WebPush-py programvarebiblioteket](https://github.com/delvinru/webpush-py/blob/main/webpush/types.py), for å slippe å importere biblioteket i programvaren.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 23.03.2024
### Tidsrom: 09:00 - 15:00, 21:00 - 00:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
## Migrere fra Webgear og Starlette til FastAPI
Prosjektet bruker egentlig ikke WebGear noe særlig, derfor implementerer jeg den funksjonaliteten som faktisk brukes selv, og fjerner bruken av WebGear.  
Med dette grepet åpnes porten for bedre prosjektstruktur, bruk av FastAPI og bedre prosesskontroll.

FastAPI bygger på Starlette og Pydantic, så det eneste som trengs for å bytte over fra Starlette er å fastsette _typer_ på input i funksjoner, og endre importnavn fra starlette.* til fastapi.*.
Funker bra!

Gjør også endel andre småting, som kan ses på repoet denne dagen.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 24.03.2024
### Tidsrom: 09:00 - 12:00, 23:30 - 00:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
## implementere database for push-abbonnementer
Fortsetter arbeidet for å implementere webpush i Predalert.  
løypa er:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.  

I dag har jeg løst 3.  
Bruker FastAPIs SQLmodel for å ordne database til abbonnementsinfo.

## Separere funksjoner
Flytter funksjoner relatert til database og sending av webpush varsler ut fra web_ui  og inn i dedikerte filer.

## Fjerne hardkoding
Bytter ut flere hardkodede verdier med variabler som leses fra konfigurasjonsfil.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 25.03.2024
### Tidsrom: 14:00 - 17:00, 19:30 - 20:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Prosjektstyring
Skriver maler for avviksmeldinger og endringsmeldinger for alt som ikke har gått som planlagt hittil i prosjektet. For å lage dokumentene, benyttes LaTex-redigeringsprogrammet [Kile](https://kile.sourceforge.io/). Dette vil gi meg øvelse i LaTex, som kommer godt med når jeg skal skrive sluttrappporten på samme vis.  
Leter frem .bibtex referanser for mange av tingene som har vært viktige for prosjektet imens jeg venter på at LaTex (6GB) skal installeres.

# Emne 2: Rapportskriving
Begynner så smått å lage forside, referanseliste og disposjon for sluttrapporten i LaTex.
Funker greit, men er litt knotete siden jeg ikke vet de nødvendige kommandoene ennå.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 26.03.2024
### Tidsrom: 10:00 - 15:00, 17:00 - 18:00, 19:00 - 21:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Prosjektstyring
Fortsetter å lage maler for endrings- & avviksmeldinger i LaTex, og skrive meldingene som skulle vært skrevet hittil.  
Oppdaterer fremdriftsplanen, som ikke har blitt oppdatert siden 12.03.2024.


# Emne 2: Rapportskriving (1 time)
Lager disposjon til sluttrapport og begynner så smått å fylle inn.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 28.03.2024
### Tidsrom: 10:00 - 12:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Prøver å eksportere RT-DETR fra pytorch til Nvidias TensorRT (for effektiv kjøring på min Nvidia GPU) og Tensorflow-lite (for kjøring på Coral ML-akselerator). Lykkes ikke med tensorflow-lite grunnet programvarelus i konverteringsbibliotekene, og SAHI viser seg å ikke støtte TensorRT.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 29.03.2024
### Tidsrom: 13:00 - 18:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Ferdigstiller en fungerende (grunnleggende) ende-til-ende implementasjon av webpush-varsler.  
Løypa var:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.  

I dag har jeg løst 4 og 5.  
Nå vil alle abbonnerte klienter motta periodiske opssummeringer av gjenkjente objekter.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 30.03.2024
### Tidsrom: 09:30 - 12:00, 00:00 - 01:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Fortsetter arbeidet med å implementere Zero-Trust nettverking med OpenZiti.  
Benytter Ziti Edge Developer Sandbox (ZEDS).  
Støter på diverse utfordringer, som jeg skriver en [kommentar om på OpenZiti forumet](https://openziti.discourse.group/t/using-openziti-in-distributed-surveillance-system/2135/4)

Får til slutt til å nå Netfoundry's eksempel-tjenester servert over ziti, med laptopen min.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 01.04.2024
### Tidsrom: 10:00 - 12:00, 16:00 - 00:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Fortsetter arbeidet med å implementere Zero-Trust nettverking med OpenZiti.  
Finner ut at det oppstår konflikt mellom Tailscale og OpenZiti dersom begge kjører på samme maskin, som medfører at ingen ziti-tjenester kan nås. Melder ifra om det på OpenZiti-forumet, og slår Tailscale-tjenesten lokalt på maskinen min, midlertidig av for å bøte på  problemet.

NetFoundry's nfconsole viser seg å ha milevis bedre brukergrensesnitt og dokumentasjon enn ZEDS, og jeg går derfor over til å bruke den inntil videre. Etter noe krøll får jeg til å servere predalert nettstedet (uten videoer og push-varsler) over ziti, og nå nettstedet over ziti.

Oppdaterer pyproject.toml sånn at Predalert igjen kan installeres på andres maskiner, uten å behøve noen variabler eller programmer lokale til min maskin. Dette vil gjøre utvikling enklere, ettersom programmet er såpass komplekst nå at det er vanskelig å destillere ned hele funksjoner til en MVE (minimumseksempel), som man bør gjøre når man ber andre hjelpe til med feilsøking.  
Med et enkelt installerbart program, håper jeg at andre noenlunde enkelt kan forstå hvor feilen ligger, uten at jeg må bruke lang tid på å lage en MVE og verifisere at den gjenskaper hele feilen.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 02.04.2024
### Tidsrom: 10:00 - 15:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Prosjektstyring
Dokumenterer prosjektets fremdrift og veikart i en prosjektlogg på [Hackaday.](https://hackaday.io/project/191509-farmersopen-source-optical-predator-alert-system/log/228719-make-it-a-thesis-make-it-a-cooperative), laster opp opptak av når lokalradioen Radio Nordsalten intervjuet meg om FOSOPAS-prosjektet i mai 2023.

Oppdaterer fremdriftslogg

Inviterer veileder Halldor til digitalt møte fredag 05.04.2024.

# Emne 2: Litteraturstudie: alternativer
Undersøker hvorvidt rovviltsikre gjerder kan være en god passiv måte å holde saueinnhegninger trygge på. [Rovviltgjerder fra Poda](https://www.poda.no/media/gk2hmuvc/rovviltgjerde-type-i-122-sau.pdf) når 130cm over bakken og bruker strømtråd. Vinterstid er det ikke uhørt at snøen når en meter over bakken, så gjerdet vil ikke hindre rovvilt i å spasere gjennom innhegningen. Dersom de blir borti gjerdet vinterstid, gir snøen effektiv isolasjon mot det verste elektriske støtet. Sommerstid kan høye strømførende gjerder være en mer effektiv og hensiktsmessig beskyttelse mot rovvilt, enn den aktive metoden dette prosjektet tar for seg.  
Der hvor gjerder ikke egner seg, kan kamera (evt. påmontert flyvende drone) med skremmeutstyr funke godt.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 03.04.2024
### Tidsrom: 20:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Feilsøker OpenZiti integrasjonen i Predalert ved hjelp av Clint i OpenZiti forumet.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 04.04.2024
### Tidsrom: 09:30 - 12:00, 17:00 - 19:00
### Sted: UiT campus Bodø, Makerspace Bodø

---


# Emne 1: Prosjektstyring
## Forretningsutvikling
Setter ballen igang for å meisle ut veien videre etter bacheloroppgaven er presentert.  
Jeg har et enkeltpersonforetak, og ønsker å tjene penger på Predalert. Én mulighet jeg undersøker er å starte et samvirke for utvikling og salg av fri programvare og -maskinvare, og tilhørende tjenester. For dette trengs én person som kan økonomi, administrasjon og forretningsutvikling; og gjerne én til utvikler som fordelaktig har erfaring med webutvikling.  

- Sender en epost til vennen min Ørjan Pettersen og spør om han vet om noen som kan være interesserte i å bli med på reisen.  
- Sender en epost til Inkubator Salten, hvor jeg tidligere har hatt et prosjekt, og forteller om dette prosjektet og behovene som trengs.


> 
> Hvert av de siste 7 årene har 16 000 får beviselig blitt tatt av freda rovvilt, hvorav en god del av dem angripes inne i gjerdet sitt. Det er ofte vanskelig å bevise at et dødt dyr er tatt av freda rovvilt, og ikke tatt av annet rovvilt eller naturlig død. De reelle tallene er derfor større.
> Jeg utvikler Fri Programvare som bruker overvåkningskameraer, objektgjenkjenning og diverse alarmintegrasjoner for å avverge, og i verste fall dokumentere og varsle om, tap av husdyr til rovvilt.
> 
> En ulempe ved overvåkningskameraer er at de ironisk nok er notoriske for sine slapphendte holdninger til datasikkerhet. Flere ganger bare hittil i år har millioner av brukere av slike nettverkstilkoblede kameraer blitt utsatt for databrudd av diverse karakter. Dette prosjektets tilnærming isolerer disse kameraene fra resten av nettverket, og gjør at all deres kommunikasjon med omverdenen kun kan nå de spesifikke enhetene brukeren tillater.
> Prosjektet gjør det unødvendig å stole på noen tredjeparter, inkludert programvareutvikler/tilbyder.
> 
> Prosjektet er min bacheloroppgave i maskinteknikk ved UiT Narvik; og har hovedfokus på tillitsbygging i digitalisering og IoT gjennom transparens og digital sjølråderett.
> 
> Prosjektdokumentasjonen er tilgjengelig her: https://gitlab.com/papiris/ml-predator-control-thesis
> Programvaren utvikles her: https://gitlab.com/papiris/predator-detect-and-notify
> 
> Case-studie og nærmeste partner i bacheloroppgaven er Strandmoa Gård i Steigen, hvor fire sau ble tatt av gaupe i vinter. 
> 
> For å redusere "time to market" og øke sannsynligheten for suksess; søker jeg en partner som vil bli med på reisen videre, og kan ta seg av økonomi og administrasjon, samt delta i forretningsutviklinga. Én til utvikler med fokus på web vil også bidra sterkt til suksess.  
> 
> Mvh Jacob Ludvigsen  
> Ludvigsen Ingeniørskap og Fri Teknologi  
> org.nr 932 966 905  

## Veiledningsmøte
Forbereder til veiledningsmøte i morgen.


# Emne 2: Rapportskriving
TexStudio vil ikke lengre kompilere LaTex-dokumentet Sluttrapport, så jeg undersøker den oppdaterte UiT-thesis malen. Finner ikke løsningen, så jeg installerer en eldre versjon av texlive.  Dette hjelper heller ikke, men det hjalp å bruke TexMaker istedenfor TexStudio eller Kile; selv om tex-kommandoene de bruker tilsynelatende er identiske.  

Produserer en graf over de 3 nærmeste leddene av eksterne programvarebiblioteker som brukes i Predalert, ved å bruke pakken `pydeps`.  
`pydeps predalert --max-bacon=3 --cluster --no-show`


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
	# Arbeidsøkt timelogg

### Dato: 08.04.2024
### Tidsrom: 12:00 - 14:00, 21:00 - 23:00
### Sted: Steigentunet, Steigen; Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Finner ut av problemene med LaTex ved hjelp av Erlend Graff, og får oversendt utkast til disposisjon til veileder Halldor.


# Emne 2: Utvikling
Lager et minimums-eksempel av python-kode som trigger `feil 11, ukjent feil` ved bruk av openziti; og publiserer den i openziti-forumet. Vedlikeholderne repliserte feilen på sitt system, og arbeider med å gjøre openziti mer robust når det brukes i kombinasjon med diverse web-rammeverk.


# Emne 3: Prosjektstyring
Fører fremdriftslogg for uke 14.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 09.04.2024
### Tidsrom: 10:00 - 15:00, 17:00 - 21:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving (6 timer)
Arbeider videre på sluttrapportens disposisjon, samt prøver å fikse en programvarelus der alle kapitler blir underkapitler av kapitlene Takksigelser eller Sammendrag. Finner etterhvert ut at kapitler må defineres me `\chapter` og ikke `\section`.  

Finner ikke veiledning i bruk av LaTex-malen, ei heller hvordan man kan inkludere bacheloroppgavens påkrevde tabell i LaTex-dokumentet. Lager derfor en ønskerapport i repoet til [UiT-thesis latex-malen](https://github.com/egraff/uit-thesis/issues/87) om integrert støtte for tabellen, sender epost til UiTs skrivesenter og ber om veiledning i LaTex; og sender epost til veileder Halldor og spør om tabellen kan være formatert på annet vis enn i kravdokumentet.


# Emne 2: Utvikling
Lager et minimums-eksempel av python-kode som trigger `feil 11, ukjent feil` ved bruk av openziti i kombinasjon med `fastapi.responses.StreamingResponse`. Det viser seg at en simpel StreamingResponse som funker i en ordinær simpel nettside, også funker over openziti-nettverket. Dette betyr at jeg sannsynligvis har en implementasjonsfeil i webappen min PredAlert.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 10.04.2024
### Tidsrom: 21:00 - 24:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Omarbeider Predalert for å kunne strømme bilder over ziti-nettverket; og lykkes delvis. Kan strømme ett enkelt bilde fra hver kilde per omlasting, men bildene blir ikke erstattet.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 11.04.2024
### Tidsrom: 09:00 - 14:00, 19:00 - 23:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Videreutvikler minimumseksempelet for OpenZiti, ved å implementere `media_type="multipart/x-mixed-replace ` i `StreamingResponse` for tekst. Det funker halvveis.

Oppdager at `multipart/x-mixed-replace` ikke lengre er offisielt støttet i Firefox og Chrome, og at dette antageligvis ikke er veien å gå.

Oppdager at websocket protokollen er avslått på Ziti Routeren jeg bruker i Netfoundy, så den protokollen kan ikke brukes.

Er godt igang med å implementere fungerende Server-Sent Events (SSE) i det minimale eksempelet, når jeg oppdager at maksimalt antall kilder hvert domene kan lytte på når protokollen HTTP/1.1 og ikke HTTP/2 brukes; er 6. [Dokumentasjon MDN](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events)  
[Uvicorn](https://www.uvicorn.org/#introduction) har bare støtte for HTTP/1.1, men [Hyprcorn](https://hypercorn.readthedocs.io/en/latest/index.html) støtter HTTP/2 i tillegg.

Alle nettlesere krever TLS / SSL for å bruke HTTP/2 protokollen.

Går derfor over til å bruke hypercorn istedenfor uvicorn. Dokumentasjonen er mye mer tungtlest enn for uvicorn, så det tar litt tid.

Veikartet blir da:  
1. Sende bilder som base64 kodet tekst via SSE over openziti  
2. SSL over openziti (via SPIFFE?)  
3. All funksjonalitet over openziti  

I dag har jeg løst 1.







<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 12.04.2024
### Tidsrom: 10:00 - 12:00, 19:00 - 24:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Videreutvikler minimumseksempelet for OpenZiti, ved å implementere samme type arkitektur som hovedprosjektet.  
- Én hovedprosess som starter de andre prosessene  
- Én prosess som produserer bilder (fra webkamera)  
- Én prosess som gjør nettrelaterte ting  
- Underprosessene deler en kø for base64-kodede bilder  

Dette viser seg å perfekt gjenskape feil-oppførselen fra hovedprosjektet, når openziti brukes.

Manager.Queue er mer kompleks enn Manager.Pipe, derfor prøver jeg å bruke Pipe også.  
Som man kan se under, er q_img ulike typer objekter på ulike minneaddresser; noe det absolutt ikke skal være.  Hver prosess holder hver sine ender av Pipe, så det er ikke problematisk at addressene deres er ulike.

```python
q_img in run_webapp: <AutoProxy[Queue] object, typeid 'Queue' at 0x7ff6e338da90; '__str__()' failed>
pipe_img_recv in run_webapp: <multiprocessing.connection.Connection object at 0x7ff6e142a510>
q_img in frame_producer: <queue.Queue object at 0x7ff6dc933f10>
pipe_img in frame_producer: <multiprocessing.connection.Connection object at 0x7ff6e0bd4290>
```

Verifiserte at Pipe sender bilder til webprosessen når den kjører openziti, uten problemer.  
Når openziti ikke brukes, er kø-objektene like hverandre, og alt funker:  
```python
q_img in run_webapp: <queue.Queue object at 0x7f31142f2650>
pipe_img_recv in run_webapp: <multiprocessing.connection.Connection object at 0x7f31197da650>
[2024-04-12 23:08:06 +0200] [639254] [INFO] Running on http://127.0.0.1:8443 (CTRL + C to quit)
q_img in frame_producer: <queue.Queue object at 0x7f31142f2650>
pipe_img in frame_producer: <multiprocessing.connection.Connection object at 0x7f311423ac90>
```


Melder ifra om funnet på openziti forumet, i tråden til prosjektet mitt.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 13.04.2024
### Tidsrom: 21:00 - 24:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Risikovurdering
Gjør meg opp noen tanker rundt hva som kan utgjøre en etisk og samfunnsmessig risiko ved prosjektet.  
-  Det er effektivt for å overvåke mennesker, og umiddelbart varsle og agere via eksterne systemer når visse betingelser er oppfylt.  

Et kjerne-element i prosjektet er at det skal respektere hele brukerkjedens frihet og personvern, og minimere trusselen utgjort av proprietære alternativer. I dette ligger det derfor som premiss at programmet må være fritt, og at enhver som gjør programmet tilgjengelig for en bruker, også må gi brukeren de samme frihetene.  
Det er dessverre ingen provisjoner i [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html) for at "Personer som får sin data/informasjon behandlet av programmet" også skal få slike rettigheter. Det som derimot står, er at man ikke kan ilegge noen ytterligere restriksjoner eller betingelser, enn hva som fremkommer i lisensen.

Det er vanskelig å finne en lisens som møter alle kriteriene, derfor skriver jeg [en tråd på Mastodon](https://hachyderm.io/@papiris/112265777619006095) om situasjonen og ber om råd fra et knippe organisasjoner som arbeider med Fri Programvare-lisenser. Vi får se hva de svarer.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 15.04.2024
### Tidsrom: 12:00 - 16:00, 21:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Omarbeider webappen jeg lagde for feilsøking i OpenZiti, til et fullverdig python-prosjekt; og publiserer det i et git-repo. Utviklerne av OpenZiti ønsket dette, for å bedre kunne hjelpe meg.


# Emne 2: Rapportskriving
Omarbeider disposisjon til sluttrapporten etter innspill fra veileder

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 21.04.2024
### Tidsrom: 19:00 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
- Oppdaterer fremdriftsplan / timelogg med arbeid siden 04.04.2024   
- Skriver avvik på at det ble gjort lite arbeide i perioden 11.04 - 21.04.2024; grunnet helseutfordringer.  
- Skriver på Statusrapport 2


# Emne 2: Utvikling
- Hjelper utviklerne av OpenZiti med å kjøre eksempel-webappen min.  
- Videreutvikler eksempel-webappen for openziti, med større kompatibilitet og bedre feilsøkings-print samt dokumentasjon.  

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 22.04.2024
### Tidsrom: 08:00 - 16:00, 19:00 - 20:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
- Skriver mer på Statusrapport 2


# Emne 2: Utvikling
- Får openziti til å fungere med den utbroderte eksempel-webappen min; og deretter i Predalert!  
- Hjelper utviklerne av OpenZiti med å kjøre eksempel-webappen min.  
- Videreutvikler eksempel-webappen for openziti, med større kompatibilitet og bedre feilsøkings-print samt dokumentasjon.  

På veien videre trengs i programmet:  
1. Trening av maskinlæringsmodell  
2. SSL over openziti vha SPIFFE/SPIRE  
3. MQTT  
4. Mulighet til å avslutte abonnement på push-varsler  
5. Sensurering av personer i objektgjenkjenninga  
6. Mulighet til å velge hvilke 'klassenavn' som skal utløse varsel ved deteksjon  
7. Lagring av bildeserier med positiv deteksjon  
8. UI for å se og slette lagrede bildeserier  



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 23.04.2024
### Tidsrom: 08:00 - 09:00, 10:00 - 16:00, 18:00 - 18:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
Skriver mer på Statusrapport 2. Fyller inn kapitler og seksjoner med tekst, ordner vedlegg.

# Emne 2: Utvikling
Laget en ny "ziti service" i NFConsole med flere tillatte porter (80, 443, 8000-12000) enn originalt (kun port 80); for å teste om dét er grunnen til at SSL ikke fungerer over Openziti.  
Hypotesen traff, og nå funker SSL (selvsignert sertifikat) over openziti! Det åpner for HTTP/2 som tillater >6 SSE-tilkoblinger (videostrømmer), og WebPush; over OpenZiti.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 24.04.2024
### Tidsrom: 08:00 - 09:00, 10:00 - 16:00, 18:00 - 20:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
## Statusrapport 2
Skriver mer på Statusrapport 2. Fyller inn kapitler og seksjoner med tekst, ordner vedlegg.

Sender epost til studieleder for bachelor i maskinteknikk ved UiT Narvik, Øyvind Søraas; om at jeg har blitt invitert til et digitalt jobbintervju imorgen, og ber om råd. Han råder meg til å lage en knallgod statuspresentasjon for bacheloroppgaven, for å vise frem til intervjueren, og heller levere statusrapporten litt senere.  
Legger derfor fra meg arbeidet med statusrapporten, og begynner arbeidet med presentasjonen.

## Statuspresentasjon 2
Lager en lysbildepresentasjon med LibreOffice Impress. 	

## Endringsmelding 1
Lager den første endringsmeldinga i prosjektet, som tar for seg revisjon av aktivitetsbeskrivelser.





<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 25.04.2024
### Tidsrom: 09:00 - 11:00, 12:30 - 18:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Prosjektstyring
## Statusrapport 2
Skriver mer på Statusrapport 2. Fyller inn kapitler og seksjoner med tekst, ordner vedlegg.


## Statuspresentasjon 2
Ferdigstiller lysbildepresentasjon med mange bilder og illustrasjoner, en video, og teknikker for å gjøre det enklere for publikum å fokusere.





<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 26.04.2024
### Tidsrom: 09:00 - 12:30
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Prosjektstyring

## Statuspresentasjon 2
Presenterer statuspresentasjon for klassen, veiledere og lærere.  

# Emne 2: Rapportskriving
Prøver å finne ut av hvordan "toppteksten" i rapporten bør være.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 27.04.2024
### Tidsrom: 13:30 - 21:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Stig Strandmoa demonstrerer at overvåkningskameraene på gården har toveis audio, altså at det kan komme lydsignal av et akseptabelt volum ut av høyttaler på kameraene. Bruker derfor dagen på å undersøke og teste ulike metoder for å benytte seg av den funksjonen.  
Kameraene Foscam-kloner.  
Prøver ONVIF, men det går ikke. Prøver cgi, altså kameraets web-baserte API, og det funker ikke. Prøver RTSP bak-kanal, men det funker heller ikke. Det er altså likevel nødvendig å skaffe en ekstern dyreskremmer, siden kameraet ikke kan ha den rollen.

Sier meg ferdig med utviklingsfasen.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 28.04.2024
### Tidsrom: 18:30 - 23:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Fyller utkast inn i kapitler i sluttrapporten.  
Setter opp integrasjon mellom referanse-håndteringsprogramvaren [Zotero](https://www.zotero.org/) og TeXStudio.  
Finner oppdaterte kilder til enkelte data, og refererer til dem.

# Emne 2: Prosjektstyring
Hittil i prosjektet har gratisversjonen av OpenAI sin ChatGPT3.5 blitt brukt som [sparringspartner for feilsøking](https://en.wikipedia.org/wiki/Rubber_duck_debugging) og for å få en oversikt over diverse fagfelt og emneord å sette meg nærmere inn i. Dette har noen uheldige etiske implikasjoner, siden alle samtaler man har med modellen deles med OpenAI for videre trening av modellen. Det er et kjent fenomen at LLM-er kan produsere deler av datasettet de er trent på, ordrett, under visse omstendigheter. Selv om programvaren er lisensiert AGPLv3, ville dette omgått lisensen.

[Metas Llama3-modell](https://llama.meta.com/llama3/) skårer høyere enn ChatGPT3.5 på ytelsesmålinger, har en komparativt mer åpen lisens og kan kjøres lokalt.

Går derfor over til å bruke Llama3 som sparringspartner. Som server bruker jeg [Ollama](https://ollama.com/), og som brukergrensesnitt [Open WebUI](https://openwebui.com/).

Jeg har nå full kontroll over hvor mye data som deles med andre.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 29.04.2024
### Tidsrom: 11:00 - 16:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Fyller utkast inn i underkapitler Resultatmål og Etisk risiko i sluttrapporten.  
Lager tabeller for Milepæler og Beslutningspunkter.

# Emne 2: Prosjektstyring/trening av objektgjenkjenning
Sender epost til Øyvind Skogstad i Statens Naturoppsyn, som jeg snakket med på rovviltmøtet i Hamarøy 18. april. Jeg ber om tilgang til bildemateriale av gaupe for å trene objektgjenkjenning.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 30.04.2024
### Tidsrom: 11:00 - 17:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapittelet om system, med fokus på underkapittelet Programvare.  
Lager en oppdatert graf av avhengigheter med `pydeps`, og oppdager at grafikken er for bred. Finner ingen reell mulighet i `pydeps`for å begrense bredden av grafen, og åpner derfor et [funksjonsønske på programvarelageret deres](https://github.com/thebjorn/pydeps/issues/220)

Lærer å bruke LaTeX funksjonene for sitering, forkortelser og ordbok.

Lista over forkortelser og ordboka krever et ekstra kompileringssted for å bli vist som del av PDF-en, så jeg endrer bygge-kjeden fra  
`txs:///compile | txs:///view`  
til  
`txs:///compile | txs:///makeglossaries | txs:///view`  
Som funker.

Lager referanser for programvarebiblioteker brukt i utviklingen.

Legger til vedlegg.

Ber om tilbakemelding fra veileder.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 05.05.2024
### Tidsrom: 12:00 - 18:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Introduksjon og Litteraturstudie.

Søker opp og setter inn referanser og vedlegg.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 06.05.2024
### Tidsrom: 12:00 - 23:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Litteraturstudie og System.

Søker opp og setter inn referanser og vedlegg.


# Emne 2: Utvikling/Trening av maskinlæringsmodell
Norsk Institutt for Naturforskning (NINA) har en søkbar database med bilder av ulike dyrearter. Lisensen tillater ikke videredeling, men sånn jeg forstår det, er det innafor å bruke bildene som treningsmateriale for en maskinlæringsmodell som kun skal kunne _gjenkjenne_ liknende objekter.

Undersøker nettsiden deres https://viltkamera.nina.no og finner ut hvilke underaddresser som kalles når et bilde åpnes. Undersøker også responsen. Basert på dette, og råd fra LLM-en Llama3 skriver jeg et skrape-skript som går gjennom alle lokalitetene og lagrer alle bilder av gaupe. Skriptet har noen ilagte pauser for å forhindre overbelastning av NINAs servere.  

Skriptet er publisert her: https://gitlab.com/papiris/lynx-scraper


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 07.05.2024
### Tidsrom: 09:00 - 11:00, 17:00 - 19:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Introduksjon, Litteraturstudie og Programvare.

Søker opp og setter inn referanser og vedlegg.


# Emne 2: Utvikling/Trening av maskinlæringsmodell
Skriptet lagret 10550 gaupebilder, de fleste av kurant kvalitet for trening.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 08.05.2024
### Tidsrom: 11:00 - 17:00, 23:00 - 00:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Introduksjon, Litteraturstudie, System og Risikoanalyse.
Fokus på seksjonene Målsetninger, Kritisk suksessfaktor, Arktiektur

Søker opp og setter inn referanser og vedlegg.

Begynner så smått å lage arkitekturdiagram for programvaren med "Diagram as Code"-metoden. Bruker [Diagrams](https://diagrams.mingrammer.com/docs/nodes/c4)


# Emne 2: Utvikling/Trening av maskinlæringsmodell
Sjekker lovligheten av skriptet for skraping av NINAs database, og bruk av bildene deres i prosjektet. Det ser ut til at jeg er innafor.

https://lovdata.no/lov/2018-06-15-40/§6 om bearbeiding av opphavsrett-beskyttet materiale

https://lovdata.no/lov/2018-06-15-40/§24 om databaser

https://lovdata.no/static/NLX3/31996l0009.pdf EUs databasedirektiv som sier "kopiering og bruk av lukkede databaser i vitenskapelig forskning er OK" i kapittel 2, artikkel 6 punkt 2(b)

Sender epost til veileder Halldor for å få hans syn på saken.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 09.05.2024
### Tidsrom: 13:15 - 17:00, 20:30 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
Installerer label-studio på datamaskinen, for å kunne annotere alle datasett på en lokal måte.

Installasjon på kubernetes via helm.  
Lager persistentVolume og persistentVolumeClaim for datasett og applikasjonslagring.  
Lager ingress med Traefik.  
<!--Utvikler en RT-DETR backend for label-studio, etter inspirasjon fra tre andre backender med ultralytics/yolov8. Backenden gir forhåndsutfylte annoteringer, slik at det tar kortere tid på store datasett.-->

Møter på trøbbel med at "/data/local-files/" sies å ikke eksistere. Noen timer feilsøking uten resultat.  
Prøver derfor label-studio installert via pip istedenfor helm. Får da problemer med at label-studio krever en spesifikk versjon av psycopg, som må bygges. Bygging av psycopg har litt tungvinte avhengigheter, så det tar en time.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 10.05.2024
### Tidsrom: 10:15 - 17:00, 20:00 - 23:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
Feilsøking viser at Label Studio ikke takler import av filnavn som har tegn utenom utf-8. Forsøk på å synkronisere en kilde som har slike filer i seg, ødelegger kilden permanent.

Jeg følger [dette issuet sine reparasjonssteg](https://github.com/HumanSignal/label-studio/issues/4874), som innebærer omdøping av støtende filnavn og sletting av støtende enheter i appens database. Dette fikser saken!

Installerer så en [ML-backend](https://github.com/kieefuanh/LabelStudio-Yolov8-Detection-Backend) for å få forhåndsutfylte annoteringer av bilder. Endrer skriptet for å bruke RT-DETR. Det funket ikke helt, fordi skriptet ikke implementerer korrekte funksjoner. (setup() er viktigst her)

Prøver deretter en [docker-compose-variant av backenden](https://github.com/35grain/label-studio-yolov8-backend), med mer komplett implementasjon.

Får koblet dem sammen, men det viser seg at backenden har en innebygd blokkering, som gjør at dersom URL'en inneholder domenet "localhost", funker ikke lokal lagring. Bruker mye tid på å finne ut av omveier, men lykkes ikke i dag.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 11.05.2024
### Tidsrom: 10:00 - 17:00, 19:00 - 02:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
Klarer å få labelstudio til å tjenes fra domenet labelstudio.lan, ved å legge til følgende linje i `/etc/hosts` på datamaskinen min:  
`127.0.0.1 labelstudio.lan`  
og gjøre tilsvarende endring i labelstudio sin values.yaml for helm/kubernetes.


Utvikler backenden til å fungere!  
Den kan nå gi forslag til annoteringer av bilder, og er godt til hjelp.  
Trener modellen med de annoterte bildene, og forbedrer den.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 12.05.2024
### Tidsrom: 11:00 - 17:00, 22:30 - 00:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
videreutvikler backenden til labelstudio litt, for økt driftssikkerhet og brukervennlighet. Ultralytics støtter mange ulike objektgjenkjenningsmodeller, og jeg mener på at biblioteket har en "auto-modus" som inspiserer hvilken modell man laster inn, og velger riktig kjøremodus ut fra det. Det gjør at backenden jeg har skrevet ikke er begrenset til RTDETR, og kan brukes mer fritt. Siden jeg bruker ultralytics sitt bibliotek, må backenden lisensieres AGPL-v3. Det hadde jeg tenkt å gjøre uansett, så ingen problem. Label-Studio sin "boilerplate" ml-backend som min backend baseres på, er lisensiert Apache-2.0, så det er heller inget problem. 

Har nå annotert 400 bilder av gaupe, og 40 bilder av sau; med stadig mer hjelp av maskinlæringsmodellen.  
Starter en treningsrunde på de hittil annoterte datasettene, og ser resultatene.

Det viser seg at ultralytics automatisk balanserer datasett, sånn at antall bilder for hver signifikante objektklasse holdes likt. Derfor tok treninga kun for seg 40 bilder hver av sau og gaupe.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 14.05.2024
### Tidsrom: 21:00 - 00:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Starter skrivinga på kapitlene Resultat, Konklusjon og Diskusjon.

Skriver noen ordlistedefinisjoner.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 15.05.2024
### Tidsrom: 13:00 - 23:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Fortsetter skrivinga på kapitlene Resultat, Konklusjon og Diskusjon.

# Emne 2: Utvikling / Trening av objektgjenkjenning (7 timer)
Videreutvikler backenden, og trener objektgjenkjenningsmodellen.

Møter noen utfordringer med ulik håndtering av bl.a. mellomrom i filnavn, mellom backenden og frontenden, og lager en halvgrei omvei.


	


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 16.05.2024
### Tidsrom: 09:00 - 13:00, 18:00 - 24:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapitlene System, Litteraturstudie og Verktøy, og finner referanser.

Lager system- og konteinerdiagrammer med Python-pakken `diagrams`.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 17.05.2024
### Tidsrom: 17:00 - 20:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving

Lager system- og konteinerdiagrammer med Python-pakken `diagrams`.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 18.05.2024
### Tidsrom: 15:00 - 21:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving

Lager system- og konteinerdiagrammer med Python-pakken `diagrams`.

Skriver på kapitlene System og Litteraturstudie




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 19.05.2024
### Tidsrom: 12:00 - 16:00, 18:00 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring (6 timer)
Selv om det har vært gjort beslutninger i prosjektet, har den formelle dokumentasjonen for beslutningene ikke blitt ført før nå. Ettersom det ikke er ført tilstrekkelig dokumentasjon underveis i prosjektet, registreres de med omtrentlig dato for utførelse der det er nødvendig.

Skriver dokumentasjon for beslutningspunkter 01-07, avviksmeldinger 05-06 og endringsmeldinger 02-03.

Ordner møtedokumenter, og legger dem ved sluttrapport.

Oppdaterer fremdriftsplan delvis.


# Emne 2: Rapportskriving
Ordner på tabeller, særlig beslutningspunkter og milepæler.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 20.05.2024
### Tidsrom: 12:00 - 13:00,  15:00 - 17:00, 19:00 - 22:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling (30 min)
Utvikler funksjon i Predalert som sladder skikkelsen av personer med et ugjennomsiktig rektangel, dersom objektet "person" gjenkjennes. Fikser også sletting av gjenkjenning av "person" fra gjenkjenningsdetaljer, som jeg forsøkte å implementere tidligere.

# Emne 2: Rapportskriving
Skriver på kapitlene System og Litteraturstudie, med fokus på seksjonene Arkitektur, Konseptutvikling og Programvare.

Ordner flere vedlegg.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 21.05.2024
### Tidsrom: 12:00 - 13:00,  15:00 - 21:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapitlene Konklusjon, System, Sammendrag og Diskusjon, med fokus på seksjonene konseptutvikling, trening av maskinløringsmodell, videre arbeid.

Fikser tabell over beslutningspunkter.

Ordner vedlegg relatert til maskinlæring.

Ordner referanser. Forfatter, dato og slikt.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 22.05.2024
### Tidsrom: 09:00 - 15:00,  16:00 - 21:00, 00:00 - 00:30
### Sted: Håsand Gård og Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Lager tabeller for utvelgelseskriterier

Bruker 2  timer på å finne ut hvordan man viser en forkortelse som også har en forklaring i latex. (hint, \ newdualentry)

Skriver om datasikkerhet, avverging, objektgjenkjenning og videre arbeid


# Emne 2: Testing (30min)
Under kveldsfôring på Strandmoa Gård blir jeg oppmerksom på gaupehyl bortenfor ene sauegjerdet, 100-150 meter unna. Jeg tar med Border Collie bikkjene våre, og går mot lyden. Det viser seg at gaupa er bak lauvskog på andre siden av Stamsvikelva, som renner langsmed sauegjerdet. Jeg oppfordrer bikkjene til å bjeffe, og de setter i et álo. Gaupa slutter å hyle etter noen minutter.  
Jeg vasser over elva med den lydigste av bikkjene, og starter sporing. Vi ser at gaupa har vandret rundt på platået og daltoppen, og langs etter elvebredden. Når vi mister sporet, har gaupa stukket seg opp mot fjellet, i motsatt retning av hundebjeffene. Den har trolig vært på leting etter et sted å krysse elva, kanskje for å angripe sauene.

Hundebjeff ser ut til å ha en viss effekt på gaupe.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 23.05.2024
### Tidsrom: 09:00 - 12:00, 15:30 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapitlene Diskusjon, Tap av får


# Emne 2: Utvikling (6 timer)

Konteineriserer Predalert programvaren.  
Konteineren baseres på Rocky Linux 9, og inneholder kun de ytterlige avhengigheter som trengs for å kjøre programvaren. 

Jeg finner ut hvilke pakker som kreves ved å bruke [distrobox](https://github.com/89luca89/distrobox). I en distrobox basert på grunnbildet, kan jeg enkelt eksperimentere for å se hvilke pakker og innstillinger som er nødvendige for programvaren.

Med en `Containerfile` og små endringer i programvaren (baner, konfigurasjonsinnlasting, miljøvariabler), får jeg en fungerende konteiner.  
Kan kjøres slik fra git repoets rotbane:  
```bash
podman run -v config:/config:z \
    -v "./example videos":"/example videos":z \
    -v ./models:/models:z \
    -p 8443:8443 \
    --security-opt=label=disable \
    --device=nvidia.com/gpu=all  \
    predalert
```

Neste steg er å lage en `compose.yaml`-fil, som kan starte konteineren programmatisk.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 24.05.2024
### Tidsrom: 09:00 - 10:00, 11:00 - 16:00, 19:30 - 21:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på System, og annet.

# Emne 2: Utvikling (2,5 timer)

Konteineriserer Predalert programvaren.  
Lager fungerende `compose.yaml`, så nå kan programmet startes med kun `podman-compose up`; gitt en fungerende konfigurasjonsfil

# Emne 3: Prosjektstyring (4 timer)
Fyller ut fremdriftsplan f.o.m. 23. april. Hang en måned på etterslep der ja.

## Veiledningsmøte 4
Møter Halldor kl 15 - 15:50


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 25.05.2024
### Tidsrom: 09:00 - 11:00, 13:00 - 14:00, 15:00 -18:30, 19:30 - 21:00, 21:30 - 23:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Innarbeider Halldors forslag i rapporten


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 26.05.2024
### Tidsrom: 11:00 - 18:00, 22:30 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Innarbeider Halldors forslag i rapporten.

Forbedrer grafikker og overganger mellom temaer.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
# Arbeidsøkt timelogg

### Dato: 27.05.2024
### Tidsrom: 13:00 - 23:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Innarbeider Halldors forslag i rapporten.

Forbedrer siteringer, henviser til prosjektets online dokumentasjonsarkiv.

Oppdaterer bilder.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
\newpage
