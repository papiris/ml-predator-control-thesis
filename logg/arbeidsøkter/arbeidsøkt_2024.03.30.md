# Arbeidsøkt timelogg

### Dato: 30.03.2024
### Tidsrom: 09:30 - 12:00, 00:00 - 01:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Fortsetter arbeidet med å implementere Zero-Trust nettverking med OpenZiti.  
Benytter Ziti Edge Developer Sandbox (ZEDS).  
Støter på diverse utfordringer, som jeg skriver en [kommentar om på OpenZiti forumet](https://openziti.discourse.group/t/using-openziti-in-distributed-surveillance-system/2135/4)

Får til slutt til å nå Netfoundry's eksempel-tjenester servert over ziti, med laptopen min.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
