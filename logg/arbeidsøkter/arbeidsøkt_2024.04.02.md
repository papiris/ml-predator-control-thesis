# Arbeidsøkt timelogg

### Dato: 02.04.2024
### Tidsrom: 10:00 - 15:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Prosjektstyring
Dokumenterer prosjektets fremdrift og veikart i en prosjektlogg på [Hackaday.](https://hackaday.io/project/191509-farmersopen-source-optical-predator-alert-system/log/228719-make-it-a-thesis-make-it-a-cooperative), laster opp opptak av når lokalradioen Radio Nordsalten intervjuet meg om FOSOPAS-prosjektet i mai 2023.

Oppdaterer fremdriftslogg

Inviterer veileder Halldor til digitalt møte fredag 05.04.2024.

# Emne 2: Litteraturstudie: alternativer
Undersøker hvorvidt rovviltsikre gjerder kan være en god passiv måte å holde saueinnhegninger trygge på. [Rovviltgjerder fra Poda](https://www.poda.no/media/gk2hmuvc/rovviltgjerde-type-i-122-sau.pdf) når 130cm over bakken og bruker strømtråd. Vinterstid er det ikke uhørt at snøen når en meter over bakken, så gjerdet vil ikke hindre rovvilt i å spasere gjennom innhegningen. Dersom de blir borti gjerdet vinterstid, gir snøen effektiv isolasjon mot det verste elektriske støtet. Sommerstid kan høye strømførende gjerder være en mer effektiv og hensiktsmessig beskyttelse mot rovvilt, enn den aktive metoden dette prosjektet tar for seg.  
Der hvor gjerder ikke egner seg, kan kamera (evt. påmontert flyvende drone) med skremmeutstyr funke godt.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
