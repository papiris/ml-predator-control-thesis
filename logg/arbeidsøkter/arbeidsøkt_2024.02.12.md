
# Arbeidsøkt timelogg

### Dato: 12.02.2024
### Tidsrom: 10:30 - 15:30, 22:30 - 23:30
### Sted: Tjeldberget, Bodø

---

# Emne 1: litteraturstudie

## Sammenstille litteraturstudie
Utbrodere kapittel 1: Eksisterende løsninger. Skrive inn informasjon om ulike løsninger for kameraovervåkning og vert-til-vert nettverkskommunikasjon.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
