# Arbeidsøkt timelogg

### Dato: 06.05.2024
### Tidsrom: 12:00 - 23:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Litteraturstudie og System.

Søker opp og setter inn referanser og vedlegg.


# Emne 2: Utvikling/Trening av maskinlæringsmodell
Norsk Institutt for Naturforskning (NINA) har en søkbar database med bilder av ulike dyrearter. Lisensen tillater ikke videredeling, men sånn jeg forstår det, er det innafor å bruke bildene som treningsmateriale for en maskinlæringsmodell som kun skal kunne _gjenkjenne_ liknende objekter.

Undersøker nettsiden deres https://viltkamera.nina.no og finner ut hvilke underaddresser som kalles når et bilde åpnes. Undersøker også responsen. Basert på dette, og råd fra LLM-en Llama3 skriver jeg et skrape-skript som går gjennom alle lokalitetene og lagrer alle bilder av gaupe. Skriptet har noen ilagte pauser for å forhindre overbelastning av NINAs servere.  

Skriptet er publisert her: https://gitlab.com/papiris/lynx-scraper


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
