
# Arbeidsøkt timelogg

### Dato: 03.02.2024
### Tidsrom: 12:00 - 12:30, 14:30 - 15:30, 21:30 - 22:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: litteraturstudie

## Nasjonal Sikkerhetsmyndighets Grunnprinsipper for IKT-sikkerhet (NSM GP-IKT)
Et offentlig tilgjengelig rammeverk, koblet mot ISO/IEC 27002:2022, tilpasset NSMs vurderinger
- Inneholder 118 sikkerhetstiltak, fordelt på 21 prinsipper og 4 kategorier
- Lettlest og forståelig, lite vanskelig fagterminologi
Siden denne standarden er like gyldig som ISO/IEC 27002, men denne er betydelig lettere å lese;
og dermed å etterleve, tror rett og slett dette blir standarden jeg skal følge.
- Det er ingen krav om at virksomheter som ikke er underlagt sikkerhetsloven må følge NSM GP-IKT




## Rammeverk for brukergrensesnitt
### Rust
Egui https://github.com/emilk/egui  
Yew https://github.com/yewstack/yew  
Dioxus https://github.com/dioxuslabs/dioxus  
Iced https://iced.rs/



## Dypdykk hos Veilid
Et dypdykk i [Veilid teamets Discord server](https://discord.com/invite/5Qx3B9eedU) viser at de ikke offisielt har startet utviklingen av håndtering av lagring og sending av datablokker på nettverket; kun 64bit chunker. Ei heller har de begynt å utvikle protokoller for sending av mediafiler og strømmer. En implementasjon av WebRTC er i planleggingsfasen.  
Det at disse funksjonene ikke er tilgjengelige, diskvalifiserer Veilid umiddelbart fra 


---

# Konstruksjonsgrunnlag
## Hva skal utføres?
1. koble videostrøm, samhandling med tjener og varsling til mobil enhet både innenfor og utenfor det lokale nettverket
2. koble videostrøm, samhandling med tjener og varsling til datamaskin innenfor og utenfor det lokale nettverket
3. All personlig informasjon skal være kryptert i transitt
4. mulighet for å samhandle med enheten plassert lokalt i nettverket
5. Alt av overvåkningskameraenes internett-tilgang skal skje gjennom denne slusen, som ikke tillater andre tilkoblinger enn til godkjente enheter.
6. Så lite behov for lokal konfigurering som overhodet mulig
7. Skal funke uavhengig av kundens installerte nettverksruter
8. Multifaktor-autentisering
9. Være tilgjengelig for folk med funksjonsvariasjoner

## Hva må til for å gjøre det?
1 & 2: Det enkleste for å fer å bruke en eksisterende applikasjonssuite.  
1 & 2: Det nest enkleste, dersom noe skal bygges fra grunnen, er å lage en progressiv webapp, som kan "installeres" på skrivebordet på pc og telefon.  
3: En omvendt proxy som Traefik kan konfigureres til å bruke et inkludert sertifikat for å kryptere trafikk til https  
4: Et brukergrensesnitt, duplex kommunikasjon, og håndtering av brukerhendelser på tjenersiden.  

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
