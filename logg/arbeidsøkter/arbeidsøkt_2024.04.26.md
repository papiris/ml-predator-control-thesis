# Arbeidsøkt timelogg

### Dato: 26.04.2024
### Tidsrom: 09:00 - 12:30
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Prosjektstyring

## Statuspresentasjon 2
Presenterer statuspresentasjon for klassen, veiledere og lærere.  

# Emne 2: Rapportskriving
Prøver å finne ut av hvordan "toppteksten" i rapporten bør være.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
