# Arbeidsøkt timelogg

### Dato: 24.03.2024
### Tidsrom: 09:00 - 12:00, 23:30 - 00:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
## implementere database for push-abbonnementer
Fortsetter arbeidet for å implementere webpush i Predalert.  
løypa er:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.  

I dag har jeg løst 3.  
Bruker FastAPIs SQLmodel for å ordne database til abbonnementsinfo.

## Separere funksjoner
Flytter funksjoner relatert til database og sending av webpush varsler ut fra web_ui  og inn i dedikerte filer.

## Fjerne hardkoding
Bytter ut flere hardkodede verdier med variabler som leses fra konfigurasjonsfil.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
