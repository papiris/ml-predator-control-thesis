# Arbeidsøkt timelogg

### Dato: 23.05.2024
### Tidsrom: 09:00 - 12:00, 15:30 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapitlene Diskusjon, Tap av får


# Emne 2: Utvikling (6 timer)

Konteineriserer Predalert programvaren.  
Konteineren baseres på Rocky Linux 9, og inneholder kun de ytterlige avhengigheter som trengs for å kjøre programvaren. 

Jeg finner ut hvilke pakker som kreves ved å bruke [distrobox](https://github.com/89luca89/distrobox). I en distrobox basert på grunnbildet, kan jeg enkelt eksperimentere for å se hvilke pakker og innstillinger som er nødvendige for programvaren.

Med en `Containerfile` og små endringer i programvaren (baner, konfigurasjonsinnlasting, miljøvariabler), får jeg en fungerende konteiner.  
Kan kjøres slik fra git repoets rotbane:  
```bash
podman run -v config:/config:z \
    -v "./example videos":"/example videos":z \
    -v ./models:/models:z \
    -p 8443:8443 \
    --security-opt=label=disable \
    --device=nvidia.com/gpu=all  \
    predalert
```

Neste steg er å lage en `compose.yaml`-fil, som kan starte konteineren programmatisk.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
