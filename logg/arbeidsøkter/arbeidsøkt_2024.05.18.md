# Arbeidsøkt timelogg

### Dato: 18.05.2024
### Tidsrom: 15:00 - 21:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving

Lager system- og konteinerdiagrammer med Python-pakken `diagrams`.

Skriver på kapitlene System og Litteraturstudie




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
