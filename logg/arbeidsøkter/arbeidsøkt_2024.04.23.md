# Arbeidsøkt timelogg

### Dato: 23.04.2024
### Tidsrom: 08:00 - 09:00, 10:00 - 16:00, 18:00 - 18:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
Skriver mer på Statusrapport 2. Fyller inn kapitler og seksjoner med tekst, ordner vedlegg.

# Emne 2: Utvikling
Laget en ny "ziti service" i NFConsole med flere tillatte porter (80, 443, 8000-12000) enn originalt (kun port 80); for å teste om dét er grunnen til at SSL ikke fungerer over Openziti.  
Hypotesen traff, og nå funker SSL (selvsignert sertifikat) over openziti! Det åpner for HTTP/2 som tillater >6 SSE-tilkoblinger (videostrømmer), og WebPush; over OpenZiti.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
