# Arbeidsøkt timelogg

### Dato: 15.04.2024
### Tidsrom: 12:00 - 16:00, 21:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Omarbeider webappen jeg lagde for feilsøking i OpenZiti, til et fullverdig python-prosjekt; og publiserer det i et git-repo. Utviklerne av OpenZiti ønsket dette, for å bedre kunne hjelpe meg.


# Emne 2: Rapportskriving
Omarbeider disposisjon til sluttrapporten etter innspill fra veileder

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
