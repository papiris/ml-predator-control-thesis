# Arbeidsøkt timelogg

### Dato: 12.05.2024
### Tidsrom: 11:00 - 17:00, 22:30 - 00:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
videreutvikler backenden til labelstudio litt, for økt driftssikkerhet og brukervennlighet. Ultralytics støtter mange ulike objektgjenkjenningsmodeller, og jeg mener på at biblioteket har en "auto-modus" som inspiserer hvilken modell man laster inn, og velger riktig kjøremodus ut fra det. Det gjør at backenden jeg har skrevet ikke er begrenset til RTDETR, og kan brukes mer fritt. Siden jeg bruker ultralytics sitt bibliotek, må backenden lisensieres AGPL-v3. Det hadde jeg tenkt å gjøre uansett, så ingen problem. Label-Studio sin "boilerplate" ml-backend som min backend baseres på, er lisensiert Apache-2.0, så det er heller inget problem. 

Har nå annotert 400 bilder av gaupe, og 40 bilder av sau; med stadig mer hjelp av maskinlæringsmodellen.  
Starter en treningsrunde på de hittil annoterte datasettene, og ser resultatene.

Det viser seg at ultralytics automatisk balanserer datasett, sånn at antall bilder for hver signifikante objektklasse holdes likt. Derfor tok treninga kun for seg 40 bilder hver av sau og gaupe.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
