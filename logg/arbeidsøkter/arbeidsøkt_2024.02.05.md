
# Arbeidsøkt timelogg

### Dato: 05.02.2024
### Tidsrom: 14:30 - 15:00
### Sted: Steigen Vertshus, Steigen

---


# Emne 1: litteraturstudie

## NSM Grunnprinsipper
Disse tiltakene prioriterer NSM høyest, fordi mangel på korrekt implementering av disse som oftest er rotårsaken til at dataangrep finner sted.  
Nummeret tilsvarer deres tiltaks-ID i NSM GP  
1.2.3  Kartlegg enheter i bruk i virksomheten.  
1.2.4  Kartlegg programvare i bruk i virksomheten.  
2.1.2  Kjøp moderne og oppdatert maskin- og programvare.  
2.1.9  Ta ansvar for virksomhetens sikkerhet også ved tjenesteutsetting.  
2.2.3  Del opp virksomhetens nettverk etter virksomhetens risikoprofil.  
2.3.1  Etabler et sentralt styrt regime for sikkerhetsoppdatering.  
2.3.2  Konfigurer klienter slik at kun kjent programvare kjører på dem.  
2.3.3  Deaktiver unødvendig funksjonalitet.  
2.3.7  Endre alle standardpassord på IKT-produktene før produksjonssetting.  
2.6.4  Minimer rettigheter til sluttbrukere og spesialbrukere.  
2.6.5  Minimer rettigheter på drifts-kontoer.  
2.9.1  Legg en plan for regelmessig sikkerhetskopiering av alle virksomhetsdata.  
3.2.3  Avgjør hvilke deler av IKT-systemet som skal overvåkes.  
3.2.4  Beslutt hvilke data som er sikkerhetsrelevant og bør samles inn.  
4.1.1  Etabler et planverk for hendelseshåndtering.  


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
