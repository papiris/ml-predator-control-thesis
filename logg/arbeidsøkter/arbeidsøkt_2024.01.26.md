
# Arbeidsøkt timelogg

### Dato: 26.01.2024
### Tidsrom: 09:00 - 20:00, pause 16:00 - 18:00
### Sted: Steigen, Strandmoa Gård


-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; Kvalitetsstyring, Gjennomføring, Aktivitetsbeskrivelser, Fremdriftsplan.


-----

# Emne 2: Presentere prosjektbeskrivelse
prosjektbeskrivelsen er ikke ferdig enda når jeg har presentasjon kl 14:20, og jeg har ikke begynt på en presentasjon.
Derfor presenterer det jeg har ferdig til da.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
