# Arbeidsøkt timelogg

### Dato: 19.05.2024
### Tidsrom: 12:00 - 16:00, 18:00 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring (6 timer)
Selv om det har vært gjort beslutninger i prosjektet, har den formelle dokumentasjonen for beslutningene ikke blitt ført før nå. Ettersom det ikke er ført tilstrekkelig dokumentasjon underveis i prosjektet, registreres de med omtrentlig dato for utførelse der det er nødvendig.

Skriver dokumentasjon for beslutningspunkter 01-07, avviksmeldinger 05-06 og endringsmeldinger 02-03.

Ordner møtedokumenter, og legger dem ved sluttrapport.

Oppdaterer fremdriftsplan delvis.


# Emne 2: Rapportskriving
Ordner på tabeller, særlig beslutningspunkter og milepæler.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
