
# Arbeidsøkt timelogg

### Dato: 04.02.2024
### Tidsrom: 21:20 - 21:50
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: litteraturstudie

## Verktøy for statisk svakhetsanalyse av programvarekildekode
Før jeg velger hvilke eksisterende løsninger prosjektet skal bygge på, er det lurt å danne seg et bilde over etablert sikkerhetspraksis i løsningene.
Dersom det er mange svakheter i kildekoden som kan oppdages av automatiserte verktøy, er nok sikkerhet ikke i løsningens førersete.
Det er likevel viktig å tenke på at verktøy for statisk kodeanalyse har rykte på seg for å produsere mange falske positiver.

- https://owasp.org/www-community/Source_Code_Analysis_Tools
- ASH integrerer mange verktøy: https://github.com/awslabs/automated-security-helper

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
