# Arbeidsøkt timelogg

### Dato: 14.05.2024
### Tidsrom: 21:00 - 00:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Starter skrivinga på kapitlene Resultat, Konklusjon og Diskusjon.

Skriver noen ordlistedefinisjoner.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
