
# Arbeidsøkt timelogg

### Dato: 20.02.2024
### Tidsrom: 11:00 - 14:00, 15:00 - 17:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Sammenlikning av Tailscale, Nebula, Netmaker og ZeroTier

Leser resten av sammenlikningen.  
- ZeroTier er uaktuelt pga mangel på tilgangskontroll-lister (ACL), samt dårlig ytelse.  
- Tailscale's ytelse er noe mer variabel enn Nebulas og Netmakers.  
- Tailscale bruker betydelig mer minne enn alternativene, og minnebruken er svært variabel i testingen deres. Tailscale har brukt opp mot 1 GB i testing, mot Nebulas og Zerotiers stabile 25MB og 10MB respektivt. Netmaker bruker Wireguard kjernemodulen på Linux og er derfor vanskelig å måle men virker ressursgjerrig.
- Gjennomflyt per CPU-kjerne er best hos Tailscale, grunnet deres avlasting av nettverksruting til Linux-kjernen. Det har antagelig også sammenheng med den økte minnebruken.
- Nebula skal visstnok være mer ressursgjerrig og høytytende enn alternativene på plattformer som ikke er Linux, da alternativene hovedsaklig er optimalisert for Linux.


## Deepdive Headscale dokumentasjon
Det ser ut til at Headscale har svært begrenset støtte for ACL'er per nå; og det er allerede etablert at Headscale ikke har intensjoner om å støtte mer enn ett "tailnet" (virtuelt flatt, sammenkoblet nettverk) per kontrollserver. Det gjør Headscale mindre kurant for prosjektet, ettersom et av målene mine er å kunne tilby én sentral server som kunder kan koble seg til; som tilbyr sikker kommunikasjon mellom deres enheter og ingen mulighet for tilgangs- eller informasjonslekkasje.


## Netmaker
Netmaker ser ut til å ha best ytelse av mesh-nettverks-løsningene, men har per nå ikke en mobilapp for enkel tilkobling til mesh-nettverket.


## Nebula
Jeg begynner å sette opp et Nebula nettverk, med et "fyrtårn" i Google Cloud. Alle produserte nøkler og sertifikater krypteres i hvile, med nøklene lagret i min Bitwarden passordbehandler.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
