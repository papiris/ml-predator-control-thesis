
# Arbeidsøkt timelogg

### Dato: 25.01.2024
### Tidsrom: 12:00 - 15:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; mål, organisering, kravspesifikasjon, kvalitetsstyring

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
