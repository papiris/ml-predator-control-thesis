# Arbeidsøkt timelogg

### Dato: 25.04.2024
### Tidsrom: 09:00 - 11:00, 12:30 - 18:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Prosjektstyring
## Statusrapport 2
Skriver mer på Statusrapport 2. Fyller inn kapitler og seksjoner med tekst, ordner vedlegg.


## Statuspresentasjon 2
Ferdigstiller lysbildepresentasjon med mange bilder og illustrasjoner, en video, og teknikker for å gjøre det enklere for publikum å fokusere.





<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
