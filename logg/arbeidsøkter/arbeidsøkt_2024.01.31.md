
# Arbeidsøkt timelogg

### Dato: 31.01.2024
### Tidsrom: 14:20 - 16:00, 16:20 - 18:00
### Sted: Steigen Folkebibliotek, Steigen

-----


# Emne 1: Litteraturstudie, eksisterende løsninger
Lese om eksisterende løsninger innenfor samme område.


## FOSOPAS - Jacob Ludvigsen
Intro: En meget begrenset og simpel programvare, utviklet av meg siden april 2023.
- Lisensiert under AGPL-v3
- Skrevet i Python
- Laget av undertegnede
- Ingen innebygget webserver, kun lokalt på maskinen
- Ingen eksterne tjenester
- Kun varsling lokalt på maskinen
- Innebygget objektgjenkjenning med maskinlæring
- Ingen støtte for konteinerisering
- Ingen utviklersamfunn rundt
- Begrenset innebygget auto-oppdaging og konfigurering av ONVIF-støttede kameraer
- Ingen støtte for avlasting av objektgjenkjenning


# Emne 2: Litteraturstudie; Objektgjenkjennings-modeller
Lese om ulike maskinlæring modeller for objektgjenkjenning.  
Til evaluering av presisjon brukes mAP: https://towardsdatascience.com/map-mean-average-precision-might-confuse-you-5956f1bfa9e2  
Hastighet er målt på T4 GPU med filformatet TensorRT FP16

### Støttelitteratur
- https://neptune.ai/blog/object-detection-algorithms-and-libraries
- https://deci.ai/blog/yolov8-vs-yolo-nas-showdown-exploring-advanced-object-detection/
- https://www.augmentedstartups.com/blog/yolo-nas-vs-yolov8-a-comprehensive-comparison
- https://medium.com/@pankajsharma01/analyzing-the-performance-yolo-nas-and-yolo-v8-side-by-side-11406800906a

## YOLOv8
Intro: En modell jeg har erfaring med. God treffsikkerhet, kjapp.
- Utgitt i april 2022
- Lisensiert AGPL-v3.
- Mange tilgjengelige integrasjoner, som SAHI for "små glidende undersøkelsesvinduer"
- Aktivt utviklet, med 240 bidragsytere
- Har et selskap i ryggen, som tjener penger på tilleggstjenester og "enterprise" lisensiering.  
Altså at man kan betale dem for å omgå kravene til å publisere kildekoden om man gjør endringer på modellen / trener den.
- Opptil 53.9 mean Average Precision (MaP_val)
- Repo: https://github.com/ultralytics/ultralytics
- Skrvet i Python og Pytorch, men modellen kan omdannes til andre rammeverk som Tensorflow Lite, ONNX og OpenVino.
- Kan kvantiseres til F16 eller INT8, ned fra F32 i trening.
- FPS for YOLOv8-x: 50

## YOLOX
Intro: En litt eldre modell, men fortsatt bra.
- Utgitt i 2021
- Lisensiert Apache-2.0
- Ikke veldig aktivt utviklet lengre
- Opptil 51.1 MaP_val
- Repo: https://github.com/Megvii-BaseDetection/YOLOX


## YOLO-NAS
Intro: Nyere enn YOLOv8, en mix mellom yolov8 og yolov6.
- Støtter INT 8-bit kvantisering (ned fra FLOAT 32-bit i trening), med lite presisjonstap.  
Dette øker effektiviteten og hastigheten, særlig på plattformer med få ressurser.
- Ikke like presis som YOLOv8, men betydelig mindre latens.
- Opptil 52.1 mAP ved 8-bit kvantisering 
- Repo: https://github.com/Deci-AI/super-gradients
- Ultralytics tilbyr modellen integrert i deres verktøy: https://docs.ultralytics.com/models/yolo-nas

## RT-DETR
Intro: Veldig god, veldig ny. Den første sanntids ende-til-ende objektgjenkjenningsmodellen.  
Bedre mAP og FPS enn alle YOLO-modeller. State of The Art
- Utgitt i juli 2023
- Opptil 54.8 mAP
- Lisensiert Apache-2.0
- Ultralytics har integrert i deres verktøy https://docs.ultralytics.com/models/rtdetr/
- Repo: https://github.com/lyuwenyu/RT-DETR
- Papir: https://arxiv.org/abs/2304.08069
- FPS for RT-DETR-X: 74


#  Emne 3: Litteraturstudie; maskinvare
Det finnes ulike typer maskinvare egnet for å kjøre maskinlæringsmodeller.  
Vi har enkeltkorts-datamaskiner som [ASUS Tinkerboard Edge R](https://tinker-board.asus.com/series/tinker-edge-r.html) som har en ordinær prosessor og en nevral ko-prosessor,  
eksternt tilkoblede ASIC kretskort som kun har en nevral prosessor, som [Coral AI sin USB akselerator](https://coral.ai/products/accelerator);  
og PCI-e pluggbare moduler med kun nevrale prosessorer [som Coral AI sine](https://coral.ai/products/m2-accelerator-dual-edgetpu).

De ulike maskinvarene og produsentene har sine egne optimaliseringer, og ofte egne verktøy og rammeverk for å bruke dem. For å abstrahere dette bort kan man bruke Microsoft sin [Olive programvare](https://github.com/microsoft/Olive)  
Dokumentasjon for Olive: https://microsoft.github.io/Olive/overview/olive.html

Coral.ai sine PCIe akseleratorer er tilgjengelig i formfaktoren m.2 A+E og m.2 B+M. Den doble PCIe akseleratoren er bare tilgjengelig i m.2 A+E.  
B+M brukes typisk til SSD-er, A+E brukes typisk til trådløse nettverkskort.  
Adapter for å montere m.2 A+E dobbel akselerator i m.2 B+M slot: https://www.makerfabs.com/dual-edge-tpu-adapter-m2-2280-b-m-key.html


# Emne 4: Rammeverk
Oversikt over ulike rammeverk og filformater for maskinlæring. Alt er Fri programvare med mindre noe annet er skrevet.  

Rammeverk: ONNX, openvino, Tensorflow, Pytorch, CUDA, ROCM  
Filformat: Safetensors

Pytorch og Tensorflow er mest populære, og har støtte for flest funksjoner og maskinvare.

OpenVino er et rammeverk for optimalisering av diverse maskinlæringsmodeller, spesifikt for Intel maskinvare.  
CUDA er et rammeverk for kjøring av diverse maskinlæringsmodeller (dersom de implementerer CUDA støtte), spesifikt for Nvidia maskinvare. Proprietær. Generelt populær.  
ROCM er et rammeverk for kjøring av diverse maskinlæringsmodeller (dersom de implementerer CUDA støtte), spesifikt for AMD maskinvare. Lav popularitet.  
ONNX er et abstraksjonsledd mellom ulike rammeverk, og mellom rammeverk og maskinvare. Det legger opp til at de ulike rammeverkene sikter seg inn på maks støtte for ONNX, så sikter ONNX seg på maks støtte for maskinvare. Populært.

Safetensors er et filformat som sikter på tryggere lagring og deling av maskinlæringsmodeller. Enkelte andre formater er trege å laste inn, utsatt for korrupsjon, og utrygge med tanke på kjøring av innpakket, gjemt kode. Særlig utsatt er Pytorch, hvor ønsket om å unngå bruken av `Pickle` programvarebiblioteket [var hovedårsaken til at Safetensors ble utviklet.](https://github.com/huggingface/safetensors?tab=readme-ov-file#yet-another-format-) 

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
