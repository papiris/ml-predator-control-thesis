# Arbeidsøkt timelogg

### Dato: 13.04.2024
### Tidsrom: 21:00 - 24:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Risikovurdering
Gjør meg opp noen tanker rundt hva som kan utgjøre en etisk og samfunnsmessig risiko ved prosjektet.  
-  Det er effektivt for å overvåke mennesker, og umiddelbart varsle og agere via eksterne systemer når visse betingelser er oppfylt.  

Et kjerne-element i prosjektet er at det skal respektere hele brukerkjedens frihet og personvern, og minimere trusselen utgjort av proprietære alternativer. I dette ligger det derfor som premiss at programmet må være fritt, og at enhver som gjør programmet tilgjengelig for en bruker, også må gi brukeren de samme frihetene.  
Det er dessverre ingen provisjoner i [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html) for at "Personer som får sin data/informasjon behandlet av programmet" også skal få slike rettigheter. Det som derimot står, er at man ikke kan ilegge noen ytterligere restriksjoner eller betingelser, enn hva som fremkommer i lisensen.

Det er vanskelig å finne en lisens som møter alle kriteriene, derfor skriver jeg [en tråd på Mastodon](https://hachyderm.io/@papiris/112265777619006095) om situasjonen og ber om råd fra et knippe organisasjoner som arbeider med Fri Programvare-lisenser. Vi får se hva de svarer.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
