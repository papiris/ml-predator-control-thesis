# Arbeidsøkt timelogg

### Dato: 25.03.2024
### Tidsrom: 14:00 - 17:00, 19:30 - 20:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Prosjektstyring
Skriver maler for avviksmeldinger og endringsmeldinger for alt som ikke har gått som planlagt hittil i prosjektet. For å lage dokumentene, benyttes LaTex-redigeringsprogrammet [Kile](https://kile.sourceforge.io/). Dette vil gi meg øvelse i LaTex, som kommer godt med når jeg skal skrive sluttrappporten på samme vis.  
Leter frem .bibtex referanser for mange av tingene som har vært viktige for prosjektet imens jeg venter på at LaTex (6GB) skal installeres.

# Emne 2: Rapportskriving
Begynner så smått å lage forside, referanseliste og disposjon for sluttrapporten i LaTex.
Funker greit, men er litt knotete siden jeg ikke vet de nødvendige kommandoene ennå.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
