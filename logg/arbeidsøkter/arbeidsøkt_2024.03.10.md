
# Arbeidsøkt timelogg

### Dato: 10.03.2024
### Tidsrom: 19:30 - 21:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Prosjektstyring
## Arbeidslogger
Lager arbeidslogger over fredag, lørdag og dagen i dag; fordi jeg glemte det.

# Emne 2: Konseptutvikling
## FOSOPAS
Setter opp GPG-signering av commits, for å øke sikkerhetsnivået på prosjektet.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
