
# Arbeidsøkt timelogg

### Dato: 25.02.2024
### Tidsrom: 23:00 - 01:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Går gjennom den interaktive [quickstart-guiden](https://openziti.io/docs/learn/quickstarts/network/local-no-docker/) for lokal bruk uten konteinerisering.

## Blue-candle
Ønsket om støtte for RT-DETR modellen lukkes av prosjektvedlikeholderen, fordi prosjektet avhenger av støtte i oppstrømsbiblioteket [candle](https://github.com/huggingface/candle).

## Moonfire-NVR
Jeg prøver å kjøre programmet konteinerisert, men møter mange problemer.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
