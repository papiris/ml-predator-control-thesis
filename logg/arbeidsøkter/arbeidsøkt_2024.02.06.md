
# Arbeidsøkt timelogg

### Dato: 06.02.2024
### Tidsrom: 14:00 - 16:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring; Melde ifra om etterslep i tidsplan
Det har ikke vært lett for meg å sette av tid til å arbeide med bacheloroppgaven den siste uka.  
Det har vært møter i diverse lag og foreninger, fullt hus med unger og hunder, sykdom og bursdag i familien, jeg har startet enkeltpersonforetak og gjort mitt første oppdrag.  
Per nå henger jeg 23 timer etter tidsplanen; og likeledes med fremdrifta.  
For å unngå videre forsinkelser, har jeg kontaktet veilederen i prosjektet Halldor Arnason og forklart situasjonen. Jeg skal forsøke å med noen om å bli en "ansvarlighetskamerat" som kan studere sammen med meg, for å gi en følelse av ansvarlighet og kollegium.


# Emne 2: litteraturstudie
## Skrive oppsummering av litteratur
Sammenfatter det jeg har lært gjennom litteraturstudiet; i ett dokument.


## Bruke LaTex istedenfor Libreoffice?
Jeg ønsker at prosjektet skal være reproduserbart, etterprøvbart, og at den publiserte dokumentasjonen skal være kontinuerlig oppdatert. Dette kan være utfordrende ved bruk av formater som Libreoffice's Open Document Format (.odf).
Det er ganske populært å bruke LaTex for å skrive artikler, avhandlinger og oppgaver i akademia, og fremfor alt er det programmerbart.
Det lar meg lage en prosess for kontinuerlig utvikling og integrering av dokumentasjonen (CI/CD), som så kan publiseres automatisk på prosjektsiden på Gitlab og refereres til.
Jeg har aldri brukt LaTex, men er åpen for å prøve.
Relavant lesning:
- https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/
- https://www.vipinajayakumar.com/continuous-integration-of-latex-projects-with-gitlab-pages.html
- https://egraff.github.io/uit-thesis/manual/frontpage.html
- https://github.com/egraff/uit-thesis

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
