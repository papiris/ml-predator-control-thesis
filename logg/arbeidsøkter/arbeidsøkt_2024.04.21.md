# Arbeidsøkt timelogg

### Dato: 21.04.2024
### Tidsrom: 19:00 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
- Oppdaterer fremdriftsplan / timelogg med arbeid siden 04.04.2024   
- Skriver avvik på at det ble gjort lite arbeide i perioden 11.04 - 21.04.2024; grunnet helseutfordringer.  
- Skriver på Statusrapport 2


# Emne 2: Utvikling
- Hjelper utviklerne av OpenZiti med å kjøre eksempel-webappen min.  
- Videreutvikler eksempel-webappen for openziti, med større kompatibilitet og bedre feilsøkings-print samt dokumentasjon.  

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
