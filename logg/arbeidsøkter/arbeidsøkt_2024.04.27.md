# Arbeidsøkt timelogg

### Dato: 27.04.2024
### Tidsrom: 13:30 - 21:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Stig Strandmoa demonstrerer at overvåkningskameraene på gården har toveis audio, altså at det kan komme lydsignal av et akseptabelt volum ut av høyttaler på kameraene. Bruker derfor dagen på å undersøke og teste ulike metoder for å benytte seg av den funksjonen.  
Kameraene Foscam-kloner.  
Prøver ONVIF, men det går ikke. Prøver cgi, altså kameraets web-baserte API, og det funker ikke. Prøver RTSP bak-kanal, men det funker heller ikke. Det er altså likevel nødvendig å skaffe en ekstern dyreskremmer, siden kameraet ikke kan ha den rollen.

Sier meg ferdig med utviklingsfasen.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
