
# Arbeidsøkt timelogg

### Dato: 13.02.2024
### Tidsrom: 10:00 - 11:00, 14:30 - 17:30
### Sted: Tjeldberget, Bodø; og Makerspace Bodø

---

# Emne 1: litteraturstudie

## Sammenstille litteraturstudie
Utbrodere kapitler 1: Eksisterende løsninger, 6: Programvarebiblioteker, 7: Maskinlæringsmodeller, og 8: Treningmateriale for maskinlæring.  
Skrive om ulike nettverksløsninger for kryptert vert-til-vert kommunikasjon, programvarebiblioteker som vanligvis brukes til maskinlæring, maskinlæringsmodeller, og hvor treningsmaterialet for maskinlæring skal komme fra i dette prosjektet.

Skrev referanser.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
