# Arbeidsøkt timelogg

### Dato: 26.05.2024
### Tidsrom: 11:00 - 18:00, 22:30 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Innarbeider Halldors forslag i rapporten.

Forbedrer grafikker og overganger mellom temaer.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
