# Arbeidsøkt timelogg

### Dato: 19.03.2024
### Tidsrom: 13:15 -  16:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling 
## Web push for Predalert
Fortsetter arbeidet med å implementere Web Push i programmet Predalert. Dette gir muligheten for å motta push-varsler på mobiltelefon (eller andre enheter med tilgang til nettleser), selv om nettsiden og nettleseren er lukket.  
Web Push avhenger av såkalte "Service Workers", som arbeider i en tråd på klientenheten, separat fra nettsiden som startet dem. For å minimere sikkerhetsrisiko, kreves det at nettsider bruker kryptert tilkobling (SSL/TLS) for å lagre Service Workers på klientenheter og la dem kjøre i bakgrunnen.

Jeg sliter med å komme igang igjen etter noen dager uten studier. Var på fylkesårsmøte i Rødt i helga, og farfaren min sin begravelse i Senja. Det tar naturlig nok litt tid å få hodet tilbake til studiene.

Vegrer meg litt for å starte opp igjen med Web Push arbeidet. Dette grunner i at jeg ikke har arbeidet med Javascript før, og ikke klarer helt godt å lese og forstå det. Jeg har ikke forutsetningene for å utvikle Web Push-implementasjon for Predalert på egenhånd. Jeg har heller ikke forutsetningene for å vurdere sikkerheten og egnetheten av de ulike eksempel-implementasjonene som finnes på internett.

Gode kilder om Service Workers:  
[Felix Gerchau blog 1](https://felixgerschau.com/service-workers-explained-introduction-javascript-api/)  
[Mozilla Developer Network 1](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers)

Gode kilder om Web Push:  
[Felix Gerchau blog 2](https://felixgerschau.com/web-push-notifications-tutorial/)   
[Mozilla Developer Network 2](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Tutorials/js13kGames/Re-engageable_Notifications_Push)  
[GauntFace's komplette oversikt](https://web-push-book.gauntface.com)  
[Surya Sankars gjennomgang](https://suryasankar.medium.com/how-to-setup-basic-web-push-notification-functionality-using-a-flask-backend-1251a5413bbe)  
[Google's Codelab på temaet](https://codelabs.developers.google.com/codelabs/push-notifications)  
[Nitin Raturi's programvareutviklings blog](https://tech.raturi.in/webpush-notification-using-python-and-flask)
 
<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
