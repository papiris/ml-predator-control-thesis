# Arbeidsøkt timelogg

### Dato: 10.04.2024
### Tidsrom: 21:00 - 24:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Omarbeider Predalert for å kunne strømme bilder over ziti-nettverket; og lykkes delvis. Kan strømme ett enkelt bilde fra hver kilde per omlasting, men bildene blir ikke erstattet.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
