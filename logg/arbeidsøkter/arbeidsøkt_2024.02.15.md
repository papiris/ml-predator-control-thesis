
# Arbeidsøkt timelogg

### Dato: 15.02.2024
### Tidsrom: 10:30 - 11:00, 12:30 - 16:00, 17:30 - 22:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Kundemøte
Holder innledende møte med Stig Strandmoa for å avklare ønsker og behov, som grunnlagsmateriale for konstruksjonsgrunnlaget / kravspesifikasjonen.

## Komponenter
Går bort fra metoden med å flette komponenter sammen til konsepter og så vurdere dem med vektingsmatrise, siden alle komponentene i hver kategori (nettverk, kameraprogramvare, maskinlæringsmodell) stort sett er internt utbyttbare. Vurderer heller komponentene i hver kategori for seg selv, og syr sammen et konsept av det.

# Emne 2: Prototyping
Aktiverer en 90-dagers prøveperiode med Google Cloud Platform sitt Kubernetes-tilbud, for å ha en klynge med offentlig IP-adresse. Nødvendig for å kunne teste ulike nettverksløsninger

Lager en virtuell maskin på GCP, og installerer cloudflareddns i docker på den for å sikre at vertsnavnet alltid peker på riktig IP-adresse.

Setter opp en klynge med k3s, og installerer headscale på den. Når ikke frem til IP-adressen, så jeg installerer en enkel webtjener på serveren for feilsøking. Når fortsatt ikke frem.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
