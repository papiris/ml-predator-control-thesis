# Arbeidsøkt timelogg

### Dato: 11.05.2024
### Tidsrom: 10:00 - 17:00, 19:00 - 02:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
Klarer å få labelstudio til å tjenes fra domenet labelstudio.lan, ved å legge til følgende linje i `/etc/hosts` på datamaskinen min:  
`127.0.0.1 labelstudio.lan`  
og gjøre tilsvarende endring i labelstudio sin values.yaml for helm/kubernetes.


Utvikler backenden til å fungere!  
Den kan nå gi forslag til annoteringer av bilder, og er godt til hjelp.  
Trener modellen med de annoterte bildene, og forbedrer den.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
