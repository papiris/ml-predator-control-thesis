# Arbeidsøkt timelogg

### Dato: 09.05.2024
### Tidsrom: 13:15 - 17:00, 20:30 - 23:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
Installerer label-studio på datamaskinen, for å kunne annotere alle datasett på en lokal måte.

Installasjon på kubernetes via helm.  
Lager persistentVolume og persistentVolumeClaim for datasett og applikasjonslagring.  
Lager ingress med Traefik.  
<!--Utvikler en RT-DETR backend for label-studio, etter inspirasjon fra tre andre backender med ultralytics/yolov8. Backenden gir forhåndsutfylte annoteringer, slik at det tar kortere tid på store datasett.-->

Møter på trøbbel med at "/data/local-files/" sies å ikke eksistere. Noen timer feilsøking uten resultat.  
Prøver derfor label-studio installert via pip istedenfor helm. Får da problemer med at label-studio krever en spesifikk versjon av psycopg, som må bygges. Bygging av psycopg har litt tungvinte avhengigheter, så det tar en time.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
