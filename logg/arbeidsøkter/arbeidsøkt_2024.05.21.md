# Arbeidsøkt timelogg

### Dato: 21.05.2024
### Tidsrom: 12:00 - 13:00,  15:00 - 21:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapitlene Konklusjon, System, Sammendrag og Diskusjon, med fokus på seksjonene konseptutvikling, trening av maskinløringsmodell, videre arbeid.

Fikser tabell over beslutningspunkter.

Ordner vedlegg relatert til maskinlæring.

Ordner referanser. Forfatter, dato og slikt.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
