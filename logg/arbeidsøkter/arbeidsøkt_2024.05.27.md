# Arbeidsøkt timelogg

### Dato: 27.05.2024
### Tidsrom: 13:00 - 23:55
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Innarbeider Halldors forslag i rapporten.

Forbedrer siteringer, henviser til prosjektets online dokumentasjonsarkiv.

Oppdaterer bilder.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
