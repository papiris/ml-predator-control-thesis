# Arbeidsøkt timelogg

### Dato: 07.05.2024
### Tidsrom: 09:00 - 11:00, 17:00 - 19:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Introduksjon, Litteraturstudie og Programvare.

Søker opp og setter inn referanser og vedlegg.


# Emne 2: Utvikling/Trening av maskinlæringsmodell
Skriptet lagret 10550 gaupebilder, de fleste av kurant kvalitet for trening.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
