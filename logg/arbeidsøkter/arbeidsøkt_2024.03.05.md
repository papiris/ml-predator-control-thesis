
# Arbeidsøkt timelogg

### Dato: 05.03.2024
### Tidsrom: 10:30 - 12:00, 15:00 - 16:00, 19:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Konstruksjonsgrunnlag

## FOSOPAS
Etter råd fra veileder Halldor Arnason i veiledningsmøte 04.03.2024, ser jeg nærmere på hvordan jeg kan lage et minimumsprodukt basert på FOSOPAS.

## Zoneminder
Prøver å bruke Zoneminders distribusjonspakker, via [distrobox](https://distrobox.it/). [Zoneminder-pakkene i Fedora](https://zoneminder.readthedocs.io/en/latest/installationguide/redhat.html) kan bruke enten nginx eller apache nettserver, i motsetning til pakkene i Debian og deres nedstrøms-distribusjoner som kun bruker apache. Apache krever rot-tilgang, som ikke er enkelt forenlig med bruk i Distrobox. Derfor installerer jeg en fedora-40 konteiner med distrobox, og inkluderer init-systemet `systemd` og støtte for bruk av vert-operativsystemets grafikkort.  
Hele kommandolinjen jeg brukte følger her:
```
distrobox create -i fedora:40 --nvidia --init  --additional-packages "systemd nano" -n fedora-zm
distrobox enter fedora-zm
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y zoneminder-nginx
sudo dnf install -y mariadb-server
sudo systemctl enable --now mariadb
sudo mysql_secure_installation   # her svarer jeg "ja" på bruk av unix-socket authentication, og klikker gjennom resten.
sudo mysql -u root -p < /usr/share/zoneminder/db/zm_create.sql
sudo mysql -u root -p -e "CREATE USER 'zmuser'@'localhost' \
                    IDENTIFIED BY 'zmpass';"
sudo mysql -u root -p -e "GRANT ALL PRIVILEGES ON zm.* TO \
                    'zmuser'@localhost;"
sudo mysqladmin -uroot -p reload
sudo ln -sf /etc/zm/www/zoneminder.nginx.conf /etc/nginx/conf.d/
sudo ln -sf /etc/zm/www/redirect.nginx.conf /etc/nginx/default.d/
sudo nano /etc/sysconfig/fcgiwrap   # her setter jeg DAEMON_PROCS lik antall kameraer denne zoneminder-installasjonen skal håndtere.
```

Siden vi kjører konteinerisert, får ikke prosesser i konteineren binde seg til porter under 1024 i standardoppsettet. Derfor flytter vi portene til 8080, som det er tillatt å bruke fra konteinere.  
Filen `/etc/nginx/conf.d/zoneminder.nginx.conf` skal se slik ut i toppen:
```
server {
    listen	 8080 default_server;
    listen	 [::]:8080 default_server;
    server_name = localhost $hostname;

#    ssl_certificate "/etc/pki/tls/certs/localhost.crt";
#    ssl_certificate_key "/etc/pki/tls/private/localhost.key";
#    ssl_session_cache shared:SSL:1m;
#    ssl_session_timeout  10m;
#    ssl_ciphers PROFILE=SYSTEM;
#    ssl_prefer_server_ciphers on;
```

Filen `/etc/nginx/nginx.conf` skal se slik ut på ca. linjene 37-41:
```
	server {
		listen       8080;
        listen       [::]:8080;
        server_name  _;
        root         /usr/share/nginx/html;
```

Vi kan nå ferdigstille installasjonen:
```
sudo systemctl enable --now nginx
sudo systemctl enable --now zoneminder
```

Zoneminders brukergrensesnitt er nå tilgjengelig i nettleseren på http://localhost:8080

Dette funker den første starten, men senere starter feiler pga MariaDB.

Førsøk med å bruke ordinære docker-conteinere (via podman, podman-compose, og podman kube create --> podman kube play / sudo k3s kubectl apply) feiler alle pga fcgiwrap ikke har tillatelse til å binde seg til en bestemt unix socket.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
