
# Arbeidsøkt timelogg

### Dato: 29.02.2024
### Tidsrom: 12:00 - 17:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Hører på opptaket fra møtet med Philip Griffith, og lager en transkripsjon ved hjelp av [Speechnote](https://flathub.org/apps/net.mkiol.SpeechNote) og [Whisper-v3](https://huggingface.co/openai/whisper-large-v3).  
Lager en bruker på [Netfoundry's CloudZiti SaaS tilbud](https://netfoundry.io/), som bygger på OpenZiti. Det vil ta bort mye av hodebryet under testing av ulike løsninger for integrering av OpenZiti i prosjektet mitt. Poliser og andre innstillinger som funker i CloudZiti, kan direkte overføres til en instans av OpenZiti jeg oppfører selv.

## Zoneminder
Undersøker muligheten for å bruke OpenZiti sin SDK direkte i Zoneminder, for enda sikrere kommunikasjon. For å kunne gjøre dét, tenker jeg [Alpine Linux](https://alpinelinux.org/) er en fin og sikker base for et [konteinerisert byggemiljø](https://github.com/ZoneMinder/zmdockerfiles/tree/master/development). Dessverre viser det seg at Alpine's Musl-libc ikke tillater mange av funksjonene fra GNU-libc som Zoneminder gjør bruk av. Det er én av grunnene til at Alpine Linux er sikrere enn mange andre Linux-distribusjoner, men gjør at jeg ikke kan bruke den for å bygge zoneminder.

Foreløpig er nok det enkleste å bruke en OpenZiti tunneler eksternt fra applikasjonen for å få få applikasjonens kommunikasjon over på ziti-nettverket.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
