# Arbeidsøkt timelogg

### Dato: 21.03.2024
### Tidsrom: 09:00 -  17:00, 18:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling 
## Web push for Predalert
Fortsetter arbeidet med å implementere Web Push i programmet Predalert.  
løypa er:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.

I dag har jeg løst punkt 2.  
Brukte hovedsaklig [Akilesh Raos videoer](https://invidious.einfachzocken.eu/watch?v=oDIYl3G613E) som veiledning. Dette fordi han bruker async javascript, og videoene ble publisert for kun 6 måneder siden. De er dermed 3,5 - 5 år nyere enn de andre veiledningene jeg har sett på.

commit-melding for igår og idag:
* Implement frontend javascript to ask for notification permission,  
  register a serviceWorker and a pushManager,  
  fetch vapid public key from the backend,  
  subscribe to push notifications from the backend,  
  and display recieved notifications.  
* Implement backend python to send vapid public key to frontend,  
  recieve pushSubscription from frontend and send test notification
  to frontend.  
* Implement starlette lifespan to connect and disconnect from database  
  before starlette app is started and after it's stopped.  
* Implement starlette middleware to accept a wider scope of CORS,  
  which will be necessary once the app is deployed.  
* Improve docstrings everywhere

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
