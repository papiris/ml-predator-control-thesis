# Arbeidsøkt timelogg

### Dato: 05.05.2024
### Tidsrom: 12:00 - 18:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Arbeider med kapitlene Introduksjon og Litteraturstudie.

Søker opp og setter inn referanser og vedlegg.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
