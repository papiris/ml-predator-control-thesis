
# Arbeidsøkt timelogg

### Dato: 27.01.2024
### Tidsrom: 12:00 - 17:00
### Sted: Steigen, Strandmoa Gård

-----

# Emne 1: Skrive prosjektbeskrivelse
Fortsetter å skrive prosjektbeskrivelse; Pusser på mål, organisering, kravspesifikasjon, kvalitetsstyring, fremdriftsplan

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
