# Arbeidsøkt timelogg

### Dato: 17.05.2024
### Tidsrom: 17:00 - 20:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving

Lager system- og konteinerdiagrammer med Python-pakken `diagrams`.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
