# Arbeidsøkt timelogg

### Dato: 26.03.2024
### Tidsrom: 10:00 - 15:00, 17:00 - 18:00, 19:00 - 21:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Prosjektstyring
Fortsetter å lage maler for endrings- & avviksmeldinger i LaTex, og skrive meldingene som skulle vært skrevet hittil.  
Oppdaterer fremdriftsplanen, som ikke har blitt oppdatert siden 12.03.2024.


# Emne 2: Rapportskriving (1 time)
Lager disposjon til sluttrapport og begynner så smått å fylle inn.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
