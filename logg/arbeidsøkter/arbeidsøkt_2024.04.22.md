# Arbeidsøkt timelogg

### Dato: 22.04.2024
### Tidsrom: 08:00 - 16:00, 19:00 - 20:30
### Sted: Håsand Gård, Steigen

---


# Emne 1: Prosjektstyring
- Skriver mer på Statusrapport 2


# Emne 2: Utvikling
- Får openziti til å fungere med den utbroderte eksempel-webappen min; og deretter i Predalert!  
- Hjelper utviklerne av OpenZiti med å kjøre eksempel-webappen min.  
- Videreutvikler eksempel-webappen for openziti, med større kompatibilitet og bedre feilsøkings-print samt dokumentasjon.  

På veien videre trengs i programmet:  
1. Trening av maskinlæringsmodell  
2. SSL over openziti vha SPIFFE/SPIRE  
3. MQTT  
4. Mulighet til å avslutte abonnement på push-varsler  
5. Sensurering av personer i objektgjenkjenninga  
6. Mulighet til å velge hvilke 'klassenavn' som skal utløse varsel ved deteksjon  
7. Lagring av bildeserier med positiv deteksjon  
8. UI for å se og slette lagrede bildeserier  



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
