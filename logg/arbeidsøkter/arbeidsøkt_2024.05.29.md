# Arbeidsøkt timelogg

### Dato: 29.05.2024
### Tidsrom: 10:00 - 16:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Presentasjon
Lager disposisjon til presentasjon, og pusher til git.
Drar til Narvik i morgen med bussen, så må bruke en pc som funker på turen.

## Disposisjon presentasjon
### Problemstilling
- Bakgrunn / problemstillinger

- Mål

### Metode

- Fremdriftsplan + S-kurve

- Timebudsjett og -regnskap + arbeidsintensitetsgraf

- Prosjektstyring (rammeverk, tiltak, hendelser)

- Kvalitetstyring (rammeverk, tiltak, hendelser)

### Resultat

- Resultat

- Diskusjon (om metode og resultat, videre arbeid)

- Konklusjon



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
