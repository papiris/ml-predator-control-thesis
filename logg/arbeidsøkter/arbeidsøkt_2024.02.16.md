
# Arbeidsøkt timelogg

### Dato: 16.02.2024
### Tidsrom:  13:30 - 16:30, 18:00 - 22:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Konstruksjonsgrunnlag
## Undersøker ergonomien av å bruke k3s, headscale og zoneminder
Setter opp en klynge med k3s, og forsøker å installere Zoneminder på den. Det lykkes, men tilkoblinger nektes. Mulig mismatch mellom helm-charten sine forventninger til ingress, og k3s' standard-konfigurasjon med traefik.


Fant ut hvorfor jeg ikke kunne nå noen ting på Debian VM'en i Google Cloud, ved å lage en ny Fedora VM der, med kun pakke-installert nginx som serverer en statisk nettside på port 80.

Feilen er at k3s i standard-konfigurasjon kjører reverse-proxyen Traefik som binder seg til bl.a. port 80. Alle andre tjenester som forsøker å bruke den porten, blir da nektet.
Løsningen er nok å bruke reverse proxyen, vil jeg tro. Eller å kjøre ukonteinerisert på serveren.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
