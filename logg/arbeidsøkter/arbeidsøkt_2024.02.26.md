
# Arbeidsøkt timelogg

### Dato: 26.02.2024
### Tidsrom: 10:00 - 12:00, 13:00 - 17:15, 18:00 - 20:00, 00:00 - 01:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Publiserer en kommentar i [tråden hos OpenZiti forumet](https://openziti.discourse.group/t/using-openziti-in-distributed-surveillance-system/2135/3), der jeg oppklarer noen ting og nevner at det ville være nyttig med en SDK for Openziti i Rust.

Prøver å få kontrolleren, konsollen og en ruter opp og gå på k3s-klynga i VM'en hos GCP, ved å følge [disse guidene](https://openziti.io/docs/category/hosting-openziti)

Etter at det ikke funket så bra, prøver jeg å gå steg for steg gjennom den manuelle utgaven av [hurtigoppsettet for OpenZiti i kubernetes](https://openziti.io/docs/learn/quickstarts/network/local-kubernetes). For at det skal funke på et mer permanent vis, må jeg oversette kommando for kommando til de forholdene jeg ønsker å oppnå; som også viser seg å bli problematisk. Det er vanskelig å bygge bro mellom en liten og automatisk test-oppføring av OpenZiti, og en bruksklar oppføring.

## Cert-Manager
For at brukere skal kunne bruke tjenesten uten å bli varslet om at tjenesten bruker "selvsignerte utrygge sertifikater", må jeg få utstedt TLS-sertifikater. Det gjennomføres ved hjelp av [Cert-Manager](https://cert-manager.io/). Jeg følger [denne guiden](https://cert-manager.io/docs/installation/helm/) for å installere Cert-Manager i k3s-klyngen, og [denne guiden](https://cert-manager.io/docs/configuration/acme/dns01/cloudflare/) for å sette opp en ACME sertifikathenter for Cloudflare.  
Det viser seg at YAML-formatet er svært følsomt for innrykk, og jeg strever en god halvtime med å få korrekt innrykk på Issuer.yaml. Dette er den endelige fila:

```
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-dns01-issuer
spec:
  acme:
    email: jacob.ludvigsen@protonmail.com
    server: https://acme-v02.api.letsencrypt.org./directory
    privateKeySecretRef:
      name: cluster-issuer-account-key
    solvers:
    - dns01:
        cloudflare:
          email: jacob.ludvigsen@protonmail.com
          apiTokenSecretRef:
            name: cloudflare-api-token-secret
            key: api-token
      selector:
        dnsZones:
          - 'ingeniorskap.no'
```

Cloudflare er den aller største internett-aktøren, særlig innen DNS, CDN og sertifikat-signering. All bruk av Cloudflare medfører derfor visse personvernavveininger og bekymringer. Ved en senere anledning vil jeg gå over til å bruke [desec.io](https://desec.io/) for DNS og TLS-signering, via en [Cert-Manager Webhook](https://github.com/su541/cert-manager-desec-webhook?tab=readme-ov-file). deSEC er en ideell organisasjon i Tyskland, med formål å yte DNS-tjenester på en personvernrespekterende måte.

Ved endringer i Cert-managers issuer sin konfigurasjonsfil: Issuer.yaml, må endringene gjøres gjeldende slik: `sudo helm --kubeconfig /etc/rancher/k3s/k3s.yaml -n cert-manager upgrade cert-manager jetstack/cert-manager  -f cert-manager/Issuer.yaml`  
Det er viktig å spesifisere `--namespace` / `-n`når man ikke kjører ting i standard-navnerommet til kubernetes.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
