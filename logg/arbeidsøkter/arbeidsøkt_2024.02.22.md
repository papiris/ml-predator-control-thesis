
# Arbeidsøkt timelogg

### Dato: 22.02.2024
### Tidsrom: 10:00 - 16:30, 18:00 - 19:30
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Konstruksjonsgrunnlag

## Nebula
### Installasjon på Windows 10
Må kjøres med administrator-privilegier, kan kjøres uten installasjon.
nebula install service?
Banene for de ulike delene av PKI i konfigurasjonsfila trenger ikke være i gåseøyne.

En tjeneste for å kjøre nebula-agenten kan installeres. Dette åpner for at systemet kan håndtere logger ved å følge [guiden her](https://nebula.defined.net/docs/guides/viewing-nebula-logs/#windows)

<br>


### Automatisk installasjon og konfigurasjon
Etter å ha installert og satt opp to-tre nebula verter/agenter manuelt, ser jeg at det er en omstendelig og manuell prosess som ikke lar seg enkelt skalere. Begynner derfor letingen etter et verktøy som kan foreta automatisk installasjon og oppsett av nebula-klienter; gjerne gjennom et brukergrensesnitt som er tilgjengelig for brukeren selv.

Finner da [CEGO A/S sin Nebula-provisioner](https://github.com/cego/nebula-provisioner), som ihvertall overfladisk ser ut til å passe formålet. Prosjektet har svært begrenset dokumentasjon, og har ikke definert en programvarelisens. [Jeg åpnet et issue med ønske om lisens-avklaring og bedre dokumentasjon.](https://github.com/cego/nebula-provisioner/issues/571)

<br>

### Trygg bruk

Det er ikke nødvendig at systemet som kontrollerer Nebula-nettverkets CA og utsteder sertifikater til verter (både agenter og fyrtårn), må være del av Nebula nettverket. Det er fullt mulig å bruke et separat system, som utsteder sertifikater til verter på forespørsel; og får sertifikatet overført til den respektive verten. Et slikt system trenger ikke ordinært være tilkoblet internett utenom selve overføringen, så dette kan gjøres ganske trygt.

Det er flere interessante guider for nebula, med ulike perspektiver.

- [Eliseo Martelli fokuserer på sikkerhet](https://www.eliseomartelli.it/blog/nebula)
- [apalrd](https://invidious.einfachzocken.eu/watch?v=aImSCypCsuw)


<br>

## OpenZiti
Oppdager [OpenZiti](https://openziti.io/docs/learn/introduction/features), som virker å være som Nebula, med tilleggsmuligheten å bruke en [ekstern identitetstilbyder](https://openziti.io/docs/learn/core-concepts/identities/overview#3rd-party-ca
---auto-enrolled), for å logge inn i nettverket. OpenZiti har også sterkt fokus på konteinerisering og kubernetes-bruk; og har offisielle helm-kart for alle sine komponenter.

OpenZiti har også brukervennlige interaktive veiledninger for å sette opp og bruke programvaren i ulike scenarioer.

OpenZiti har applikasjoner for [Android](https://openziti.io/docs/reference/tunnelers/android/) og [iOS](https://openziti.io/docs/reference/tunnelers/iOS/), og har også mulighet for å [opprette en tilkobling kun i nettleseren](https://openziti.io/docs/learn/quickstarts/browzer/), uten å installere noe lokalt på enheten.

Ser noen OpenZiti videoer:
- [introduksjon](https://invidious.einfachzocken.eu/watch?v=qyjM5y8Op_I)


### Implementasjon
Ziti har mange komponenter, og fremmedord. Tenker jeg skal prøve å sette opp et enkelt nettverk, med en kontroller på GCP; og så skrive om prosjektet mitt på [OpenZitis diskusjonsforum](https://openziti.discourse.group/), og etterspørre hvilket oppsett og arkitektur OpenZiti bør ha for mitt formål.


## Arkitekturdiagram
Jeg lager et simpelt diagram over systemets arkitektur, for å forklare tredjeparter og partnere hva systemet skal se ut som.

![diagram of network architecture. Each node in a subnet is connected to a mesh router / control server.](../../media/pred-eye_overview.png "Nettverksdiagram")

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
