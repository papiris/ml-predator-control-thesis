
# Arbeidsøkt timelogg

### Dato: 08.02.2024
### Tidsrom: 08:00 - 09:00
### Sted: Leinesfjord, Steigen

---


# Emne 1: litteraturstudie
## Undersøke miljøbelastningen av maskinlæring
Fikk svar om strømforbruk av utviklerne av YOLOv8: https://github.com/ultralytics/ultralytics/issues/8083  
Nødvendig prosessorkraft for å trene en maskinlæringsmodell (og da også å finjustere den); skalerer lineært med antallet bilder i treningsmaterialet og antallet parametere i modellen.

Antallet objektklasser har mindre å si.

For dette prosjektets spesifikke datasett, modellkonfigurasjon og maskinvare; er nok den mest presise måten å finne ut av strømforbruk å måle forbruket av treningen.
Der kan [OpenCost](https://www.opencost.io/), [cAdvisor](https://github.com/google/cadvisor) eller [sysdig](https://sysdig.com/) komme inn. De alle monitorerer ressursbruken til Linux-konteinere, enten for seg selv (docker/podman) eller som del av en system-klynge (kubernetes/openshift).

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
