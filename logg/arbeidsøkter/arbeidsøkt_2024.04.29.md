# Arbeidsøkt timelogg

### Dato: 29.04.2024
### Tidsrom: 11:00 - 16:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Fyller utkast inn i underkapitler Resultatmål og Etisk risiko i sluttrapporten.  
Lager tabeller for Milepæler og Beslutningspunkter.

# Emne 2: Prosjektstyring/trening av objektgjenkjenning
Sender epost til Øyvind Skogstad i Statens Naturoppsyn, som jeg snakket med på rovviltmøtet i Hamarøy 18. april. Jeg ber om tilgang til bildemateriale av gaupe for å trene objektgjenkjenning.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
