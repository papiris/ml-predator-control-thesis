# Arbeidsøkt timelogg

### Dato: 04.01.2024
### Tidsrom: 12:00 - 16:00
### Sted: Bodø

-----

# Emne 1: Prosjektstart
I diskusjon med emneansvarlig Øyvind Søraas, avtaler vi at jeg skal skrive oppgave om kameraovervåkning og maskinlæring i rovviltforvaltningen.
Basert på mitt tidligere arbeid med [Farmers' Open Source Optical Predator Alert System](https://hackaday.io/project/191509-farmersopen-source-optical-predator-alert-system).
Begynner å skrive prosjektbeskrivelse


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
