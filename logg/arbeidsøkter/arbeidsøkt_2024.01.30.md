
# Arbeidsøkt timelogg

### Dato: 30.01.2024
### Tidsrom: 12:00 - 19:00, pause 14:00 - 16:00
### Sted: Strandmoa Gård, Steigen

-----

# Emne 1: Prosjektstyring
- Lisensiere prosjektdokumenter under CC-BY-SA 4.0, fordi åpen forskning er bra.
- Begynne å bruke git versjonskontroll av prosjektdokumenter, og publisere dem på https://gitlab.com/papiris/ml-predator-control-thesis


# Emne 2: Prosjektbeskrivelse
- Legge til sammendrag


# Emne 3: Litteraturstudie
Lese om eksisterende løsninger innenfor samme område.
Frigate, Zoneminder, Shinobi, Moonfire NVR, Viseron


## Zoneminder
Intro: Antageligvis den mest velutprøvde og "modne" fri programvaren for kameraovervåkning og objektgjenkjenning.
- Stort og aktivt utviklersamfunn, og etablerte sikkerhetspraksiser.
- Enormt bibliotek av støttede kameraer. Dokumentasjonen er utdatert, og installasjon litt vanskelig.
- Skrevet i PHP, Perl, C++, javascript, hvorav jeg har erfaring med ingen av dem.
- lisensiert under GPL-2.0-or-later. Dette gjør det mulig å endre lisens til AGPL-v3-or-later, men det ser ikke ut til å ha vært diskusjon om det før nå.
- Har API med standard protokoller (Websocket, MQTT, perl) for triggering av eksterne skripter ved hendelser
- Har innebygget alarmfunksjonalitet
- Har tilhørende app, som fungerer på Android, IOS, Windows, Mac og Linux. Er lisensiert GPL-v3: https://zmninja.zoneminder.com/
- Hjemmeside: https://zoneminder.com/
- Maskinlærings-modul: https://github.com/ZoneMinder/zmeventnotification
- Etter sigende begynner arkitekturen å vise sin alder
- Har et selskap i førersetet, som tilbyr betalt support og kontraktarbeid
- Støtter konteinerisering


## Shinobi
Intro: Relativt ny på banen, startet i 2016. Opererer med en progressiv lisensmodell, som låser brukeren til selskapet.
- Ikke Fri programvare lisens: https://gitlab.com/Shinobi-Systems/Shinobi/-/blob/dev/COPYING.md
-- Jeg åpnet en tråd om det på repoet deres: https://gitlab.com/Shinobi-Systems/Shinobi/-/issues/506
- Skrevet i Nodejs (javascript)
- Krever minimum $6/mnd for mulighet til å tilkoble 5 kameraer og 1 mobilapp.
Progressiv lisenskostnad, opptil $600/mnd for 1000 kameraer.
Har også betalingsmur for avansert funksjonalitet, som kryptert fjerntilgang uten personlig VPN.
- Har et selskap i førersetet, som tilbyr betalt support og kontraktarbeid
- Har API-er
- Har innebygget alarmfunksjonalitet


## Frigate
Intro: konteiner-native, ML-by-default, sterkt fokus på integrering med Home-Assistant og MQTT
- lisensiert under MIT
- bruker WebRTC
- Tilbyr egen datasett-annoterings + modelltreningstjeneste, koster $50 / år: https://frigate.video/plus/
- Støtter Coral ML-akselerator m/ tensorflow lite
- Støtter Nvidia TensorRT maskinvareakselerasjon for YOLO modeller
- Støtter avlasting av maskinlærings objektgjenkjenning over nettverk via Deepstack / Codeproject.ai
- repo: https://github.com/blakeblackshear/frigate
- dokumentasjon: https://docs.frigate.video


## Moonfire NVR
Intro: Ungt prosjekt, har ikke publisert 1.0 enda, selv om tidligste commit var i 2016. Lite utviklet, men under utvikling. Spennende.
- Lisensiert under GPL-v3
- Skriver .h264 videostrømmer direkte til disk, uten omkoding. Dette gjør det svært effektivt og ressursgjerrig i bruk.
- Skrevet i Rust og Typescript
- Svært lite prosjekt, men en god del større enn Furbinator
- Har ikke innebygget Maskinlærings objektgjenkjenning, men det er planlagt.
- Jeg har preferanse for Rust og Python, så oppstrøms bidrag er en mulighet.
- God dokumentasjon, for de få brukstilfellene og funksjonene som finnes per nå.
- Har ikke en app, og nettgrensesnittet kan kun brukes til visning.
- Sikkerhet er ikke enkelt: https://github.com/scottlamb/moonfire-nvr/blob/master/guide/secure.md
-- Må enten implementere https-støtte i inkludert webserver, bytte ut inkludert webserver, eller proxy til en webserver som støtter https.
-- Kan settes opp med traefik foran seg, med autentisering, i så måte?
- støtter konteinerisering
- Støtter per nå ikke Coral ML-akselerator m/ tensorflow lite
- repo: https://github.com/scottlamb/moonfire-nvr


## Surrey man; Furbinator 3000
Intro: Basic sak, men i grunn nesten akkurat hva jeg er ute etter.
- Bloggpost: https://medium.com/@james.milward/deterring-foxes-and-badgers-with-tensorflow-lite-python-raspberry-pi-ring-cameras-ultrasonic-75b3160faa3cs
- Hovedkode: https://github.com/Jimbwlah/tensorflow-ring-animal-detector
- Arduinokode: https://github.com/Jimbwlah/furbinator3000
- Ikke AGPL-kompatibel for nå, siden prosjektet mangler lisens.
-- Jeg åpnet en tråd om AGPL-v3 kompatibel lisens på repoet: https://github.com/Jimbwlah/tensorflow-ring-animal-detector/issues/1
- Ingen support
- Ingen utviklersamfunn rundt
- Simpel sak, simpelt oppsett
- helmanuelt oppsett, ingen integrert ONVIF kamera-bibliotek
- Støtter ikke Coral ML-akselerator m/ tensorflow lite


## Viseron
Intro: Startet for 4 år siden, basert på moduler. Skryter av brukervennlighet.
- Skrevet i Python og Typescript
- Lisensiert under MIT
- konteinerisering aktivit støttet
- Mange tilgjengelige moduler
- ingen commits siste 4 måneder, ingen store commits siden juli 2023 ==> treg utvikling
- Har flere moduler for objektgjenkjenning med maskinlæring
- Repo: https://github.com/roflcoopter/viseron
- Hjemmeside: https://viseron.netlify.app/


## MotionPlus / MotionEye
Intro: Gammel og moden applikasjon. Nylig renovert.
- Lisensiert under GPL-v-3-or-later
- Hjemmeside: https://motion-project.github.io/
- Repo backend: https://github.com/Motion-Project/motionplus
- Repo frontend: https://github.com/motioneye-project/motioneye
- Frontend er python og javascript. Backend er C++
- full https støtte, samt autentisering
- Kjør skripter ved hendelser
- inkludert støtte for maskinlærings objektgjenkjenning

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
