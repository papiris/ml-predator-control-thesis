# Arbeidsøkt timelogg

### Dato: 12.03.2024
### Tidsrom: 08:30 - 13:00, 15:00 - 16:00, 20:00 - 23:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: Prosjektstyring 

## Statusrapport 
Begynner å skrive statusrapport. Skriver alt utenom Fremdriftsstatus, og leverer utkastet på Canvas. Gjør resten i morgen.

## Lisensiering
Setter maskinlesbare lisens-markører inn i alle markdown-filer i prosjektet, for å vise tydelig at de alle er under fri kultur-lisensen CC-BY-SA-4-0+. Lisensmarkørene følger den internasjonale standarden SPDX (Software Package Data Exchange) v2.3. 


# Emne 2: Kravspesifikasjon 
Hittil har kravspesifikasjonen levd som del av konstruksjonsgrunnlaget; men for å separere hensyn blir også filene separert.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
