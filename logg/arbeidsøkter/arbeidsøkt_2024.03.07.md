
# Arbeidsøkt timelogg

### Dato: 07.03.2024
### Tidsrom: 13:00 - 16:00, 18:00 - 22:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konseptutvikling

## FOSOPAS
Gjør arbeid på FOSOPAS.
- Gjør skriptet om til en installerbar python-modul
- Gjør konfigurasjonsfil importerbar i programvaren
- Går over til å bruke RT-DETR i programvaren
- Fjerner kommandolinje-argumenter

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
