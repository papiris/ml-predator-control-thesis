
# Arbeidsøkt timelogg

### Dato: 14.02.2024
### Tidsrom: 9:00 - 15:00
### Sted: Tjeldberget, Bodø

---

# Emne 1: Konstruksjonsgrunnlag

## Flette sammen konsepter
Ser videre på tre eksisterende løsninger;
- Frigate, Viseron og Zoneminder
- Nebula og Tailscale
- Traefik, Authentik og Atuhelia

Nebula har ikke per nå en kontainer, eller helm chart for kubernetes. 
Relevant issue: https://github.com/slackhq/nebula/issues/25  
sudo docker run -d --restart always --name nebula  --net host --cap-add=NET_ADMIN --cap-add=SYS_ADMIN --device=/dev/net/tun -v $PWD:/app -w /app debian:buster-slim ./nebula -config ./nebula.config.yml


Tailscale er tilgjengelig som konteiner, og har god integrering med kubernetes
Dokumentasjon: https://tailscale.com/kb/1185/kubernetes

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
