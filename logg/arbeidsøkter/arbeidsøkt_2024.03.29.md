# Arbeidsøkt timelogg

### Dato: 29.03.2024
### Tidsrom: 13:00 - 18:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Ferdigstiller en fungerende (grunnleggende) ende-til-ende implementasjon av webpush-varsler.  
Løypa var:  
1. Få serviceworker og push til å bli ordentlig registrert og startet på klienten.  
2. Få push på klientsiden til å sende klient-id (abbonnement) til backend, og få backend til å akseptere det.  
3. Få backend til å lagre abbonnements-info i database.  
4. Få sendt ut varsler fra backend til service worker.  
5. Få service worker til å ta imot varsler fra backend, og vise dem.  

I dag har jeg løst 4 og 5.  
Nå vil alle abbonnerte klienter motta periodiske opssummeringer av gjenkjente objekter.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
