
# Arbeidsøkt timelogg

### Dato: 21.02.2024
### Tidsrom: 15:00 - 17:00, 18:00 - 20:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## Nebula
Legger til brannmur-regel i Google Cloud Platform (GCP) om å akseptere inngående UDP-trafikk til port 4242, og likeledes i `iptables` i den Virtuelle Maskinen (VM).

Fortsetter oppsett av Nebula nettverk; med å inkludere laptopen min i nettverket.  
Det funker så som så, har problemer med at "handshake" mellom laptop og fyrtårn ikke fullfører. Det kommer ikke opp i loggen til fyrtårnet at det har mottatt noen "handshake" pakke, så jeg undrer hva som skjer.

Etter å ha lagt til <code>respond: true</code> i nebula konfigurasjonsfilas <code>punch</code> seksjon, som tilrådet i [Nebula issue #660](https://github.com/slackhq/nebula/issues/660#issuecomment-1102535594) får jeg opprettet kontakt knirkefritt. 

Husk; `systemctl reload nebula` ved endret konfigurasjon, ikke `systemctl restart nebula`.

### isolering av brukere
Nebula har støtte for "tagger", og å tillate at kun verter med samme tagger kan kommunisere med hverandre. Det kan være aktuelt å bruke hashede kundenr som tagg, sånn at alle enhetene til én enkelt bruker kan kommunisere, uten at andre brukere kan legge til taggen hos seg manuelt.


### Nebula intern DNS
Jeg aktiverer DNS-funksjon på fyrtårnet, slik at verter kan nås med sine vertsnavn. For dette, lager jeg en ny brannmurregel i GCP som tillater inngående UDP-trafikk på port 53; for DNS-forespørsler.


<br>


## Dynamisk DNS
Når man har en server i et datasenter, en hjemmeruter eller en mobiltelefon; tildeles nesten alltid den eksterne IP-adressen til enheten dynamisk. Adressen kan altså endres relativt ofte.  
DNS er systemet som holder styr på hvilke URL'er (f.eks. uit.no) som hører til hvilke IP-adresser.

Med dynamisk DNS holdes IP-addressen til spesifikke URL'er oppdatert, gjennom at maskinen som ønsker å vite sin IP-adresse; spør en ekstern tilbyder hvilken ekstern adresse senderen har. Deretter gir DynDNS-programmet beskjed til en DNS-server om at IP-adressen til den relevante URL'en er endret; og ber om at den oppdateres.

I mitt tilfelle bruker jeg [Timothy Miller's Cloudflare-ddns](https://github.com/timothymiller/cloudflare-ddns/), som kjøres i k3s klynga på Debian VM'en hos GCP.  
Den sørger for at URL'en <https://net.ingeniorskap.no> alltid peker på den faktiske eksterne IP-adressen som tilhører VM'en. DNS-oppføringen bruker ikke Cloudflares proxy-funksjon.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
