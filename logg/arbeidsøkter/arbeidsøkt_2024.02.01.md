
# Arbeidsøkt timelogg

### Dato: 01.02.2024
### Tidsrom: 21:00 - 21:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Ingen arbeid i dag
Grunnet ekstremværet Ingunn har Trollfjord Bredbånds kunder i Steigen vært [foruten internett-tilgang i dag.](trollfjord.no/driftsmeldinger)  
Fordi så mye av litteraturstudiet avhenger av søk på nettet, bestemte jeg meg for å heller bruke dagen på ting jeg ikke trengte internett til; som ikke var prosjektrelaterte.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
