
# Arbeidsøkt timelogg

### Dato: 06.03.2024
### Tidsrom: 10:30 - 15:00, 20:00 - 23:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1:  Konseptutvikling

## FOSOPAS
Gjør arbeid på FOSOPAS: Går bort fra bruk av kommandolinje-argumenter ved kjøring av skriptet, til en TOML-formatert konfigurasjonsfil. Dette åpner for mer fleksibel og repeterbar bruk; og gjør det enklere å konteinerisere programmet.

Bytter ut bruk av print()-funksjonen med logging()-funksjonen; for å ha bedre kontroll på beskjedene som betyr noe, uten å bli oversvømt av mindre viktige beskjeder.

Bytter ut lisens-tekst i headeren til skriptet med en SPDX-lisens-identifikator. Dette forenkler maskinell redegjørelse for såkalte SBOM, programvare-datablad.

Bytter ut programvarebiblioteket SAHI med en midlertidig forgrening som implementerer støtte for bruk av RT-DETR modeller.

Det FOSOPAS behøver for å kunne funke halvgreit, er:
- Mulighet for å varsle per epost, uten å måtte oppgi passord i klartekst
- Mulighet til å slå av og på ulike funksjoner (typer varsel, display)
- Ingen unødvendig bildegjenkjenning når det ikke er bevegelse eller interessante objekter i interesseområdet.
- Mulighet til å avklare et interesseområde vha polygon-koordinater

# Emne 2: Statusrapport og -møte 1
Statusrapport 1 skal leveres 12. mars, og statusmøte skal holdes 15.mars. Det er ikke lenge til, og jeg må lage ferdig en rapport til da.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
