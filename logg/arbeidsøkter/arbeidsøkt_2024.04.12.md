# Arbeidsøkt timelogg

### Dato: 12.04.2024
### Tidsrom: 10:00 - 12:00, 19:00 - 24:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Utvikling
Videreutvikler minimumseksempelet for OpenZiti, ved å implementere samme type arkitektur som hovedprosjektet.  
- Én hovedprosess som starter de andre prosessene  
- Én prosess som produserer bilder (fra webkamera)  
- Én prosess som gjør nettrelaterte ting  
- Underprosessene deler en kø for base64-kodede bilder  

Dette viser seg å perfekt gjenskape feil-oppførselen fra hovedprosjektet, når openziti brukes.

Manager.Queue er mer kompleks enn Manager.Pipe, derfor prøver jeg å bruke Pipe også.  
Som man kan se under, er q_img ulike typer objekter på ulike minneaddresser; noe det absolutt ikke skal være.  Hver prosess holder hver sine ender av Pipe, så det er ikke problematisk at addressene deres er ulike.

```python
q_img in run_webapp: <AutoProxy[Queue] object, typeid 'Queue' at 0x7ff6e338da90; '__str__()' failed>
pipe_img_recv in run_webapp: <multiprocessing.connection.Connection object at 0x7ff6e142a510>
q_img in frame_producer: <queue.Queue object at 0x7ff6dc933f10>
pipe_img in frame_producer: <multiprocessing.connection.Connection object at 0x7ff6e0bd4290>
```

Verifiserte at Pipe sender bilder til webprosessen når den kjører openziti, uten problemer.  
Når openziti ikke brukes, er kø-objektene like hverandre, og alt funker:  
```python
q_img in run_webapp: <queue.Queue object at 0x7f31142f2650>
pipe_img_recv in run_webapp: <multiprocessing.connection.Connection object at 0x7f31197da650>
[2024-04-12 23:08:06 +0200] [639254] [INFO] Running on http://127.0.0.1:8443 (CTRL + C to quit)
q_img in frame_producer: <queue.Queue object at 0x7f31142f2650>
pipe_img in frame_producer: <multiprocessing.connection.Connection object at 0x7f311423ac90>
```


Melder ifra om funnet på openziti forumet, i tråden til prosjektet mitt.



<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
