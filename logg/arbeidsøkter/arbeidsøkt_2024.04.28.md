# Arbeidsøkt timelogg

### Dato: 28.04.2024
### Tidsrom: 18:30 - 23:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Fyller utkast inn i kapitler i sluttrapporten.  
Setter opp integrasjon mellom referanse-håndteringsprogramvaren [Zotero](https://www.zotero.org/) og TeXStudio.  
Finner oppdaterte kilder til enkelte data, og refererer til dem.

# Emne 2: Prosjektstyring
Hittil i prosjektet har gratisversjonen av OpenAI sin ChatGPT3.5 blitt brukt som [sparringspartner for feilsøking](https://en.wikipedia.org/wiki/Rubber_duck_debugging) og for å få en oversikt over diverse fagfelt og emneord å sette meg nærmere inn i. Dette har noen uheldige etiske implikasjoner, siden alle samtaler man har med modellen deles med OpenAI for videre trening av modellen. Det er et kjent fenomen at LLM-er kan produsere deler av datasettet de er trent på, ordrett, under visse omstendigheter. Selv om programvaren er lisensiert AGPLv3, ville dette omgått lisensen.

[Metas Llama3-modell](https://llama.meta.com/llama3/) skårer høyere enn ChatGPT3.5 på ytelsesmålinger, har en komparativt mer åpen lisens og kan kjøres lokalt.

Går derfor over til å bruke Llama3 som sparringspartner. Som server bruker jeg [Ollama](https://ollama.com/), og som brukergrensesnitt [Open WebUI](https://openwebui.com/).

Jeg har nå full kontroll over hvor mye data som deles med andre.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
