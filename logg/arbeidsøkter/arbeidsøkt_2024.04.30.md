# Arbeidsøkt timelogg

### Dato: 30.04.2024
### Tidsrom: 11:00 - 17:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på kapittelet om system, med fokus på underkapittelet Programvare.  
Lager en oppdatert graf av avhengigheter med `pydeps`, og oppdager at grafikken er for bred. Finner ingen reell mulighet i `pydeps`for å begrense bredden av grafen, og åpner derfor et [funksjonsønske på programvarelageret deres](https://github.com/thebjorn/pydeps/issues/220)

Lærer å bruke LaTeX funksjonene for sitering, forkortelser og ordbok.

Lista over forkortelser og ordboka krever et ekstra kompileringssted for å bli vist som del av PDF-en, så jeg endrer bygge-kjeden fra  
`txs:///compile | txs:///view`  
til  
`txs:///compile | txs:///makeglossaries | txs:///view`  
Som funker.

Lager referanser for programvarebiblioteker brukt i utviklingen.

Legger til vedlegg.

Ber om tilbakemelding fra veileder.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
