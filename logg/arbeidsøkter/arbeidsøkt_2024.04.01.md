# Arbeidsøkt timelogg

### Dato: 01.04.2024
### Tidsrom: 10:00 - 12:00, 16:00 - 00:30
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Utvikling
Fortsetter arbeidet med å implementere Zero-Trust nettverking med OpenZiti.  
Finner ut at det oppstår konflikt mellom Tailscale og OpenZiti dersom begge kjører på samme maskin, som medfører at ingen ziti-tjenester kan nås. Melder ifra om det på OpenZiti-forumet, og slår Tailscale-tjenesten lokalt på maskinen min, midlertidig av for å bøte på  problemet.

NetFoundry's nfconsole viser seg å ha milevis bedre brukergrensesnitt og dokumentasjon enn ZEDS, og jeg går derfor over til å bruke den inntil videre. Etter noe krøll får jeg til å servere predalert nettstedet (uten videoer og push-varsler) over ziti, og nå nettstedet over ziti.

Oppdaterer pyproject.toml sånn at Predalert igjen kan installeres på andres maskiner, uten å behøve noen variabler eller programmer lokale til min maskin. Dette vil gjøre utvikling enklere, ettersom programmet er såpass komplekst nå at det er vanskelig å destillere ned hele funksjoner til en MVE (minimumseksempel), som man bør gjøre når man ber andre hjelpe til med feilsøking.  
Med et enkelt installerbart program, håper jeg at andre noenlunde enkelt kan forstå hvor feilen ligger, uten at jeg må bruke lang tid på å lage en MVE og verifisere at den gjenskaper hele feilen.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
