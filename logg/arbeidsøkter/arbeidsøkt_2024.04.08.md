	# Arbeidsøkt timelogg

### Dato: 08.04.2024
### Tidsrom: 12:00 - 14:00, 21:00 - 23:00
### Sted: Steigentunet, Steigen; Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving
Finner ut av problemene med LaTex ved hjelp av Erlend Graff, og får oversendt utkast til disposisjon til veileder Halldor.


# Emne 2: Utvikling
Lager et minimums-eksempel av python-kode som trigger `feil 11, ukjent feil` ved bruk av openziti; og publiserer den i openziti-forumet. Vedlikeholderne repliserte feilen på sitt system, og arbeider med å gjøre openziti mer robust når det brukes i kombinasjon med diverse web-rammeverk.


# Emne 3: Prosjektstyring
Fører fremdriftslogg for uke 14.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
