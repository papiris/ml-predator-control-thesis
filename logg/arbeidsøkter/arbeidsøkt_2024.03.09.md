
# Arbeidsøkt timelogg

### Dato: 09.03.2024
### Tidsrom: 10:00 - 18:00
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: Konseptutvikling

## FOSOPAS
Separerte relaterte funksjoner ut fra hovedskriptet, til hver sine filer.   
Forbedret dokumentasjon av funksjoner og objekter.  

Prøver først å implementere en nettside med Django, men det viser seg svært omstendelig. Bytter derfor til Flask.
Implementere en simpel nettside med Flask, som delvis fungerer. Mye feilsøking i den anledning. Det viste seg at Flask, når `debug=True` , restarter den innebygde webserveren. Dette gjorde at kø-objektene som hovedskriptet delte med flask-prosessen gjennom Flasks `config`-objekt; ble klonet, og det var klonen som ble videreført i Flask-prosessen etter restarten. Når produsent-skriptene da fylte på de originale kø-objektene, hadde ikke Flask-prosessen tilgang på dem.

Gjør feil i koden synlige ved å legge til flere hendelser som sender deskriktive loggmeldinger ved feil.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
