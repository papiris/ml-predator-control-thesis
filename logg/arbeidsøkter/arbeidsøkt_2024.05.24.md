# Arbeidsøkt timelogg

### Dato: 24.05.2024
### Tidsrom: 09:00 - 10:00, 11:00 - 16:00, 19:30 - 21:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Rapportskriving
Skriver på System, og annet.

# Emne 2: Utvikling (2,5 timer)

Konteineriserer Predalert programvaren.  
Lager fungerende `compose.yaml`, så nå kan programmet startes med kun `podman-compose up`; gitt en fungerende konfigurasjonsfil

# Emne 3: Prosjektstyring (4 timer)
Fyller ut fremdriftsplan f.o.m. 23. april. Hang en måned på etterslep der ja.

## Veiledningsmøte 4
Møter Halldor kl 15 - 15:50


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
