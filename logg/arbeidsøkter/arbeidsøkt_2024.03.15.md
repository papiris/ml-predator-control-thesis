# Arbeidsøkt timelogg

### Dato: 15.03.2024
### Tidsrom: 10:30 - 12:00
### Sted: I bilen på vei fra Steigen til Bodø

---

# Emne 1: Prosjektstyring
Skriver dokumentasjon fra statusmøte 1 som ble holdt tidligere i dag.  
Gjør klart et brukermiljø for å bruke LaTex til skriving av sluttrapporten. Prøver [Texmaker](https://www.xm1math.net/texmaker/index.html) og [TexStudio](https://texstudio.org). Bruker [UiTs LaTex-mal for masteroppgaver og Doktoravhandlinger](https://github.com/egraff/uit-thesis?tab=readme-ov-file).


 
<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
