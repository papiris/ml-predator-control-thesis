
# Arbeidsøkt timelogg

### Dato: 07.02.2024
### Tidsrom: 11:00 - 16:30, 20:00 - 21:00
### Sted: Håsand Gård, Steigen

---

# Emne 1: litteraturstudie
## Undersøke personvernlovgivning
Et sentralt krav i peronopplysningsloven er at ethvert system eller løsning skal ha innebygd personvern.
Hva som menes med det generelt, og i programvareutvikling spesielt, har datatilsynet veiledning på hhv. [her](https://www.datatilsynet.no/rettigheter-og-plikter/virksomhetenes-plikter/innebygd-personvern-og-personvern-som-standard/) og [her](https://www.datatilsynet.no/rettigheter-og-plikter/virksomhetenes-plikter/programvareutvikling-med-innebygd-personvern/).

## Undersøke miljøbelastning av dyrehold
Hva er miljøbelastningen av ett lam som dør, og dermed ikke havner på matfat?  
Hvis man går ut fra at forbruket av kjøtt fra småfe og rein er forutbestemt og stabilt, og produksjonen tilpasser seg forbruket; hvor mye kan miljøbelastningen av dyreholdet reduseres dersom man forhindrer tap av husdyr til rovvilt?

Langtidseffekten (frem til år 2100) av å gradvis redusere produksjonen av dyrebaserte matvarer over en periode på 15 år fra 2020, per kg matvare: 
- fårekjøtt: 286kg aCO2e 
- storfekjøtt: 298kg aCO2e
- fåremelk: 23kg aCO2e
- storfemelk: 10kg aCO2e

https://journals.plos.org/climate/article?id=10.1371/journal.pclm.0000010

<br>

Husdyrbasert matproduksjon står for mellom 11 og 16% av totale menneskelige klimagassutslipp. 10% reduksjon i utslipp fra dyrebasert matproduksjon åpner for 1,5% økning i utslipp fra industriell sektor. Dersom klimagassutslipp fra matproduksjon holdes stabil, må utslippene fra industri reduseres med 80% før 2050 for å holde global oppvarming under 2 grader Celsius.
https://www.nature.com/articles/s43016-021-00265-1

<br>

Det finnes ikke statistikk for kjøttvekt og alder på dyr tapt til rovvilt, derfor brukes gjennomsnittlig slaktevekt når tapt kjøttproduksjon beregnes. Dette medfører overestimering, men å innhente og beregne mer presise data for dette er utenfor prosjektets fokusområde.  
I mars 2023 var det 915 344 vinterfôra sau i Norge.  https://www.ssb.no/jord-skog-jakt-og-fiskeri/jordbruk/statistikk/husdyrhald  
I 2022 ble 81 638 slaktelam godkjent til menneskemat, som tilsammen veide 1925 tonn slaktevekt.  https://www.ssb.no/jord-skog-jakt-og-fiskeri/jordbruk/statistikk/kjotproduksjon   https://data.norge.no/datasets/b8da2c09-d2ab-3258-9bc7-ec2472a7e9fd  
Gjennomsnittlig slaktevekt var da 23,6kg.


Statistikken i Nature bruker kg kjøtt, imens slaktevekt inkluderer skjelett og andre deler av slaktet som ikke normalt spises. Ifølge Universitetet i Wisconsin-Madison blir rundt halvparten av slaktevekta av sau, brukbar mat. https://livestock.extension.wisc.edu/articles/how-much-meat-should-a-lamb-yield/   
På Strandmoa Gård i Steigen, med blanding av rasene villsau, spæl og dorper; var 40% av slaktevekta kjøtt. Dette er ikke representativt for Norge.

Jeg kan da estimere at hvert lam tapt til rovvilt innebærer 10 kg tapt kjøtt, og unødvendig utslipp på 2860kg annualiserte CO2-ekvivalenter (år 2100).


---

## Undersøke miljøbelastningen av maskinlæring
Nødvendig prosesseringskraft som brukes av en maskinlæringsmodell for objektgjenkjenning kan deles inn i to faser:
- treningsfasen
- bruksfasen 

Nødvendig prosessorkraft og strøm varierer sterkt med antall objektklasser man ønsker å trene på; maskinvaren man bruker for treningen, treningens parametere, ønsket presisjon, samt modellen man trener. Det er ofte store besparelser i å fin-justere en eksisterende modell-fil, heller enn å trene en ny modell-fil fra grunnen av.

Det tar mellom 2 og 5 dager å trene en modell-fil av YOLOv8-nano fra grunnen av; på COCO datasettet med 80 objektklasser, på en Nvidia A100 GPU. Treningstiden øker for større modeller. https://github.com/ultralytics/ultralytics/issues/5172

Stiller spørsmål om strømforbruk til utviklerne av YOLOv8: https://github.com/ultralytics/ultralytics/issues/8083


## Skrive oppsummering av litteratur
Skrive kapitler 3, 4 og 5 om rammeverk for brukergrensesnitt, relevante standarder og lovverk, og miljøbelastning.


---

# Emne 2: Prosjektstyring
For å imøtekomme forsinkelser, inngår jeg avtale med prosjektveileder Halldor Arnason om å ha 30min framdriftsmøter over video annenhver uke. Jeg inngår også avtale med en medstudent om å være studiekamerater, der vi studerer sammen virtuelt, selv om vi ikke studerer det samme.

Lage README.md fil, som folk vil se når de besøker git repoet på gitlab.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
