
# Arbeidsøkt timelogg

### Dato: 11.03.2024
### Tidsrom: 08:30 - 20:00, 23:00 - 00:00
### Sted: Håsand Gård, Steigen

----

# Emne 1: Prosjektstyring
## Fremdriftsplan
Oppdaterer fremdriftsplan.

## Avviksmeldinger
1. Jeg har ikke skrevet avviksmeldinger hittil, selv om det har vært flere avvik. Den originale planen var ikke fullstendig realistisk; og forsøket på å smelte sammen UiTs krav til distinkte faser i plan og gjennomføring av bacheloroppgave i Maskinteknikk, og de iterative metodene jeg bruker i programvareutvikling, har ikke båret frukt. 

2. Jeg lærte svært mye under litteraturstudiet, som har kommet til nytte. Fokuset på å lære _alt jeg ville trenge senere_ før jeg kunne gå videre, gjorde at jeg brukte mye tid på å undersøke ting som _kanskje_ ville komme til nytte; og mindre viktige aspekter. 

3. Jeg har ikke begynt å skrive statusrapport 1 enda, og den skal leveres 12.03.


## Statusrapport
Jeg har enda ikke begynt å skrive statusrapporten som skal leveres i morgen. Det må jeg gjøre.


# Emne 2: Konseptutvikling
## FOSOPAS
- Implementerer en fungerende nettside som bruker Webgear og det asynkrone rammeverket Starlette; for å vise annoterte bilder fra analyserte videokilder, i et adaptivt rutenett.
- Forbedrer dokumentasjon ved å skrive "docstrings" på alle funksjonsdefinisjoner som forklarer hva funksjonen gjør og hvilke inputs den forventer.
- Forsøker å implementere Zero Trust nettverk i applikasjonen med OpenZitis Python SDK.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
