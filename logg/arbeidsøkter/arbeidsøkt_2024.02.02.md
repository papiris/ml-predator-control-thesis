
# Arbeidsøkt timelogg

### Dato: 02.02.2024
### Tidsrom: 12:00 - 16:20
### Sted: Strandmoa Gård, Steigen

---

# Emne 1: litteraturstudie
Nettverkstilgang er nå gjenopprettet, og arbeidet kan fortsette.  

## Relevante standarder

### EUs Cyber Resilience Act
https://digital-strategy.ec.europa.eu/en/library/cyber-resilience-act-factsheet  
- I løpet av 2024 blir EUs Cyber Resilience Act den første loven som krever at programvare (og maskinvare med programvare-elementer) solgt i EU, skal følge standarder for utvikling, ansvar, tilgjengelighet og datasikkerhet. Produsenter må etterfølge standardene innen 36 måneder, så det er ikke umiddelbart klart at dette prosjektet må etterleve kravene.
 
### Standard for informasjonssikkerhet i datasystemer ISO/IEC 27000:2018
https://www.iso.org/standard/73906.html  
- Ikke offentlig tilgjengelig, ei heller gjennom UiTs abbonnement hos Norsk Standard.  

### NS-EN ISO/IEC 27002:2022 for Informasjonssikkerhetstiltak
- tilgjengelig gjennom UiTs abbonnement  https://online.standard.no/nb/ns-en-isoiec-27002-2022 
- Tre aspekter: Konfidensialitet, integritet, tilgjengelighet.
- Kapittel 5.8: Informasjonssikkerhet som del av prosjektstyring.  
-- Informasjonssikkerhetsrisikoer bør vurderes på et tidlig stadie, og oppdateres underveis i prosjektet. 
-- Vurdér hvilken informasjon som er involvert, hva som trengs for å beskytte den, tilgangskontroll, informering av brukere om deres ansvar og risiko, vurdering av tredjeparters pålitelighet.
- Lag oversikt over informasjonsverdier

### NS-ISO/IEC 27017 for skytjenester

### NS-ISO/IEC 27701 for personvern

### NS-ISO 21500 og 21502 for Prosjektstyring i IKT

### NS-ISO/IEC 27005 for risikostyringsprosesser

### Digitalytelsesloven
https://lovdata.no/dokument/NL/lov/2022-06-17-56  
- Paragraf 8, punkt 3: "{produktet skal} leveres med tilbehør, innpakning, installasjonsveiledning og lignende som forbrukeren med rimelighet kan forvente å få"  
- Paragraf 8, punkt 6: "{produktet skal} være upåvirket av tredjepersoners rettigheter i ytelsen, for eksempel eiendomsrett, panterett eller immaterialrettigheter." <== Hva betyr det mtp fri programvare?  
- Paragraf 9: "Leverandøren skal sørge for at forbrukeren får beskjed om og får levert oppdateringer, herunder sikkerhetsoppdateringer, som er nødvendige for å oppfylle kravene i §§ 7 og 8."  
- Ellers gjelder at man ikke kan avtale seg bort fra lovkravene, at man skal levere det man averterer og lover.  
- Paragraf 26: Krav om erstatning, retting, heving, avslag kan kun rettes mot tidligere ledd i avtalekjeden (oppstrøms fra B2C-leverandør), dersom det tidligere leddet opptrer i næringsvirksomhet.  
- Paragraf 40: Spesifikasjon om ytelsen, når kunden skal bidra til utforming av spesifikasjonen, kan utformes av leverandøren dersom ikke kunden bidrar innen tidsfristen.
- Paragraf 45: Ved heving av avtalen, skal leverandør gjøre forbrukers data  tilgjengelig for kostnadsfri nedlasting i et ordinært, maskinlesbart format.


## Verktøy for ekstern tilknytning
Avveininger:
- Er det ønskelig med kommunikasjon mellom klienter i nettverket, eller skal det etableres enkeltvis tilkobling mellom hver klient (mobiltelefon) og tjener (overvåkningsprogram)?

### Veilid
Intro: Utviklet av det anarkistiske hackerkollektivet Cult of the Dead Cow, Veilid er et ungt rammeverk for ende-til-ende vert-til-vert kryptert zero-trust kommunikasjon.  
Nettside: https://veilid.com/
- Fordeler:
	- Fullstendig distribuert, ingen behov for å lite på eksterne.
	- Alle nettverkets deltakere bidrar med å videresende trafikk ==> Økt antall deltakere medfører automatisk økt båndbredde.
	- Skrevet i Rust, et trygt språk
* Ulemper:
	- Relativt ungt prosjekt, utgitt sommeren 2023.
	- Lite dokumentasjon
	- Ingen fullstendige integrasjoner, i noen språk, for sending av video eller bilder.
* Interessante objekter:
	- https://github.com/stillonearth/bevy_veilid
	- https://github.com/socketwench/veilid-server-alpine
	- https://github.com/spithash/Veilid-IRC-Bot
	- https://github.com/stillonearth/veilid_duplex  !
	- https://github.com/miampf/bote/blob/main/src/config.rs
	

### Tailscale
Intro: et sentralisert overlay VPN-nettverk, hvor sentralen kun administrer hvordan verter kan koble seg sammen med hverandre, uten at sentralen håndterer trafikken deres.
- Ikke fri programvare på tjenersiden.
-- En friprog implementasjon av tjenersiden kalles headscale. https://github.com/juanfont/headscale
- Jeg har erfaring med å bruke tailscale


### Nebula
Intro: Et overlay nettverk utviklet av Slack. Omtrent likt som Headscale, Tailscale og netbird
- Fri programvare tvers gjennom
- Testet av uavhengig sikkerhetsrevisor
- Bloggpost: https://medium.com/several-people-are-coding/introducing-nebula-the-open-source-global-overlay-network-from-slack-884110a5579
- repo: https://github.com/slackhq/nebula

### Netbird
Et sentralisert Overlay VPN-nettverk, ikke så ulikt Tailscale og Headscale

- https://github.com/netbirdio/netbird

### Spritely
Et rammeverk for å lage klient-til-klient applikasjoner, fri fra tjener-klient modellen for internett.
- Under utvikling
- Objekt-orientert programmering
- Støtter språkene Guile og Racket, begge språk jeg ikke har noen erfaring med.
- Hjemmeside: https://spritely.institute

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
