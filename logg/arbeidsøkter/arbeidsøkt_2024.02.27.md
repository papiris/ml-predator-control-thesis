
# Arbeidsøkt timelogg

### Dato: 27.02.2024
### Tidsrom: 10:00 - 15:15
### Sted: Håsand Gård, Steigen

---

# Emne 1: Konstruksjonsgrunnlag

## OpenZiti
Møter Philip Griffith i OpenZiti/Netfoundry.

## Moonfire-NVR
Prøver å kjøre Moonfire utenfor konteiner, som en statisk kjørbar fil. Får fortsatt ikke bilde fra lokale IP-kameraer, så Moonfire legges bort for nå.

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
