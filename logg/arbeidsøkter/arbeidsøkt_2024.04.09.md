# Arbeidsøkt timelogg

### Dato: 09.04.2024
### Tidsrom: 10:00 - 15:00, 17:00 - 21:00
### Sted: Strandmoa Gård, Steigen

---


# Emne 1: Rapportskriving (6 timer)
Arbeider videre på sluttrapportens disposisjon, samt prøver å fikse en programvarelus der alle kapitler blir underkapitler av kapitlene Takksigelser eller Sammendrag. Finner etterhvert ut at kapitler må defineres me `\chapter` og ikke `\section`.  

Finner ikke veiledning i bruk av LaTex-malen, ei heller hvordan man kan inkludere bacheloroppgavens påkrevde tabell i LaTex-dokumentet. Lager derfor en ønskerapport i repoet til [UiT-thesis latex-malen](https://github.com/egraff/uit-thesis/issues/87) om integrert støtte for tabellen, sender epost til UiTs skrivesenter og ber om veiledning i LaTex; og sender epost til veileder Halldor og spør om tabellen kan være formatert på annet vis enn i kravdokumentet.


# Emne 2: Utvikling
Lager et minimums-eksempel av python-kode som trigger `feil 11, ukjent feil` ved bruk av openziti i kombinasjon med `fastapi.responses.StreamingResponse`. Det viser seg at en simpel StreamingResponse som funker i en ordinær simpel nettside, også funker over openziti-nettverket. Dette betyr at jeg sannsynligvis har en implementasjonsfeil i webappen min PredAlert.




<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
