# Arbeidsøkt timelogg

### Dato: 10.05.2024
### Tidsrom: 10:15 - 17:00, 20:00 - 23:00
### Sted: Håsand Gård, Steigen

---


# Emne 1: Utvikling/Trening av maskinlæringsmodell
Feilsøking viser at Label Studio ikke takler import av filnavn som har tegn utenom utf-8. Forsøk på å synkronisere en kilde som har slike filer i seg, ødelegger kilden permanent.

Jeg følger [dette issuet sine reparasjonssteg](https://github.com/HumanSignal/label-studio/issues/4874), som innebærer omdøping av støtende filnavn og sletting av støtende enheter i appens database. Dette fikser saken!

Installerer så en [ML-backend](https://github.com/kieefuanh/LabelStudio-Yolov8-Detection-Backend) for å få forhåndsutfylte annoteringer av bilder. Endrer skriptet for å bruke RT-DETR. Det funket ikke helt, fordi skriptet ikke implementerer korrekte funksjoner. (setup() er viktigst her)

Prøver deretter en [docker-compose-variant av backenden](https://github.com/35grain/label-studio-yolov8-backend), med mer komplett implementasjon.

Får koblet dem sammen, men det viser seg at backenden har en innebygd blokkering, som gjør at dersom URL'en inneholder domenet "localhost", funker ikke lokal lagring. Bruker mye tid på å finne ut av omveier, men lykkes ikke i dag.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
