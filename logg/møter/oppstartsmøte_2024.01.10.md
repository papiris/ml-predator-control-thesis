<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->

# oppstartsmøte Bacheloroppgave UiT 10.01.2024

Send utkast til prosjektbeskrivelse til prosjektveileder minimum ei uke før frista. Altså senest 17.01.
Sjekk malen i Canvas!
Sjekk Wangs' bacheloroppgave, bruk metode for produktutvikling på side 107



## Presentasjon av mitt bachelorprosjekt
Utvikling av programvare for innhenting av bilder fra overvåkningskameraer ved hjelp av åpne standarder, objekt-gjenkjenning av rovdyr på bilder ved bruk av maskinlæring, automatisk opptak og varsling


Fri Programvare, respekterer brukernes frihet og personvern ==> all kode utviklet i prosjektert publiseres åpent under AGPLv3
Tillit.

Arkitektur som minimerer avhengighet av lukkede tredjeparter og informasjonslekkasje.


Egen oppgave ==> opprette ENK som oppdragsgiver
Bacheloroppgave Tyskland; venter på svar fra studieadministrasjonen, Øyvind



oppgaver:
litteraturstudie (rammeverk, fundamentmodeller, containerteknologi, eksisterende løsninger, maskinvare)
kundeoppfølging (behovene på Strandmoa Gård, andre gårdbrukere)
utvikle programvare etter CI/CD-metodikk / iterativ prototyping
trene maskinlæringsmodeller (innhente bilder, markere, trene)


Resultatmål:
- Programvare som (med fjernassistanse) kan implementeres hos gårdbrukere med diverse overvåkningskameraer i sauegjerdene.
- Programvare gjenkjenner de vanligste rovdyrene som utfører skade på husdyr (gaupe, ulv, rev), med akseptabel nøyaktighet.
- Programvare varsler gårdbrukers mobiltelefon når rovdyr oppdages
- Programvare lagrer automatisk video/bildesekvens med rovdyr
- Lagrede materialer er tilgjengelig for gårdbruker gjennom nettgrensesnitt
- Programvare kan kobles til eksterne tjenester for å f.eks. automatisk skremme vekk rovvilt

Samfunnsmål:
- Unngå tap av husdyr til rovvilt ==> 16 100 lam og sau i 2022
Minimere lidelser,
- redusere unødvendig ressursbruk ==> Programvaren må kreve færre ressurser enn sauene som tas av rovvilt uten den.
- redusere konflikter mellom rovvilt og landbruk
