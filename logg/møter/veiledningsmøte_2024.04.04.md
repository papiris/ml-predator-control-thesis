# Veiledningsmøte 3, bacheloroppgave UiT
### Dato: 04.04.2024
### Tid: 12:00 - 12:30
### Deltakere: Halldor Arnason, Jacob Ludvigsen
---
## Sak 1: Hva er status?
* Har implementert grunnleggende ende-til-ende støtte for pushvarsler via nettleser.  
* Har bytta til mer fleksibelt og brukervennlig rammeverk for webutvikling.  
* Har skrevet endel avviksmeldinger i LaTex  
* Har begynt å skrive sluttrapport i LaTex, utkast til disposisjon; 1000 ord.  
* Har ikke begynt å skrive statusrapport 2  
* Har kontaktet en inkubator, og fått en kapabel og tillitsverdigkompis med på laget for videreutvikling, forretningsutvikling og senere kommersialisering  


## Sak 2: Hva skal gjøres?

- Lag lysbilde-presentasjon til statusrapport 2
- Lag statusrapport 2
- Lag straks utkast til disposisjon for sluttrapport, send pdf til Halldor
- Bruk opp gratispoeng på Roboflow for å lage et godt annotert og velbalansert datasett. Dersom modellen som bruker det datasettet ikke er treffsikker nok, send datasett m/ merkelapper til Halldor. Han har erfaring med å trene mange bildegjenkjenningsmodeller, og kan ta seg av videre trening. Bruker modell trent på annotert datasett til å gjøre aktiv trening på større, uannotert datasett.


**Tips**
- Ikke bruk for mye tid på én ting i programmeringa, hvis du blir stående fast.
- Vær var på at du har en bacheloroppgave som kan leveres, før du begynner på andre tidkrevende ting (ref. oppstartsbedrift)
- Avviksmeldinger, milepælmeldinger og endringsmeldinger trenger bare gjøres minimalt ut av. Hele karakteren baseres på **Sluttrapport** og **sluttpresentasjon**.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->