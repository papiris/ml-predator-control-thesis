# Statusmøte 1, bacheloroppgave UiT
### Dato: 15.03.2024
### Tid: Jeg var med 10:05 - 10:30

## Intro
Jeg har sittet med krummet nakke og stått på i arbeidet med prototypen, og har derfor ikke laget en statuspresentasjon for møtet. Tenkte heller jeg skulle vise en demonstrasjon av prototypen i aksjon.


## Spørsmål og svar
Q: Funker programvaren bra på datamaskiner med begrensede ressurser?  
A: Jeg utvikler og kjører programvaren på gaming-laptopen min. Den har et diskret grafikkort men er ikke et beist, og er noen år gammel nå. Når jeg får tak i en diskret maskinvareakselerator for kjøring av maskinlæringsmodeller, skal jeg prøve programvaren på en Raspberry Pi 4 jeg har liggende klar.  
Maskinvare bør ikke være en absolutt begrensende faktor, da kraftigere maskinvare kun vil redusere tida brukt på analyse og dekoding av videostrømmer, og dermed redusere responstida ved hendelser. 

Q: Hvordan går det med prosjekt-dokumentasjonen, selve sluttrapporten? Anbefaler alltid sterkt å skrive litt på den kontinuerlig underveis.  
A: Jeg har lastet ned [LaTex-malen for oppgaver ved UiT](https://github.com/egraff/uit-thesis) og sett litt på den, men har ikke begynt å skrive på den enda.


## Til neste statusmøte
Tar mål av å ha utført disse sakene:  
1. Skrive 2000 ord i sluttrapporten.  
2. Implementere push-varsler om gjenkjenning av gaupe, som åpner lenke til nettside.  
3. Trene ferdig maskinlæringsmodell.  
4. Kjøpe og ta i bruk maskinvareakselerator for maskinlæringsmodeller.  


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
