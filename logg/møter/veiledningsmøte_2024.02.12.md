<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->

# Veiledningsmøte 1, bacheloroppgave UiT
### Dato: 12.02.2024
### Tid: 10:00 - 10:25
### Deltakere: Halldor Arnason, Jacob Ludvigsen
---
## Sak 1: Hva er status?
Jeg har falt etter, fordi jeg har brukt mye tid på svært detaljert litteraturstudie. Brukt flere dager på å finne tallgrunnlag og beregne karbonavtrykket av tap av sau til rovvilt, kontra å trene og bruke maskinlæringsmodell for å hindre tapene.  
Har også brukt en god del dager på å studere mange relevante standarder for sikker programvareutvikling og relevant lovverk. Har snakket med min studiepartner om det; og hun sier de aldri har vært borti hverken de standardene eller det lovverket i studiet. Hun går integrert master i cybersikkerhet ved UiT, og gjør masteroppgaven dette semesteret.

Jeg har skrevet fem kapitler i litteraturstudiet, og har fire kapitler igjen.  
Er bra sikker på at jeg har gått for dypt i litteraturstudiet.


## Sak 2: Hva skal gjøres?
Reduser ambisjonene; gjør det raskt og simpelt.  
Jeg blir ikke vurdert på om prosjektet resulterer i et helhetlig produkt som kan gjøre alt, men på systemet, konseptet og rapporten.  
Oppgaven er ikke å gjøre en grundig kost/nytte analyse av bruk av maskinlæring for å unngå tap av husdyr til rovvilt, _men å lage systemet som gjør det mulig._
- Skriv i litteraturstudien at det er 8 objektklasser som er interessante, men grunnet tidsbegrensninger forholder jeg meg kun til tre av dem. Sau/lam, et av rovviltene som tar flest av dem, og menneske.  
- Skriv kort og overordnet i litteraturstudiens resterende kapitler. Det er ikke der jeg skal legge hovedinnsatsen.
- Skriv ett til to korte paragrafer om to til tre ulike maskinlæringsmodeller.
- Skriv ett til to korte paragrafer om to til tre ulike programvarebiblioteker for maskinlæringsmodeller.
- Skriv ett til to korte paragrafer om to til tre ulike rammeverk for maskinlæringsmodeller.
- Skriv ett til to korte paragrafer om eksisterende løsninger.
- Ha et møte med kunden om hvilke krav/ønsker dem har, for å kunne forhindre tap av sau til rovvilt.
- Skriv konstruksjonsgrunnlag basert på det.
- Jeg trenger ikke gjøre systemet perfekt, jeg kan bruke enkle løsninger til å begynne med for å "få et produkt ut døra", og så kan jeg forbedre det om jeg får tid.
- Maskinvare for trening av modellen kommer til å ordne seg, UiT og veileder har tilgang.
- Neste veiledningsmøte blir 4. mars

