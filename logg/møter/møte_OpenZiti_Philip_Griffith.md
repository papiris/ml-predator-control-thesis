# Briefing / bootstrapping OpenZiti
#### Participants: Philip Griffith of Openziti/NetFoundry and Jacob Ludvigsen
#### Date: 27.02.2024
#### Time: 10:00 - 10:45 UTC

## Questions
1. I do my project in a radically transparent way, continuously releasing all project documents under an open license in a public git repo. To better document progress and basis of my decisions. Would it be alright that if I also applied that to notes from this meeting?
- Sure

2. May I record the sound from this meeting, strictly for my personal use?
- sure 

## Transcription

Phillip: So, yeah, really cool use case. Um, there's been a few comments in the discourse, and I provide some input. Um... So I guess the most pertinent question is, how best can I help you today? Are there any specific concepts or areas you're like, oh, I'd love to kind of understand this a bit more, uh, or specific questions that you have? 

Jacob: I think what I would really need is. Kind of some guidance in how to actually get this up and running in more of a distributed fashion. Because like the quick start guides and such are really good for just getting something small up and testing it, like with pinging to and from such. But how would I connect... Routers from outside the cluster to the controller in the cluster, for example. And it feels a little diffuse to me of how much of the step-by-step quick start guide can be used for a permanent deployment. 

P: Yeah. OK, so let's start at 50,000 feet. I'll skip over marketing. So Ziti has three main areas. There is the edge. These are the points of ingress and egress that consume strong identity and build private encrypted connections from edge to edge. SDKs and it's the tunnelers. You then have the fabric, which consists of the control plane and the data plane. So you have, and I'll get more into that later, the data plane. And then you have the orchestration engine. In OpenZiti, we call it ZAC. In CloudZiti, we call it Mop. So it's Ziti administration console or the management and orchestration platform. Effectively, it's where you put your policy. It's where you manage integrations and you visualize stuff, you monitor things, et cetera, et cetera. Three components which exist in the ziti overlay be it edge or fabric has a strong identity and we do that because what several reasons: A we want to ensure that we don't depend upon weak network identifiers. B it enables us to build a secure private system and C it allows us to abstract away a lot of complexity now pki is complicated but again Ziti tries to abstract away as much of that as possible.

If you generate a component which is going to to exist on the overlay assuming you're using the ziti pki and not a third-party one which you can do effectively every single component needs to go through a process called bootstrapping trust this is a a high-level process representation of that taking place there's a five-part blog series that goes way deep onto this if you really want to dig into it it but effectively it's, I want to create a router or an endpoint. I have to give it an identity. So I out of band create a JOT or JWT. That gets given to the endpoint, which generates a public-private key pair on the underlying host operating system with the private key being always stored locally. The endpoint then communicates back to the controller. They validate each other's public keys to ensure the controller trusts the SDK or the endpoint and the SDK could trust the controller. Once you have finished this, the endpoint is bootstrapped into the overlay. It exists, but it still has access to nothing nor any policy. It is only when you configure the policy or the service in the console and associate it to endpoints that that configuration is complete. The configuration gets pushed down and therefore packets can be deleted or terminated. That gets you to this kind of point, which was the overview which I shared with you, which is we have the SDKs and the tunnellers which are at source and destination. So in your use case, there is the endpoints which are in this . So in your use case we have the cameras and those cameras are, if I remember correctly, I think connecting to a centralized cloud or third parties which have the endpoints on the user side.

J: The camera should only be able to connect to a box that is put within the user's local network and then that box then connects or makes services available to the user outside of the network. The cameras connect to the box, and the box makes the UI for viewing the cameras available to the users with the proper authentication outside.

P: And the box is hosted in the customer premise or they're hosted centrally on a cloud?

J: I want to avoid any kind of personal information processing, so it's all hosted with the customer.

P: Cool. So effectively, therefore, we have the endpoints in wherever the farms are with the cameras. And that is pushing the data and maybe pulling data from the customer environment where they're hosting the box. In the middle, we have at least one controller. HA controllers is coming really soon. So you can have resiliency with that. And then you have at least one edge router. And as you read more, as I explained in the discourse, you get smart routing, mesh networking and greater resiliency and redundancy. So to immediately shoot one of your questions, don't use a quick start. Because the quick starts are very much designed for getting started. 

J: So... I've tried the guide for getting up in kubernetes but  it feels like something's not quite right with it. Because I'm using K3S and it packages Traefik by default. And I haven't really found like the proper way to apply Nginx so that I could use the hosting guides.

P: And have you followed this flow?

J: I have, but I'm not quite sure. I don't quite understand it.

P: Got it so because there's two two two important concepts here there is how am i hosting the fabric and how am i deploying my points of ingress and egress and e for hosting openziti that's where we have um various different guides in terms of deploying those components onto the instance because ken who is our resident kubernetes expert he built a canary stack canary stack. Where is it? Ah, maybe it's got the link here. We've got a Canary site. I feel like it did it on K3S. Maybe I'm just making that up. There it is. I think it's this one. Is it K3S? K9. 

J: Haven't heard of that before.

P: So it's basically a, and to be honest, the docs aren't up to date, but it's a Terraform module for deploying, if I remember correctly, by default on DigitalOcean, but you can change it, which uses a Terraform package to pre-deploy the Ziti components, but then also has the tools in order to do monitoring and other capabilities. Now, again, it doesn't say K3S here, but I could have sworn I saw Ken once mention that was on K3S. So at the very least, check through there or doing a post into discourse around it and deploying on K3S, you'll probably get a response from Ken. I think Clint's pretty knowledgeable on Kubernetes, but Ken's in Clint's team and is really the expert on Kubernetes. That is how you deploy the components. This section then gets to, how do you want to expose your services? Because different endpoints give different outcomes, whether you want to work on cluster level or pod level, or in fact, you mentioned NGINX, if you're using NGINX, you could even, we built a C module so that you can effectively deploy Ziti on top of, or as part of the NGINX server.  So that C module sits on the NGINX server so that you can expose the services behind it. You can expose the services behind it to Ziti effectively sitting on NGINX rather than having to deploy any, additional tunnellers. Or edge routers into that environment. So let's say a customer has a box. And I'd like to have that box running Kubernetes to ease of deployment and maintenance with Nginx and surveillance software. So have the Ziti module as a sub charge, I guess, of Nginx ingress. And it just then gains control of or gains access to all the services that Nginx has the ingress for. Let's rephrase. So we have cameras over here. And we've got the... The customer box. Over here and you're not using NGINX for this connection are you?

J: I haven't gotten that far yet because I have deployed a couple different camera surveillance softwares to try to figure out which one I want to move forward with but they mostly use, well, they're not, these boxes aren't multi-purpose, they're just meant for a single purpose. So if I'm just running a single service on it. I'm not. I'm not sure if I need an ingress.

P: Are we talking about the camera or?

J: Yeah, on the camera.

P: Agreed, agreed. So what we require over here on the camera is just a Ziti tunnel to provide point of ingress and egress. We don't need a layer 7 gateway or API gateway or anything like that. On the server side, I also don't think we need that. We don't need to protect from the internet like an API gateway is going to do because there's no ingress from the internet. It's completely blocked. The only connections which are allowed are the authenticated and authorized client connections. So then maybe we need NGINX between the user and the person accessing the service. And this gets into the interesting question of can I apply zero trust private connections to that? Have you come across browzer at all? 

J: Yeah, I've had a look into it and Clint advised me to try to get that up and running as a final step. And maybe not start with that.

P: Agreed. If you have a tunneler here and a tunneler in the environment that you're hosting, the box and the NVR and all of that stuff, you then have users potentially just using browzer. And that effectively means that we're injecting an endpoint into their browser's tab so that they can have mutuality of their center encryption, from their laptop to this server without them having to load the Ziti SDK. And so to them, it seems like a public SaaS application, but it's not. It's a private server, which is only accessible. Now, do you need an API gateway? Probably not. Could you use a layer 7 WAF? Maybe, maybe not. What's it gonna protect against? Cross site scripting attack? Well, we've built some defenses into browzers you can't do cross site scripting. SQL injection? Maybe, but if it's not the whole internet that can find your application in the first place, what is the risk of an SQL injection if you make sure the database is secure, at least as you would expect it? So maybe you don't need the API gateway.

J: Okay, so... But I've seen in the docs as well as in the forums that. In order to get that little, or well, not get the warning that this is a self-signed certificate, presenting a second certificate provided by Acme would be a nice way of solving that user experience.

P: The reason browser will solve that problem for you is because browser is not acting as your identity provider. It is interoperating with an IDP. So let's say you're using Google as the identity provider. What the user is doing is they're coming to effectively a... bootstrap. Let's just call it the Bootstrapper.

J: Is it like a... login page? 

P: It's just a login page. That login page purely has a connection to the IDP so that the user is effectively accessing the IDP via the login page. And therefore, when they authenticate to the IDP, the IDP returns the X.509, which gets put into their browser. And then the browser Bootstrapper goes, yep, controller, this person's authenticated, so provide me the endpoint. And that, therefore, it loads the browser runtime and the service worker endpoints into the user's tab running in memory for that tab so that they have the identity associated to the IDP, but then they have the endpoint, this bit, effectively, so that they can now make outbound connections to the fabric. And the fabric... Outbound to the server. So now you've got that connection between these two components. 

J: All right. That makes things a whole lot easier. 

P: Yeah, 100%. And between these connections, you don't care there's a self-signed certificate because no one's using the browser to go, oh, this isn't secure. 

J: Yeah. Also, these various surveillance softwares have. Varying qualities, I guess you could say, and either deploy native mobile applications or progressive web apps. So in the case of native applications, they could just install the Ziti Tunneler app.

P: You could either install the Ziti Tunneler app or you could go one step further and actually build the Android or Swift SDKs directly into their application and now it's just invisible inside the application on the mobile phone.

J: Okay, yeah. Yeah, that's definitely an option. Absolutely. Hmm. And I guess the first easiest part there would be to use the tunneler application. And since it intercepts traffic that's destined for Ziti before reaching anything, they. If the mobile application for surveillance has a TLS certificate checking ability, it still won't cause any warnings?

P: No.

J: Because Ziti provides the necessary certificate to convince the application? Do I understand that correctly?

P: Well, the application isn't aware of Ziti. So from an encryption perspective, the application is just doing its happy little job encrypting in the green from the client to the server, server, server, whatever. Ziti is on the outside of that. So the application is not aware. 

J: So, but... I'm still a bit diffuse on the certificates part for the native applications.  It's not completely clear to me yet how does the certificate from Ziti not come up as a self-signed certificate for the app? Or does it come as an unencrypted connection for the app? 

P: So Ziti is providing an encrypted connection from endpoint to endpoint, which if you're using a tunneler or an edge router, the application is not aware of at all. Like, for example, this is my laptop. This is me sharing your comments earlier into our Dev channel. This is me searching for the Canary thing. This is a web application which is hosted in the cloud somewhere. It has absolutely no idea that I'm using Ziti, but I am because I have an identity, and someone has pushed the policy that if I enter a certain IP address or URL into my browser, I get redirected to the server. The client on the server has no idea this is happening in the host operating system, just like using a VPN. It's a redirect to the network. It's only if you're embedding inside the application that the application has any concept of the zero-trust network. 

J: Mm-hmm. So the application doesn't have any concept that the communication is encrypted either?

P: Correct. No idea. Because we are effectively capturing the packets and repacketizing them to send over our overlay. 

J: Okay, I think I get it now. 

P: Net result, yeah, we can effectively intercept any packets from, let's say the app is here and the other app is here. The packets are just coming in. We're intercepting them in the host OS if we're running as a tunneler. And this can be on an arbitrary IP or URL. And then we're popping them out on this side. And we can, same IP and URL or DNS, or we can change it to whatever we want, to however we want the service to terminate. And the receiving end to the application, the application just thinks they're in the same private network happily communicating. They have no idea that we're effectively tying together these two completely private networks. 

J: Okay, okay. So it's like they're in the same LAN. 

P: Yes, correct. So this yeah again this could be 192 dash whatever and this can be 192 dash whatever dot one and they're like yeah cool we can just communicate 

J: okay yeah I did read your blog post and like the teleportation metaphor exactly and this like it didn't quite click then but this explains it. 

P: yeah the problem of using analogies to simplify is that you simplify the news details 

J: yeah I mean different different strokes for different blokes there are more there are billions of people in the world and billions of ways to explain things to you uh regarding like the context of the people listening and stuff.

P: Yeah, for sure. 

J: I see NetFoundry offers a hosted managed city. I was wondering, is it difficult to migrate between them, between the hosted and self-hosted version? 

P: It's a good question. So today, it's more difficult than it should be. But we are quite literally and have been for the last few months working on updating the Cloud Ziti API so that, if we go back to this picture, how best to draw this. So updating at the moment so that when you're talking to the Cloud Ziti API, you're effectively talking to the exact same OpenZiti API. What you then have is then the second function of the specific Cloud Ziti functions of management, of monitoring, of building the infrastructure onto AWS, Azure, Google, whatever. like all the stuff that we're doing to provide our SaaS. So effectively, this stays the pure CloudZiti API. And for all of the other OpenZiti functions, you're basically just using the OpenZiti API. But you don't have a CloudZT API, you just have a transparent shim on top of it. Today, this is all one homogenous API. So there's ways which you can extract and migrate without too much issue. But once we complete this work, it's going to be way easier. Because you'll just be doing the exact same calls to the APIs.

J: Hmm, like transferring tunnels, tunnelers and apps and such?

P: Oh, they'll migrate immediately today. Yes, you just, if you're changing the controller, then you'd need to re-enroll it with a new identity. And you could extract the policies via a YAML file or something and then put those into ZAC. Again, there's parts which can migrate across quite seamlessly. Once we've completed this work, then it's very possible that we might have a function in the future where we say, you know, we can take your controller and migrate it into a Cloud Ziti deployment. But we haven't even started the work on that yet.

J: That'd be super neat.

P: It would be, yes. 

J: Easily scalable with, like for a small project. Because I'm doing this project as both... my bachelor's. Thesis in mechanical engineering, if you could believe it. And, um, uh, and also building, uh, small business out of it. So being able to, uh, like scale with. What kind of expertise I can get a hold of or build myself for time. And just months in the winter months when people don't really have a lot of animals attacking their livestock, I could just. Scale down and host it myself and in the summer months I could move it on to NetFoundry's SaaS.

P: That is conceivable yeah there's many ways you cut and slice that in fact on that note with your question around very quickly seeing a demonstration capability we have a completely free tier so you can come into the equivalent of basically go to NF console but the one that I cool I'm already logged in. It's me to go create me a network there's deploy it and then I create me a new a new edge router it's gonna be my it's gonna be my fabric or actually it's gonna be my edge so you could deploy this in minutes and get a demonstration system working. 

J: OK. Because I want to try to see how various surveillance softwares work with different parts like browser and such. I guess there isn't really a way to figure out how to wrench these things together without first deploying a network. Because I see it's... Well, so... Because it's 14 days of trial period, and I'm not confident I can get things up and going in that period. 

P: I mean, yeah, so once you've created your network, you can extract the policies. So even if it got spun down, you can create a new one and just re-upload the policies and it'll pre-populate all that stuff for you. But what we do have... I'm just trying to remember the bloody name. Let's go to here because I need to talk about it. Ah, the Developer Sandbox, that's the one. Ziti developer. Yeah. So you could use Zeds, although Zeds is focused on embedding Ziti into your application using the SDKs. So it abstracts away all of the stuff around policies and hosting the infrastructure. It's basically doing all that on a multi-tenant environment. So you say, this is my app endpoint. I've embedded the SDK and it's like, boom, it's connected and it works. You have to roll the identities, obviously, but everything else is abstracted away.

J: Yeah, I'm interested in using the SDKs as well because I've started doing, what's it called? I've started opening issues in various roles for the surveillance softwares to discuss ZT. Well, there aren't any surveillance softwares. Offerings, at least not open source, that integrate zero trust principles. So just trying to get the ball rolling to see, well, are you all interested in having some contributions? 

P: As you do that, here's an interesting idea for you. There's a lot of concern, particularly if you go to the US, that if I buy a camera from a Chinese manufacturer, these manufacturers could steal my video streams, steal my data, etc. If you embed Zero Trust inside your application and run it on the camera, you don't give a flying monkeys whether they've even put a backdoor in the camera. Because you can force it to only communicate to the overlay network controller and routers. So even if someone has backdoored the hardware, they're not going to be able to communicate to China. They can only communicate to the Zero Trust overlay. 

J: Mm-hmm yeah that was actually part of the reason I started this project because my dad who runs the family farm got four Chinese cameras which cost a lot but are somehow free to operate and they have no privacy policy whatsoever and I know no doubt they're analyzing and watching the people and stuff like that.

P: yeah and probably exposing all of the local networks as well yeah.

J: But how would, without, like I made a comment regarding that in the discourse, how would we ensure that, or how could we have zero trust from the camera? Do we need to trust the local network?

P: If you're using tunnelers, yes, you need to trust. If you're using tunnelers on the camera, you're trusting the camera host OS network. If you can put the SDK inside software, which is running on the camera, then you're not even trusting the camera OS network, because you're doing a private encrypted connection directly in memory of the application itself.

J: Yeah, I was playing around a bit with the idea of, you know, flashing and just a retransmission with  an SDK onto an ESP 32 and just shimming it, intercepting the Wi-Fi or radio,  you know the packets on the camera. But that would be a bit in the future. 

Got a question with regards to, because I'm doing this as a thesis, I also want to keep it radically transparent. So I've got a Git repo that I shared in the comment in this course where I continuously publish the documents I produce and the work logs and the meeting logs and such. So I was wondering if it would be okay if I, when I write notes from this meeting that I also publish those.

P: Super. Yeah, and taking a step back, our objective of Ziti is to turn it into the equivalent of Linux, but for secure distributed systems on the networking side. And so our objective is to get as many people as possible seeing how transformational it is versus better VPNs like Nebula or WireGuard. So you can truly build secure by default systems from the perspective of the system builders rather than giving it as a problem to the operators. And therefore, we... Invest a lot of time into the community, getting them having value from it. And then likewise, looking to have them say, hey, look at this awesome thing I built, because then that helps build more people that come and find it.

J: Yeah, you guys are amazingly respondent and responsible for your community. It's honestly impressive to see. And once I get this stuff up and running, and I get a hang and a feel for how the docs could be improved further, maybe updated and stuff, and other ways I could contribute to upstream, then I am very happy to do so. 

P: Brilliant. That's always appreciated. Awesome. Jacob, do you have any other questions for today?

J: No, I feel like I've got a pretty good leg to stand on now. 

P: Awesome. And if you have any other follow-up questions, yeah, shoot them into Discourse Public is probably the best first place because then you'll get way more technical people than me giving some thoughts and feedback as well. 

J: Okay, yeah. And if I have feedback with regards to the message I sent about the docs and the deprecated message, just put that public as well? 

P: Well, basically, DMs is fine unless you feel like it's a comment that other people would benefit from. If it's a comment that other people would benefit from, then we want it to go into the public. 

J: That is that's something I definitely vibe with. 

P: awesome Mmm, Jacob. I hope you have a great day then and good luck for the project. 

J: Thank you. Likewise. Thanks so much for meeting with me and sharing your knowledge.

P: You're welcome.

## Links

[Ziti Terraform](https://github.com/openziti-terraform-modules/terraform-lke-ziti#readme)

[Ziti nginx](https://blog.openziti.io/nginx-zerotrust-api-security)

[Ziti Edge Developer Sandbox guide](https://support.netfoundry.io/hc/en-us/articles/7539754761613-ZEDS-Ziti-Edge-Developer-Sandbox-User-Guide)


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->