# Veiledningsmøte 2, bacheloroppgave UiT
### Dato: 04.03.2024
### Tid: 10:15 - 11:10
### Deltakere: Halldor Arnason, Jacob Ludvigsen
---
## Sak 1: Hva er status?
Jeg har brukt mye tid på å se på nettverksløsninger for å gi enheter utenfor det lokale nettverket tilgang til brukergrensesnittet til kameraprogramvarene. Den eneste nettverksløsningen som møter kravene til pålitelighet, skalerbarhet, sikkerhet og brukervennlighet er OpenZiti.  
OpenZiti er dog vanskelig å sette opp selv, derfor skal jeg benytte meg av NetFoundry sitt "managed CloudZiti" tilbud, som er gratis inntil 100 endepunkter. Jeg kan siden eksportere innstillingene som funker til en selv-oppført instans av OpenZiti.

Fordi jeg ikke vet hva jeg ikke vet om de ulike kameraprogramvarene, har jeg prøvd dem alle i ulike situasjoner. Dette har hjulpet meg å finne begrensninger i den enkelte programvarepakken, som påvirker oppnåelsesgraden deres i vektingsmatrisen.



## Sak 2: Hva skal gjøres?
Lag presentasjon til statusmøte 

For varsling, finnes det enkle måter å sende et bilde og varsel per epost. Gjør det dumt og enkelt.

Til statusmøte neste uke må én kameraprogramvare være oppe og gå, og fungere.  
Fullt fokus fremover på kameraprogramvare og objektgjenkjenning. Nettverksløsning bør jeg sette på pause inntil videre.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->