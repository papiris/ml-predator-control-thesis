<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->

# Kundemøte Stig Strandmoa, Strandmoa Gård
### Dato: 15.02.2024
### Tid: 10:30 - 11:00
### Sted: Strandmoa Gård, Steigen



Strandmoa Gård har sporadisk besøk av gaupe i sauegjerdet. Mest på vinteren, men også på barmark. Har ikke lagt merke til tap på beite på sommeren.

Gaupa kommer både på natta og i skumringslys. I mørketida kommer den døgnet rundt.

Det har tidligere vært en del tap til rev i lammetida, men tapet er redusert de siste årene. Kanskje pga. andre typer gjerder.

Det er å foretrekke at gaupa ikke kommer tilbake, slik at gårdbrukeren slipper å bekymre seg. Eneste måten å sikre det, er å fjerne gaupa. Sekundært, er det ønskelig å oppdage, og drive bort gaupa før den kan ta sau. 
Han innser at sauene på gården er et lettere bytte for gaupa på vinteren enn på andre gårder, siden sauene på denne gården går utendørs hele året. 

For et varslingssystem er det absolutt beste å få push-varsel på telefon. Mulighet for å lagre videoopptak er en nødvendighet, fordi det har vist seg nyttig i dokumenteringsøyemed.
Det er bra å kunne styre kameraet manuelt, fra samme program på mobilen som gir varsel. 
Det er ikke viktig at programmet kan brukes fra PC, men det kan være nyttig.

Gjerne statistikk på antall deteksjoner, Falske og Sanne. At programmet kommer med varsel "Vi tror dette er en gaupe! Er det det?", og viser knapper for å bekrefte eller avkrefte det. Ønsker gjerne at gjenkjenninga "lærer underveis", og blir bedre på å gjenkjenne dyr på gården. Det er svært interessant å kunne sende slike bekreftet/avkreftede bilder til sentralen; for at sentralen (jeg) skal kunne trene neste iterasjon av modellen på bidrag fra alle gårdene, og deretter sende dem en oppdatert modell.

RE: personvern. Det sier seg selv at det ikke er nødvendig at andre skal kunne ha tilgang på videoene eller bildene.

Vi har flere gårder, med flere kameraer. Det er ønskelig å kunne ha kameraer fra flere fysiske lokasjoner, på helt andre nett, i samme app / brukergrensesnitt.

Størrelsen på boksen systemet kommer i har ingen betydning, ei heller om det er én eller to komponenter.

