# Veiledningsmøte 4, bacheloroppgave UiT
### Dato: 24.05.2024
### Tid: 15:00 - 15:50
### Deltakere: Halldor Arnason, Jacob Ludvigsen

---

## Sak 1: Tilbakemelding
Veldig bra skriving.

Trenger bedre overganger mellom temaer
forklare mer om "zero trust application access"

Knytt problemstillingene i "bakgrunn" mer sammen.

1.1 Problemet er tap av får, IoT system med KI kan være løsning, iot har problemer med datasikkerhet

Samme på litteraturstudie, avslutt tap med overgang, og start Datasikkerhet med mykere overgang

resultatmål ser bra ut.

Pass på småfeil, bl.a. i henvisning til referanser

Legg til forkortelse for CO2e

På Eksisterende systemer, skriv "gjennom litteraturstudiet har det ikke blitt avdekket eksisterende systemer med disse funksjonene. Derfor lager jeg det selv"

bruk \ref, og \section{}\label, det er fint å se kanskje sidetall for det refererte?

Hvis tid: Figurer med tekst må ha større oppløsning, og helst større tekst. Absolutt pydeps-bildet

Plasser kapittel Verktøy heller som seksjon under kapittel System.

Ikke omtal annoteringsassistenten som kun "assistenten"

Trenger ikke detaljer om statusmøter. Trenger egentlig ikke nevne statusmøter, og ikke ha som vedlegg.

Gjenta resultatmålene i Resultat, slik som i kapittel 1. Fortell det jeg har oppnådd etter hvert mål.  
Gjør samme med Oppnåelse av kravspec

Diskutere mer om systemet, dets ytelse. Fortell om at begrenset tid gjorde at dette og dette ikke ble gjort. Med mer tid, ville jeg kanskje gjort det på dette og dette viset.

Konklusjon bør bli lengre og mer altomfattende. Skriv mindre punktvis?

Videre arbeid trenger ikke være egen seksjon, omtal videre arbeid som integrert del av diskusjon

Dropp arbeidsøktlogger fra vedlegg, ha det heller som separat dokument og lever dem som separat dokument. Når de skal refereres i teksten gjør (se arbeidsøktlogg, separat dokument)  

<!-- avviksmeldinger og endringsmeldinger, møtereferater får samme, men går i samme dokument. -->



## Sak 2: Hva skal gjøres?
Skriv mer på IoT-delen i sammendraget



## Sak 3: Egne notater

Jeg brukte samme Linux-baserte operativsystem som tidligere digitale møter, men hadde ingen problemer denne gangen. Brukte USB-mikrofon, ingen endringer av EQ, og den tredjeparts-appen Teams for Linux, som bruker Electron (Chromium).

<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->