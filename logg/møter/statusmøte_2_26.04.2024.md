# Statusmøte 2
#### Dato: 26.04.2024
#### Tid: 09:20 - 11:00
#### Sted: Microsoft Teams

## Tilbakemelding presentasjon:

Litt mindre fokus på MVP, ha mer fokus på rapporten i starten.
Ta tida, ha en timer foran deg når du presenterer. Brukte lang tid.
Øv på forhånd!
Presentasjonen var ellers ganske god, gjerne mer "Tekstfokusering".

## Til meg:
Bruk Chrome om jeg er på Linux, med "del hele skjermen". Husk å start xwaylandvideobridge!
Fokusér på rapportskriving fremover.
Kan trene maskinlæringsmodell underveis i rapportskriving.
Ta opp igjen utvikling av programvare midt i mai.


## Tilbakemelding Statusrapport

Grundig rapport, god tekstflyt.

Husk topptekster. (Hvilke topptekster?)

Sammendraget er megaviktig.

Dokumentér hvorvidt og hvordan betingelser (HMS, prosjektstyring) er fulgt.


## Råd til Sluttrapport

Sluttrapporten legges ikke på en brevvekt for å måle hvor tung den er. Innholdet er det viktigste.
Den skal være "en ingeniør verdig".
Bruk vurderingsmal for sluttrapport som legges ut på Canvas, som ei smørbrødliste over hva som **bør** være dekket.
Les Alexander's sluttrapport på Canvas.
