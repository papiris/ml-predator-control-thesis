
# Litteraturstudie

## Innledning 
Denne litteraturstudien tar for seg fundamental kunnskap som må til for å kunne utforme konstruksjonsgrunnlaget i bacheloroppgaven "Maskinlæring i rovviltforvaltning og landbruk".

---

## Innholdsfortegnelse
1. Eksisterende løsninger innenfor samme felt
2. Programmeringsspråk
3. Rammeverk for brukergrensesnitt
4. Relevante standarder
5. Miljøbelastning
6. Programvarebiblioteker
7. Maskinlæringsmodeller
8. Treningmateriale for maskinlæring

---

## 1. Eksisterende løsninger innenfor samme felt
Det finnes flere ulike verktøy og programvarer som oppfyller enkelte av målene til prosjektet.  

### Kameraovervåkning med objektgjenkjenning
Innen bruk av kameraovervåking og objektgjenkjenning for å varsle brukeren om en brukerdefinert hendelse; kan nevnes Zoneminder, MotionPlus, FOSOPAS, Viseron, Frigate og Furbinator 3000.  

<br>

#### Zoneminder
Antageligvis den mest velutprøvde og "modne" fri programvaren for kameraovervåkning og objektgjenkjenning. Har et stort og aktivt utviklersamfunn, og etablerte sikkerhetspraksiser. Det innebygde biblioteket deres av støttede kameraer er enormt. Zoneminder støtter konteinerisering. Dokumentasjonen er utdatert, og installasjon litt vanskelig.  
Lisensiert under GPL-2.0-or-later. Dette gjør det mulig å endre lisens til AGPL-v3-or-later, men det ser ikke ut til å ha vært diskusjon om det før nå. Mulig å endre lisens på en avledning, men da mister man muligens det originale utviklersamfunnet.  
Zoneminder-prosjektet vedlikeholdes hovedsaklig av en virksomhet, som tilbyr betalt Zoneminder-support og utfører kontraktarbeid på Zoneminder.

Skrevet i PHP, Perl, C++ og javascript, hvorav jeg ikke har erfaring med noen av dem. Zoneminder har API med standard protokoller (Websocket, MQTT, perl), bl.a. for kjøring av eksterne skripter ved registrerte hendelser eller alarmer.
Det er innebygd en maskinlæringsmodul, med mulighet for å bruke ulike modeller.  
Har tilhørende app, som fungerer på Android, IOS, Windows, Mac og Linux; og er lisensiert GPL-v3. 

Mobilapp: https://zmninja.zoneminder.com/  
Hjemmeside: https://zoneminder.com/  
Maskinlærings-modul: https://github.com/ZoneMinder/zmeventnotification  

<br>

#### Shinobi
Relativt ny på banen, startet i 2016. Prosjektet kaller seg villedende for "Open Source" selv om lisensen er på den utgaven av prosjektet som ennå oppdateres er proprietær, kilde-tilgjengelig. ShinobiCE er lisensiert AGPVL-v3 og kan kjøres lokalt uten å betale lisens, men har ikke mottatt oppdateringer på to år; og kan derfor anses som dødt.  
Man kan ikke kjøre Shinobi uten å betale for en lisens, og knytte seg til selskapets sentral. 
Den minste lisensen koster $6/mnd, og gir mulighet til å tilkoble 5 kameraer og 1 mobilapp. Lisenskostnaden er progressiv, og koster opptil $600/mnd for 1000 kameraer.  


Avansert funksjonalitet som kryptert fjerntilgang uten personlig VPN er bak betalingsmur. Shinobi har et selskap i førersetet, som tilbyr betalt support og kontraktarbeid.  
Skrevet i Nodejs, som jeg ikke har erfaring med. Har API-er og innebygget alarmfunksjonalitet.

Enterprise-versjon: https://gitlab.com/Shinobi-Systems/Shinobi  
Community-versjon: https://gitlab.com/Shinobi-Systems/ShinobiCE

<br>

#### Frigate
Har bruk i konteiner og kubernetes; samt maskinlæring og integrering med Home-Assistant og MQTT som sine hovedfokus. Støtter Coral ML-akselerator m/ tensorflow lite og Nvidia TensorRT maskinvareakselerasjon for YOLO modeller. Støtter avlasting av maskinlærings objektgjenkjenning over nettverk via åpen-kildekode prosjektene Deepstack og Codeproject.ai. Bruker WebRTC, som lar det fordøye videostrøm fra nettverkskameraer direkte uten dekoding og re-enkoding.

Lisensiert under MIT, har en virksomhet i førersetet. Virksomheten tilbyr egen datasett-annoterings + modelltreningstjeneste som koster $50 per år: https://frigate.video/plus/

repo: https://github.com/blakeblackshear/frigate  
dokumentasjon: https://docs.frigate.video

<br>

#### Moonfire NVR
Et ungt prosjekt som ikke har publisert 1.0 enda, selv om tidligste commit var i 2016. Lite utviklet, men under utvikling. Lisensiert under GPL-v3 og skrevet i Rust og Typescript; hvor jeg har erfaring med, og liker, Rust. Prosjektet er lite, men langt større enn Furbinator 3000 og FOSOPAS.

Skriver .h264 videostrømmer direkte til disk, uten omkoding. Dette gjør det svært effektivt og ressursgjerrig i bruk. Har ikke innebygget maskinlærings objektgjenkjenning, men det er planlagt. Jeg har erfaring med og preferanse for Rust og Python, så oppstrøms bidrag er en mulighet. Dokumentasjon er god for de få brukstilfellene og funksjonene som finnes per nå.  
Moonfire har ikke en app; og nettgrensesnittet kan kun brukes til visning, ikke styring av kameraer eller annet. Sikkerhet er ikke innebygd, man må enten implementere https-støtte i den inkluderte webserveren, bytte ut inkludert webserver, eller proxy trafikk til en webserver som støtter https. Det siste, pluss innlogging og tofaktor-autentisering, kan oppnås med Traefik.

Siden Moonfire ikke støtter objektgjenkjenning per nå, støtter den naturlig nok ikke mer avanserte funksjoner som akselerasjon eller avlasting av objektgjenkjenning.  
Prosjektet støtter konteinerisering.

repo: https://github.com/scottlamb/moonfire-nvr

<br>

#### Furbinator 3000
Et prosjekt som er svært likt hva FOSOPAS prøvde å være, bare litt mer komplekst å sette opp. Prosjektet er et kombinert programvare- og maskinvare-prosjekt; med programvare som gjenkjenner skadedyr fra overvåkningskamera og aktiverer maskinvare som skal skremme skadedyrene bort ved bruk av lyd og lys.

Prosjektet mangler lisens inntil videre, og grunnet kopibeskyttelseslovgivning regnes det da som proprietært. Jeg åpnet en tråd om AGPL-v3 kompatibel lisens på repoet, men prosjektet virker forlatt siden det kun har én enkelt commit, i september 2023 [[1]].  
Det har ingen support, ingen utviklersamfunn rundt, det er en simpel sak med helmanuelt oppsett. Det fokuserer kun på integrasjon med Amazon Ring dørklokke-kamera, og har ingen integrert støtte for andre kameraer. Støtter ikke Coral ML-akselerator m/ tensorflow lite.

Bloggpost: https://medium.com/@james.milward/deterring-foxes-and-badgers-with-tensorflow-lite-python-raspberry-pi-ring-cameras-ultrasonic-75b3160faa3cs  
Hovedrepo: https://github.com/Jimbwlah/tensorflow-ring-animal-detector  
Arduinorepo: https://github.com/Jimbwlah/furbinator3000

<br>

#### Viseron
Prosjektet startet for 4 år siden, er lisensiert under MIT og skrevet i Python og Typescript. Det er basert på moduler, og har et stort bibliotek av tredjepartsmoduler tilgjengelig. Skryter av brukervennlighet, og støtter konteinerisert bruk aktivt.

Et knippe av tilgjengelige moduler: objektgjenkjenning med maskinlæring, kjøring av eksterne skripter ved hendelser, alarm, avlasting av objektgjenkjenning m/ maskinlæring, maskinvareakselerasjon av maskinlæringsmodell, MQTT.  
Hovedprosjektet har ingen commits siden november 2023, og ingen store commits siden juli 2023; men det er nylige commits i tredjepartsmodulene.

Repo: https://github.com/roflcoopter/viseron  
Hjemmeside: https://viseron.netlify.app/

<br>

#### MotionPlus / MotionEye
Gammel og moden applikasjon, som nylig er renovert. Lisensiert under GPL-v-3-or-later.   
Brukergrensesnitt er skrevet i Python og Javascript, og backend er C++. Prosjektet har integrert støtte for objektgjenkjenning m/ maskinlæringsmodeller, og full støtte for https og autentisert innlogging. Mulighet for å kjøre eksterne skripter ved hendelser/alarm.

Hjemmeside: https://motion-project.github.io/  
Repo backend: https://github.com/Motion-Project/motionplus  
Repo frontend: https://github.com/motioneye-project/motioneye

<br>

#### FOSOPAS
En meget begrenset og simpel programvare, utviklet av meg siden april 2023. Lisensiert under AGPL-v3, og skrevet i Python. Det er ingen innebygget webserver eller ekstern kommunikasjon, så alt er tilgjengelig kun lokalt på maskinen.  
Har innebygget objektgjenkjenning med maskinlæring, og alarmer lokalt på maskinen.
Ingen utviklersamfunn, support eller støtte for konteinerisering. Begrenset innebygget auto-oppdaging og konfigurering av ONVIF-støttede kameraer.
Ingen støtte for avlasting av objektgjenkjenning

Repo: https://gitlab.com/papiris/predator-detect-and-notify

<br>

---

### Sikker nettverkskommunikasjon mellom et definert utvalg enheter
Innen sikker nettverkstilgang mellom et definert utvalg enheter kan nevnes Tailscale, Headscale, Netbird, Veilid og Nebula.  
Enkelte tilbydere av kameraovervåkningsprogramvare tilbyr brukere en sentralisert konsoll for ekstern tilgang til sine enheter per abonnement. Dette prosjektet er det eneste funnet som har målsetning om å tilby kameraovervåkningsprogramvare med en integrert konsoll for ekstern tilgang.

<br>

#### Veilid
Utviklet av hackerkollektivet Cult of the Dead Cow og publisert sommeren 2023. Veilid er et ungt rammeverk for ende-til-ende kryptert, vert-til-vert, zero-trust kommunikasjon. Topologien er flat, og alle nettverkets deltakere bidrar med å videresende kryptert trafikk til riktig destinasjon. Alt er skrevet i Rust. Disse tingene gjør løsningen svært skalerbar og robust.

Veilid er ennå under utvikling, og har ikke implementert støtte for lagring og sending av datablokker eller datastrømmer enda. Derfor kan det ikke brukes i prosjektet, fordi vi trenger å sende video.

Nettside: https://veilid.com/  
Eksempel på toveis kommunikasjon over Veilid: https://github.com/stillonearth/veilid_duplex
repo: https://gitlab.com/veilid/veilid

<br>

#### Tailscale
Sentralisert overlay mesh VPN-nettverk, hvor sentralserveren kun administrer hvordan verter kan koble seg sammen med hverandre, uten at sentralen håndterer trafikken deres. Det er ikke fri programvare på tjenersiden.  
Tailscale har en app for Android og iOS. Jeg har erfaring med å bruke tailscale.

Det eksisterer en friprog implementasjon av tjenersiden som kalles headscale. Den er kun egnet for ett undernettverk, og får problemer ved mer enn 500 verter [[2]].

repo: https://github.com/juanfont/headscale

<br>

#### Nebula
Et vert-til-vert, ende-til-ende kryptert overlay mesh nettverk utviklet av Slack. Fri programvare tvers gjennom, og penetrasjonstestet av uavhengig sikkerhetsrevisor. Skrevet i GO.

Egner seg for å knytte alt fra et fåtall verter, svært mange enheter. Man må ha minst ett "fyrtårn" med offentlig tilgjengelig IP-adresse, som hjelper verter å finne hverandre. Det er visstnok svært skalerbart. Nebula er tilgjengelig på Android og iOS.

Bloggpost: https://medium.com/several-people-are-coding/introducing-nebula-the-open-source-global-overlay-network-from-slack-884110a5579  
Dokumentasjon: https://nebula.defined.net/docs/  
repo: https://github.com/slackhq/nebula  
repo mobilapp: https://github.com/DefinedNet/mobile_nebula

<br>

#### Netbird
Et sentralisert Overlay VPN-nettverk, ikke så ulikt Tailscale og Headscale

repo: https://github.com/netbirdio/netbird


----

## 2. Programmeringsspråk
Det er stor variasjon i hvilke programmeringsspråk ulike kameraovervåkingsløsninger er skrevet i.  
Zoneminder er hovedsaklig PHP og C++, Frigate er hovedskalig skrevet i Python og Typescript, FOSOPAS er ren Python, Moonfire er Rust og Typescript, Furbinator 3000 er Python og C, Viseron er Python og Typescript, MotionPlus er C++ og brukergrensesnittet er Python og Typescript.

**Python** var ifølge Statista verdens tredje mest brukte programmeringsspråk blant programvareutviklere i 2023, kun forbigått av HTML/CSS og Javascript; som nesten utelukkende brukes til webapplikasjoner og nettsider.
[[3]]. IEEE sin statistiske analyse i 2022 plasserte Python på førsteplass, tett etterfulgt av C-familien [[4]].

Python er et kraftig og fleksibelt programmeringsspråk, med en enkel og naturalspråklig syntaks; og et vidt spenn av nyttige programvarebiblioteker.  
Språket er spesielt populært blant data-analytikere, hobbyister, akademikere og på maskinlæringsfeltet. [[5]]

**Rust** er et moderne språk, med sikkerhet i høysetet. Kompilatoren tvinger deg til å skrive trygg kode, og gir fullstendig frihet fra enkelte (kritiske) klasser av kodelus. Økosystemet er ungt, og mangler implementasjoner av mange av Pythons nyttige programvarebibliotek.

**PHP** har sitt hovedfokus på nettsideutvikling. PHP står for Personal HomePage (tools)

**C-familien** er lite ergonomiske i bruk, men er veletablerte og produserer svært raske programmer. Har svært få innebygde sikkerhetsmekanismer, som gjør det enkelt å produsere kode med sikkerhetshull.

---

## 3. Rammeverk for brukergrensesnitt

Det eksisterer mange rammeverk for brukergrensesnitt, som fokuserer på ulike bruksområder, programmeringsspråk og plattformer.
GTK, QT og Iced fokuserer på bruk lokalt på datamaskiner og mobile enheter. Egui, Yew og Dioxus prøver å dekke alle bruksområder i Rust. React.js kan brukes til alt i Javascript.  
De har alle sine bruksområder og valg mellom dem og mellom alle de andre rammeverkene avhenger av avveininger og personlige ønsker.

Viktige faktorer i valget er kodeergonomi, sikkerhet, tilgjengelighet, og krysskompatibilitet for ulike plattformer og formfaktorer.

---

## 4. Relevante standarder og lovverk

De mest relevante standardene for prosjektet er diverse IKT-standarder.  
**Informasjonssikkerhet, cybersikkerhet og personvern; Informasjonssikkerhetstiltak (NS-EN ISO/IEC 27002:2022)** [[6]] er den eneste internasjonalt anerkjente standarden som er tilgjengelig for meg, med UiTs abonnement hos Standard Norge.  
Det er flere standarder jeg gjerne skulle lest, som inngår i serien, som:
- NS-ISO 27000 for introduksjon til standardserien og Informasjonssikkerhets-styringssystemer
- NS-ISO 27017 for skytjenester
- NS-ISO 27701 for personvern  

Dessverre er disse standardene låst bak betalingsmur, og derfor ikke aktuelle å bruke.  
Nasjonal Sikkerhetsmyndighet har laget en standard for informasjonssikkerhet basert hovedsaklig på NS-EN ISO/IEC 27002:2022, kalt **NSM Grunnprinsipper for IKT (NSM GP-IKT v2.0)** [[7]].  
Der ISO-standarden jevnlig referer til andre standarder (som er bak betalingsmur) for sentrale konsepter, er NSM GP-IKT standarden ment å stå selvstendig. Den er også skrevet på et mer folkelig språk, og inneholder brukervennlige veiledninger.

De mest relevante lovverkene for prosjektet er **Personopplysningsloven** [[8]] og **Digitalytelsesloven** [[9]]. På gårder med ansatte gjelder også **Forskrift om kameraovervåkning i virksomhet.**[[10]]  
Et sentralt krav i peronopplysningsloven er at ethvert system eller løsning skal ha innebygd personvern. Det betyr at hensyn til, og risiko for redusert personvern skal systematisk vurderes og imøtekommes på alle stadier av utvikling, drift, og avvikling av enhver løsning og ethvert system.  
De syv prinsippene for Innebygd Personvern er:
1. Vær i forkant, forebygg fremfor å reparere
2. Gjør personvern til standardinnstilling
3. Bygg personvern inn i designet
4. Skap full funksjonalitet
5. Ivareta informasjonssikkerheten fra start til slutt
6. Vis åpenhet
7. Respekter brukerens personvern

[[11]]

Et uttalt mål for dette prosjektet er å eksplisitt **ikke** behandle personopplysninger. 

<br>

Digitalytelseslovens mest relevante deler for dette prosjektet, er hovedsaklig disse (parafrasert): 
- § 8, punkt 3: "Produktet skal leveres med tilbehør, innpakning, installasjonsveiledning og lignende som forbrukeren med rimelighet kan forvente å få"  
- § 9: "Leverandøren skal sørge for at forbrukeren får beskjed om og får levert oppdateringer, herunder sikkerhetsoppdateringer, som er nødvendige for å oppfylle kravene i §§ 7 og 8."
- § 26: Krav om erstatning, retting, heving, avslag kan kun rettes mot tidligere ledd i avtalekjeden (oppstrøms fra B2C-leverandør), dersom det tidligere leddet opptrer i næringsvirksomhet.  
- § 45: Ved heving av avtalen, skal leverandør gjøre forbrukers data  tilgjengelig for kostnadsfri nedlasting i et ordinært, maskinlesbart format.

Ellers gjelder at man ikke kan avtale seg bort fra lovkravene, at man skal levere det man averterer og lover. [[9]]  

---

## 5. Miljøbelastning / karbonavtrykk
For å vite den miljømessige kost/nytten mellom å miste husdyr til rovvilt, og å trene og bruke en maskinlæringsmodell for å unngå tapene; må klimabelastningen av maskinlæringsmodellens trening og bruk settes måles mot hverandre.

Husdyrbasert matproduksjon står for mellom 11 og 16% av globale menneskeskapte klimagassutslipp, med noe usikkerhet[[12]]. Dersom klimagassutslipp fra matproduksjon holdes stabil, må utslippene fra industri reduseres med 80% før 2050 for å holde global oppvarming under 2 grader Celsius.[[13]]  

Databruk sto for 1.8-2.8% av menneskets karbonavtrykk i 2020 [[14]].

Det annualiserte karbonavtrykket av husdyrbasert matproduksjon frem mot 2100, i kg CO2-ekvivalenter per kg råvare, er [[12]]:
- fårekjøtt: 286kg aCO2e 
- storfekjøtt: 298kg aCO2e
- fåremelk: 23kg aCO2e
- storfemelk: 10kg aCO2e 

Det ikke-annualiserte karbonavtrykket i kg CO2-ekvivalenter per kg råvare er [[15]]:
- fårekjøtt: 19,1kg CO2e 
- storfekjøtt: 35,8kg CO2e

Gjennomsnittlig kjøttvekt per fåreslakt i Norge i 2022 var ca 10 kg [[16]]. Jeg har ikke funnet statistikk på når flest lam tas av rovvilt, og hvilket karbonavtrykk de har hatt til da, så karbonavtrykket settes likt som på slaktetidspunktet for alle dyr som tas av rovvilt.

Rovvilt tar ca 16 000 lam og sau i Norge hvert år [[17]], hvilket gir et samlet unødig karbonavtrykk på 3056 tonn CO2e. 

Trening av maskinlæringsmodellen på  vil kreve ca 10kWt, og dermed ha et karbonavtrykk på ca 2kg CO2e dersom den trenes i et toppstandard datasenter, og en viss, og ukjent faktor mer dersom den trenes på en ordinær datamaskin.[[18]] [[19]]  
Det er vanskelig å estimere karbonavtrykket av maskinlæringsmodellens bruksfase, ettersom det kommer an på implementering, språk, maskinvare og bruksforhold. 
Google estimerer at 60% av en maskinlæringsmodells totale karbonavtrykk hos dem kommer fra bruksfasen [[20]]. Dersom hver gård kjører ML-modellen lokalt vil karbonavtrykket bli høyere enn om modellen kjører i et datasenter, grunnet datasentrenes høyere energieffektivitet og ressursbruk. Dersom bruken av ML-modellen reduserer tap av husdyr til rovvilt, vil det likevel i alle tilfeller, med stor sannsynlighet veie opp for karbonavtrykket av å kjøre modellen.

---

## 6. Programvarebiblioteker

### Ultralytics
Ultralytics-pakken er et rammeverk for enkel trening og bruk av maskinlæringsmodeller til bildeanalyse, i Python. Lisensiert AGPL-v3. Begrenset til et fåtall modeller, tilbudt av selskapet Ultralytics. De tilbyr Baidu's RT-DETR, som er den beste og raskeste objektgjenkjenningsmodellen tilgjengelig per nå.

### Huggingface
En altomspennende plattform og rammeverk for maskinlæring. Verktøy for enkel til avansert bruk av modeller; plattform for å publisere, lagre, kjøre og prøve modeller. Verktøyene for integrasjon i kildekode er lisensiert Apache-2.0, modeller kan ha andre lisenser.

### Roboflow
En plattform for å dele datasett for maskinlæring, og trene modeller.

---

## 7. Maskinlæringsmodeller

### YOLOv8
Mean Average Precision (mAP_val_50-95) brukes for å måle presisjonen til objektgjenkjenningsmodeller. Forklaring kan leses i [[21]].

Lisensiert AGPL-v3. Var beste tilgjengelige modell for objektgjenkjenning før RT-DETR ble publisert. Modellen som Ultralytics rammeverket er bygd rundt. Tilgjengelig stort sett overalt.

mAP for største modellen YOLOv8x på valideringsdatasettet i COCO2017: 53.9%  

Utviklerne har ikke skrevet en offisiell forskningsartikkel om den, men en tredjepart har punlisert en uoffisiell: https://arxiv.org/abs/2305.09972  
repo: https://github.com/ultralytics/ultralytics

### RT-DETR
Real-Time DEtection TRansformer (RT-DETR). Lisensiert Apache-2.0. Beste tilgjengelige modell for objektgjenkjenning. Utviklet av forskere i selskapet Baidu.  	
Modellen er ikke særlig tilgjengelig via Huggingface, men er integrert i Ultralytics og er også direkte tilgengelig på repoet.

mAP for største modellen RT-DETR-HGNetv2-X på valideringssettet i COCO2017: 54.8%

forskingsartikkel: https://doi.org/10.48550/arXiv.2304.08069   
repo: https://github.com/lyuwenyu/RT-DETR


---

## 8. Treningmateriale for maskinlæring
Bildemateriale hentes fra Roboflow [[22]], Kaggle [[23]], og overvåkningskameraer på Strandmoa gård. Det har ikke lyktes å få bildemateriale fra noen av de store norske dyreaktørene; som NIBIO [[24]], SNO [[25]], NINA og ScandLynx [[26]], NSG [[27]] eller AKSSA [[28]].  
Til annotering av bilder kan benyttes Roboflow med metoden overvåket læring.  
Grunnet tidsbegrensninger må prosjektet avgrense seg til å gjenkjenne 3 objektklasser.  
Sau/lam, gaupe, og person. Dette vil sørge for at sau og lam, som er de vanligste objektene i en saueinnhegning, ikke blir gjenstand for falske positive gjenkjenninger på rovvilt; at et av de vanligste rovviltene gjenkjennes; og at man unngår å ta opptak eller prosessere bilder som inneholder personer.


---

## Referanseliste
[1]: https://github.com/Jimbwlah/tensorflow-ring-animal-detector/issues/1
[1] https://github.com/Jimbwlah/tensorflow-ring-animal-detector/issues/1

[2]: https://github.com/juanfont/headscale/issues/1656
[2] https://github.com/juanfont/headscale/issues/1656

[3]: https://www.statista.com/statistics/793628/worldwide-developer-survey-most-used-languages/
[3] https://www.statista.com/statistics/793628/worldwide-developer-survey-most-used-languages/

[4]: https://spectrum.ieee.org/top-programming-languages-2022
[4] https://spectrum.ieee.org/top-programming-languages-2022

[5]: https://learnpython.com/blog/why-is-python-so-popular/
[5] https://learnpython.com/blog/why-is-python-so-popular/

[6]: https://online.standard.no/nb/nek-en-isoiec-27002-2022
[6] https://online.standard.no/nb/nek-en-isoiec-27002-2022

[7]: https://nsm.no/getfile.php/133735-1592917067/NSM/Filer/Dokumenter/Veiledere/nsms-grunnprinsipper-for-ikt-sikkerhet-v2.0.pdf
[7] https://nsm.no/getfile.php/133735-1592917067/NSM/Filer/Dokumenter/Veiledere/nsms-grunnprinsipper-for-ikt-sikkerhet-v2.0.pdf

[8]: https://lovdata.no/dokument/NL/lov/2018-06-15-38
[8] https://lovdata.no/dokument/NL/lov/2018-06-15-38

[9]: https://lovdata.no/dokument/NL/lov/2022-06-17-56
[9] https://lovdata.no/dokument/NL/lov/2022-06-17-56

[10]: https://lovdata.no/dokument/SF/forskrift/2018-07-02-1107
[10] https://lovdata.no/dokument/SF/forskrift/2018-07-02-1107

[11]: https://www.datatilsynet.no/rettigheter-og-plikter/virksomhetenes-plikter/innebygd-personvern-og-personvern-som-standard/
[11] https://www.datatilsynet.no/rettigheter-og-plikter/virksomhetenes-plikter/innebygd-personvern-og-personvern-som-standard/

[12]: https://journals.plos.org/climate/article?id=10.1371/journal.pclm.0000010
[12] https://journals.plos.org/climate/article?id=10.1371/journal.pclm.0000010

[13]: https://www.nature.com/articles/s43016-021-00265-1
[13] https://www.nature.com/articles/s43016-021-00265-1

[14]: https://doi.org/10.1038/s43588-023-00461-y
[14] https://doi.org/10.1038/s43588-023-00461-y

[15]: https://foodandagricultureorganization.shinyapps.io/GLEAMV3_Public/
[15] https://foodandagricultureorganization.shinyapps.io/GLEAMV3_Public/

[16]: https://www.ssb.no/jord-skog-jakt-og-fiskeri/jordbruk/statistikk/kjotproduksjon
[16] https://www.ssb.no/jord-skog-jakt-og-fiskeri/jordbruk/statistikk/kjotproduksjon

[17]: https://www.miljodirektoratet.no/ansvarsomrader/arter-naturtyper/vilt/rovvilt/husdyr-tap/
[17] https://www.miljodirektoratet.no/ansvarsomrader/arter-naturtyper/vilt/rovvilt/husdyr-tap/

[18]: https://wandb.ai/glenn-jocher/YOLOv8/runs/oxwuolug?workspace=user-
[18] https://wandb.ai/glenn-jocher/YOLOv8/runs/oxwuolug?workspace=user-

[19]: https://corporate.ovhcloud.com/en/sustainability/environment/
[19] https://corporate.ovhcloud.com/en/sustainability/environment/

[20]: https://blog.research.google/2022/02/good-news-about-carbon-footprint-of.html
[20] https://blog.research.google/2022/02/good-news-about-carbon-footprint-of.html

[21]: https://learnopencv.com/mean-average-precision-map-object-detection-model-evaluation-metric/
[21] https://learnopencv.com/mean-average-precision-map-object-detection-model-evaluation-metric/

[22]: https://roboflow.com/
[22] https://roboflow.com/

[23]: https://www.kaggle.com/
[23] https://www.kaggle.com/

[24]: https://www.nibio.no/
[24] https://www.nibio.no/

[25]: https://www.miljodirektoratet.no/om-oss/miljodirektoratets-organisasjon/statens-naturoppsyn/
[25] https://www.miljodirektoratet.no/om-oss/miljodirektoratets-organisasjon/statens-naturoppsyn/

[26]: https://www.nina.no/Naturmangfold/Rovvilt/SCANDLYNX/
[26] https://www.nina.no/Naturmangfold/Rovvilt/SCANDLYNX/

[27]: https://www.nsg.no/
[27] https://www.nsg.no/

[28]: https://www.akssa.no/
[28] https://www.akssa.no/


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
