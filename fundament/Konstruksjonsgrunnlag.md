
# Konstruksjonsgrunnlag

## Innledning  
Essensielle egenskaper og sekundære egenskaper er beskrevet i kravspesifikasjon.

## Konsepter  
Alle komponentene er til dels modulære, slik at valg av nettverksløsning ikke påvirker valg av kamera-programvare i særlig grad. Det samme gjelder valg av maskinlæringsmodell. Derfor vurderes de tre kategoriene separat, og et helhetlig konsept dannes av de mest hensiktsmessige komponentene i hver kategori.

---

### 1. Nettverksløsning

#### Komponent 1A, Nebula  
Nebula får en offisiell Docker/podman konteiner til versjon 1.9. 48% av versjonens milepæler er nådd. Historisk har Nebula publisert en ny versjon omtrent hver 6. måned, og forrige versjon ble publisert i januar 2024. Ny versjon kan ventes iløpet av juni, og er derfor ikke appliserbar for dette prosjektet.   Nebula lar et utvalg av noder koblet til fyrtårnet kommunisere med hverandre, basert på gruppemedlemskap. Det er dermed mulig å ha ett fyrtårn for alle gårder, uten at kommunikasjonen til én gård lekker over til en annen. Nebula er enormt skalerbart.  
Når konteiner er tilgjengelig, vil den publiseres her: https://hub.docker.com/r/nebulaoss/nebula 

#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Brukervennlighet					|8			|6				|48			|
|Konteinerisering					|6			|5				|30			|
|Enkelt å implementere			|8			|6				|48			|
|Adgangskontroll					|9			|8				|72			|
|Nettgrensesnitt					|8			|2				|16			|
|Mobilapplikasjon / PWA			|8			|7				|56			|
|Ressurseffektivitet				|6			|9				|54			|
|Datasikkerhet						|8			|8				|64			|
|Lett å vedlikeholde			|8			|6				|48			|
|Aktivt utviklermiljø			|7			|8				|56			|
|Skalerbarhet					|6			|7				|42			|
|Total poengsum					|			|				|534		|

<br>

---

#### Komponent 1B, Headscale  
Headscale utviklerne anbefaler sterkt at man ikke kjører Headscale konteinerisert. Det finnes like fullt samfunns-bidratte konteinerfiler.


#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Brukervennlighet				|8			|8				|64			|
|Konteinerisering				|6			|6				|36			|
|Enkelt å implementere			|8			|5				|40			|
|Adgangskontroll				|9			|2				|18			|
|Nettgrensesnitt				|8			|8				|64			|
|Mobilapplikasjon / PWA			|8			|8				|64			|
|Ressurseffektivitet			|6			|7				|42			|
|Datasikkerhet					|8			|6				|48			|
|Lett å vedlikeholde|8			|6				|48			|
|Aktivt utviklermiljø			|7			|6				|42			|
|Skalerbarhet					|6			|4				|24			|
|Total poengsum					|			|				|490		|

<br>

---

#### Komponent 1C, OpenZiti


#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Brukervennlighet				|8			|8				|64			|
|Konteinerisering				|6			|9				|54			|
|Enkelt å implementere			|8			|4				|32			|
|Adgangskontroll				|9			|9				|81			|
|Nettgrensesnitt				|8			|9				|72			|
|Mobilapplikasjon / PWA			|8			|8				|64			|
|Ressurseffektivitet			|6			|7				|42			|
|Datasikkerhet					|8			|9				|72			|
|Lett å vedlikeholde|8			|7				|56			|
|Aktivt utviklermiljø			|7			|6				|42			|
|Skalerbarhet					|6			|9				|54			|
|Total poengsum					|			|				|633		|


<br>

#### Konklusjon  
Komponent 1C, OpenZiti skårer høyest på vektingsmatrisen, så det er denne komponenten videre konsept vil basere seg på å bruke.

<br>

----

### 2. Kameraprogramvare


#### Komponent 2A, Zoneminder  

Zoneminder's maskinlæringsmodul ZMEventNotification støtter ikke offisielt konteineriserng, men det finnes uoffisielle tredjeparts-konteinere. Det er en del manuelt oppsett involvert i ZMEN, men dokumentasjonen er god. ZMEN er hovedsaklig skrevet i Python3 og bruker OpenCV i Python, så det er ikke særlig ressurseffektivt.  
Maskinlæringsmodulen har ikke støtte for verken RT-DETR eller YOLOv8, så en integrasjon må lages manuelt.


#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Brukervennlighet				|8			|7				|56			|
|Konteinerisering				|6			|2				|12			|
|Enkelt å implementere			|8			|2				|16			|
|Adgangskontroll				|9			|4				|36			|
|Nettgrensesnitt				|8			|8				|64			|
|Mobilapplikasjon / PWA			|8			|8				|64			|
|Lagring av video				|6			|8				|48			|
|Maskinvareakselerasjon	|6			|4				|24			|
|Ressurseffektivitet			|6			|3				|18			|
|Datasikkerhet					|8			|3				|24			|
|Alarmfunksjon					|7			|7				|49			|
|Lett å vedlikeholde|8			|3				|24			|
|Aktivt utviklermiljø			|7			|4				|28			|
|Push-varsling til mobil	|7			|7				|49			|
|API for hendelser |6			|6				|36			|
|Skalerbarhet					|6			|6				|36			|
|Total poengsum					|			|				|584			|

<br>

---

#### Komponent 2B, Viseron  
Viseron har gode prinsipper men svært mangelfull dokumentasjon.


#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Brukervennlighet				|8			|6				|48			|
|Konteinerisering				|6			|4				|24			|
|Enkelt å implementere			|8			|4				|32			|
|Adgangskontroll				|9			|3				|27			|
|Nettgrensesnitt				|8			|6				|48		|
|Mobilapplikasjon / PWA			|8			|3				|24			|
|Lagring av video				|6			|6				|36			|
|Maskinvareakselerasjon|6			|6				|36			|
|Ressurseffektivitet			|6			|5				|30			|
|Datasikkerhet					|8			|3				|24			|
|Alarmfunksjon					|7			|7				|49			|
|Lett å vedlikeholde|8			|4				|32			|
|Aktivt utviklermiljø			|7			|3				|21			|
|Push-varsling til mobil	|7			|2				|14			|
|API for hendelser |6			|7				|42			|
|Skalerbarhet					|6			|4				|24			|
|Total poengsum					|			|				|511			|


<br>

---

#### Komponent 2C, Frigate  
Frigate har en helm chart tilgjengelig for kubernetes.  
Frigate har meget god dokumentasjon, og virker moderne og aktiv.


#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Brukervennlighet				|8			|7				|56			|
|Konteinerisering				|6			|7				|42			|
|Enkelt å implementere			|8			|3				|24			|
|Adgangskontroll				|9			|2				|18			|
|Nettgrensesnitt				|8			|6				|48			|
|Mobilapplikasjon / PWA			|8			|4				|32			|
|Lagring av video				|6			|7				|42			|
|Maskinvareakselerasjon	|6			|7				|42			|
|Ressurseffektivitet			|6			|7				|42			|
|Datasikkerhet					|8			|4				|32			|
|Alarmfunksjon					|7			|6				|42			|
|Lett å vedlikeholde|8			|7				|56			|
|Aktivt utviklermiljø			|7			|6				|42			|
|Push-varsling til mobil|7			|4				|28			|
|API for hendelser |6			|7				|42			|
|Skalerbarhet					|6			|8				|48			|
|Total poengsum					|			|				|636			|

<br>

---

#### Komponent 2D, Predalert / FOSOPAS  
Predalert er et egenutviklet programvareprosjekt

#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Brukervennlighet				|8			|3				|24			|
|Konteinerisering				|6			|3				|18			|
|Enkelt å implementere			|8			|3				|24			|
|Adgangskontroll				|9			|2				|18			|
|Nettgrensesnitt				|8			|3				|24			|
|Mobilapplikasjon / PWA			|8			|2				|16			|
|Lagring av video				|6			|2				|12			|
|Maskinvareakselerasjon 	|6			|5				|30			|
|Ressurseffektivitet			|6			|4				|24			|
|Datasikkerhet					|8			|3				|24			|
|Alarmfunksjon					|7			|3				|21			|
|Lett å vedlikeholde|8			|7				|56			|
|Aktivt utviklermiljø			|7			|3				|21			|
|Push-varsling til mobil	|7			|2				|14			|
|API for hendelser |6			|2				|12			|
|Skalerbarhet					|6			|3				|18			|
|Total poengsum					|			|				|356			|

<br>

#### Konklusjon  
Komponent 2C, Frigate, skårer høyest på vektingsmatrisen. Derfor burde videre arbeid basere seg på integrasjon av denne komponenten.

---

### 3. Objektgjenkjenning

#### Komponent 3A, YOLOv8x


#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Konteinerisering				|6			|6				|36			|
|Enkelt å implementere			|8			|8				|64			|
|Maskinvareakselerasjon	|6			|9				|54			|
|Ressurseffektivitet			|6			|5				|30			|
|Lett å vedlikeholde|8			|8				|64			|
|Aktivt utviklermiljø			|7			|9				|63			|
|Skalerbarhet					|6			|6				|36			|
|Presisjon							|9			|7				|63			|
|Hurtighet						|6			|5				|36			|
|Total poengsum					|			|				|446			|

<br>

---

#### Komponent 3B, RT-DETR


#### Vektingsmatrise
| Element						|Vekting	|Oppnåelsesgrad	| Poengsum  |
|------------------------------|:------------:|:------------------------:|:-----------------:|
|Konteinerisering				|6			|6				|36			|
|Enkelt å implementere			|8			|7				|56			|
|Maskinvareakselerasjon|6			|8				|48			|
|Ressurseffektivitet			|6			|7				|42			|
|Lett å vedlikeholde|8			|8				|64			|
|Aktivt utviklermiljø			|7			|8				|56			|
|Skalerbarhet					|6			|6				|36			|
|Presisjon							|9			|8				|72			|
|Hurtighet						|6			|7				|42			|
|Total poengsum					|			|				|452			|


<br>

#### Konklusjon  
Komponent 3B, RT-DETR, skårer høyest på vektingsmatrisen og videre arbeid vil derfor basere seg på integrasjon av denne.


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->
