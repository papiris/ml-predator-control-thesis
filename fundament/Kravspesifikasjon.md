# Kravspesifikasjon

#### Formål
Dette dokumentet har som formål å fastslå nødvendige krav til produktet. På denne måten skal det
allerede før utviklingen er startet være avklart og oppstått en enighet mellom oppdragsgiver og
oppdragstaker om hva som forventes ved prosjektslutt.  
Dokumentet skal signeres ved inngått enighet.

#### Innledning
Kravene er utarbeidet på bakgrunn av møter med potensielle kunder, standarden NSM-GP-IKT-v2, personopplysningsloven, digitalytelsesloven og egen erfaring. Målet er å ha et fullverdig programvareprodukt ved prosjektslutt, som lovlig kan omsettes.

<br>


## Essensielle egenskaper
### Kjøremiljø / grensebetingelser

For å sikre driftssikkerhet, enkel leveranse og applisering av oppdateringer, samt portabilitet; er det nødvendig at programvaresystemets komponenter kjører konteineriserte.  
Installasjon av programvaresystemet på en gård bør ikke samtidig kreve installasjon av en stor kabinett-pc, med grafikkort i full størrelse. 

Programvaren må:  
1. kjøre konteinerisert  
2. kunne bruke en av Coral AI sine ML-akseleratorer  
3. være ressursgjerrig og effektiv  
4. være enkel å oppdatere  
5. ha et aktivt utviklermiljø rundt komponentene sine  
6. følge personopplysningsloven  
7. følge digitalytelsesloven  
8. følge de viktigste momentene i NSM-GP-IKT-v2  
9. være trygg å bruke  
10. være fri programvare  

<br>

### Funksjon

Programvaren må:  
1. kunne gjenkjenne tre objektklasser med høy nøyaktighet  
2. kunne lagre relevant video  
3. kreve innlogging før bruk  
4. være tilgjengelig via nettgrensesnitt eller app for mobiltelefon  
5. kunne sende push-varsel til mobiltelefon  
6. være brukervennlig  

<br>

----

## Sekundære egenskaper
### Kjøremiljø / grensebetingelser
Det er ønskelig å levere systemets maskinvare som en samlet enhet, uten løse komponenter.  
Det er ønskelig å kunne oppgradere maskinvarekomponenter.


### Funksjon
Programvaren bør:  
1. kunne kobles sammen med ekstern alarmutstyr  
2. ha mulighet for multifaktor-autentisering ved innlogging  
3. gi mulighet for å laste ned lagrede videoer  

<br>

----

## Sluttrapport
Sluttrapport må inneholde litteratursammendrag, konsepter og begrunnelse for valgt konsept, systemdiagram og systembeskrivelse på komponentnivå, risikovurdering for produktet og lenke til programvarens kildekode. Alle produserte dokumenter, filer og programmer skal publiseres under en fri, copyleft lisens.  

<br>

#### Signatur oppdragsgiver: _________________________  


#### Signatur oppdragstaker: _________________________  


##### Sted:


##### Dato:


<!-- SPDX-License-Identifier: CC-BY-SA-4.0+ -->